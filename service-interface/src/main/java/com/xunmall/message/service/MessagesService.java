package com.xunmall.message.service;

import com.xunmall.base.dto.KafkaMessage;
import com.xunmall.message.dto.MessagesForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Lazy;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/**
 * Created by Gimgoog on 2017/8/18.
 */
@ConditionalOnBean(value = KafkaTemplate.class)
@Component
public class MessagesService {
    @Autowired
    @Lazy
    private KafkaTemplate<String, KafkaMessage> kafkaTemplate;

    public void sendMessage(MessagesForm messagesForm) {
        MessagesValidate.Validate(messagesForm);
        KafkaMessage<MessagesForm> msg = new KafkaMessage(messagesForm);
        kafkaTemplate.send("gin-message", "message-key-" + System.currentTimeMillis(), msg);
    }
}
