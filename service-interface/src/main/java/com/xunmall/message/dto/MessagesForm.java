package com.xunmall.message.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Gimgoog on 2017/8/18.
 */
@Data
public class MessagesForm implements Serializable {
    private static final long serialVersionUID = -7986677116890453986L;
    // 短信模板id
    @NotNull(message = "模板编号不能为空")
    private String templateId;

    // 接收手机号
    @NotNull(message = "接收号码不能为空")
    private String address;

    // 失效时间(分钟)
    private Integer expires;

    // 短信内容
    private String content;

    // 模板短信参数，参数按下标顺序对应
    private String[] params;
}
