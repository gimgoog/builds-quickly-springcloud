package com.xunmall.account.vo;

import com.xunmall.base.page.PageInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class AccountListVo extends PageInfo {
    private static final long serialVersionUID = -4678175635308239493L;
    /**
     * 状态（1：启用；0：禁用）
     */
    private Boolean isActive;

    /**
     * 开户日期开始区间
     */
    private String registBeginDate;
    /**
     * 开户日期结束区间
     */
    private String registEndDate;

    /**
     * 最后登录日期开始区间
     */
    private String lastLoginBeginDate;
    /**
     * 最后登录日期结束区间
     */
    private String lastLoginEndDate;

    /**
     * 过滤条件（姓名/手机号）
     */
    private String filter;
}