package com.xunmall.account.vo;

import com.xunmall.base.page.PageInfo;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

@Data
public class AccountCompListVo extends PageInfo {
    private static final long serialVersionUID = -4678175635308239493L;

    /**
     * 用户编号
     */
    @NotBlank(message="error.0001")
    private String userUuid;
}