package com.xunmall.account.vo;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;

/**
 * 修改企业基本信息
 *
 **/
@Data
public class UpdateBasicCompanyVo implements Serializable {
    @NotBlank(message = "error.0001")
    private String uuid;
    private String nickName;
    //企业基础信息
    private CompBasicInfoVo compBasicInfo;
    //企业联系人信息
    private CompContactsInfoVo compContactsInfoVo;

}
