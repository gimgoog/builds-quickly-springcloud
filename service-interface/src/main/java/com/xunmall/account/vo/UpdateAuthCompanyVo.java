package com.xunmall.account.vo;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;

/**
 * 修改企业认证信息
 **/
@Data
public class UpdateAuthCompanyVo implements Serializable {
    @NotBlank(message = "error.0001")
    private String uuid;
    @NotBlank(message = "error.0001")
    private String realName;
    private String busiLiceFile;

    /**
     * 注册渠道
     */
    @NotBlank(message = "error.0001")
    private String regChannel;

    /**
     * 所属行业
     */
    @NotBlank(message = "error.0001")
    private String industry;

    /**
     * 邀请码
     */
    private String invitationCode;
}
