package com.xunmall.account.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 企业联系人信息
 **/
@Data
public class CompContactsInfoVo implements Serializable{
    private static final long serialVersionUID = 9137088431950537783L;
    /**
     * 联系人
     */
    private String contacts;
    /**
     * 联系人所在地区
     */
    private String contArea;
    /**
     * 联系人手机
     */
    private String contMobile;
    /**
     * 联系人详细地址
     */
    private String contAddress;
    /**
     * 联系人邮箱
     */
    private String contEmail;
    /**
     * 联系人邮编
     */
    private String contZipCode;

}
