package com.xunmall.account.vo;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

@Data
public class AddCompanyVo {
	@NotBlank(message = "error.0001")
	private String companyName;
}
