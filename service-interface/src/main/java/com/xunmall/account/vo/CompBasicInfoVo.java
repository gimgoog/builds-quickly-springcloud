package com.xunmall.account.vo;

import lombok.Data;

import javax.persistence.Column;
import java.io.Serializable;

/**
 * 企业基本信息
 *
 **/
@Data
public class CompBasicInfoVo implements Serializable{
    /**
     * 所在地区
     */
    private String area;
    /**
     * 行业
     */
    private String industry;
    /**
     * 地址
     */
    private String address;
    /**
     * 企业规模
     */
    private String scale;
    /**
     * 邮编
     */
    private String zipCode;
}
