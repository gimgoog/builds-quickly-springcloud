package com.xunmall.account.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Yvan on 2017/8/24.
 */
@Data
public class IsActiveVo implements Serializable{
    private static final long serialVersionUID = 2911110283287669430L;

    /**
     * 用户编号
     */
    @NotNull(message="error.0001")
    private List<String> userUuids;

    /**
     * 状态：是否可用，1可用；0不可用
     */
    @NotNull(message="error.0001")
    private Boolean isActive;
}