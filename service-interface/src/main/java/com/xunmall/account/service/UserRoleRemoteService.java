package com.xunmall.account.service;

import com.xunmall.account.dto.UserDto;
import com.xunmall.account.dto.UserRoleDto;
import com.xunmall.base.dto.Result;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "gim-account", url = "${xunmall.feign-client.gim-account:}")
public interface UserRoleRemoteService {
    @RequestMapping(value = "/account/userroles/{uuid}", method = RequestMethod.GET)
    Result<UserRoleDto> getByUuid(@PathVariable("uuid") String uuid);

    @RequestMapping(value = "/account/userroles/comp-admin", method = RequestMethod.GET)
    Result<List<UserDto>> getAdminUuid(@RequestParam("compUuid") String compUuid);
}