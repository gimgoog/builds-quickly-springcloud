package com.xunmall.account.service;

import com.xunmall.account.dto.*;
import com.xunmall.base.dto.Result;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "gim-account", url = "${xunmall.feign-client.gim-account:}")
public interface UserRemoteService {
    @RequestMapping(value = "/account/users/check_user_name/{userName}", method = RequestMethod.GET)
    Result<Boolean> checkUserName(@PathVariable("userName") String userName);

    @RequestMapping(value = "/account/users/create", method = RequestMethod.POST)
    Result<UserDto> create(@RequestBody UserDto user);

    @RequestMapping(value = "/account/users/update", method = RequestMethod.POST)
    Result<UserDto> update(@RequestBody UserDto user);

    @RequestMapping(value = "/account/users/{uuid}", method = RequestMethod.GET)
    Result<UserDto> getByUuid(@PathVariable("uuid") String uuid);

    @RequestMapping(value = "/account/users/forget_pwd", method = RequestMethod.PUT)
    Result<Boolean> forgetPwd(@RequestParam("mobile") String mobile,
                              @RequestParam("password") String password);

    @RequestMapping(value = "/account/users/reset_pwd", method = RequestMethod.PUT)
    Result<Boolean> resetPwd(@RequestParam("uuid") String uuid,
                             @RequestParam("curPassword") String curPassword,
                             @RequestParam("newPassword") String newPassword);

    @RequestMapping(value = "/account/users/update_mobile", method = RequestMethod.PUT)
    Result<Boolean> updateMobile(@RequestParam("uuid") String uuid,
                                 @RequestParam("mobile") String mobile,
                                 @RequestParam("password") String password);

    @RequestMapping(value = "/account/users/get_by_name/{username}", method = RequestMethod.GET)
    Result<LoginUserDto> getByUserName(@PathVariable("username") String username);

    @RequestMapping(value = "/account/users/get_company", method = RequestMethod.GET)
    Result<CompanyDto> getUserCompany(@RequestParam("userUuid") String userUuid);

    @RequestMapping(value = "/account/users/is_user_in_comp", method = RequestMethod.GET)
    Result<Boolean> isUserInComp(@RequestParam("userUuid") String userUuid,
                                 @RequestParam("companyUuid") String companyUuid);

    @RequestMapping(value = "/account/users/get_comp_managers/{compUuid}", method = RequestMethod.GET)
    List<ManageListDto> queryCompManageList(@PathVariable("compUuid") String compUuid);

    @RequestMapping(value = "/account/users/correlation_roles", method = RequestMethod.POST)
    int correlationRoles(@RequestParam("compUuid") String compUuid,
                         @RequestParam("userUuid") String userUuid,
                         @RequestParam("roles") List<String> roles,
                         @RequestParam("onOffFlag") String onOffFlag,
                         @RequestParam("relaStatus") String relaStatus,
                         @RequestParam(value = "emplUuid", required = false) String emplUuid);

    @RequestMapping(value = "/account/users/get_user_role", method = RequestMethod.GET)
    UserRoleDto getUserRole(@RequestParam("userUuid") String userUuid,
                            @RequestParam("compUuid") String compUuid,
                            @RequestParam("roleId") String roleId);

    @RequestMapping(value = "/account/users/uncorrelation_roles", method = RequestMethod.POST)
    int uncorrelationRoles(@RequestParam("compUuid") String compUuid,
                           @RequestParam("userUuid") String userUuid,
                           @RequestParam("roles") List<String> roles);

    @RequestMapping(value = "/account/users/comp_manage_onoff", method = RequestMethod.POST)
    int modifyCompManageOnOff(@RequestParam("userRoleUuid") String userRoleUuid,
                              @RequestParam("onOffFlag") String onOffFlag);


    @RequestMapping(value = "/account/users/comp_manage_role", method = RequestMethod.PUT)
    int modifyCompManageRole(@RequestParam("userRoleUuid") String userRoleUuid,
                             @RequestParam("roleUuid") String roleUuid);

    @RequestMapping(value = "/account/users/user_role_rela", method = RequestMethod.POST)
    int modifyUserRoleRela(@RequestParam("userRoleUuid") String userRoleUuid,
                           @RequestParam("relaStatus") String relaStatus);

    @RequestMapping(value = "/account/users/transfer_admin", method = RequestMethod.POST)
    int transferAdmin(@RequestParam("compUuid") String compUuid,
                      @RequestParam("oldMobile") String oldMobile,
                      @RequestParam("newMobile") String newMobile);

    @RequestMapping(value = "/account/users/uncorrelation_emplrole", method = RequestMethod.POST)
    void uncorrelationEmplRole(@RequestParam("compUuid") String compUuid,
                               @RequestParam("emplUuid") String emplUuid);

    @RequestMapping(value = "/account/users/modify_last_comp", method = RequestMethod.PUT)
    Integer modifyLastComp(@RequestParam("uuid") String uuid,
                           @RequestParam("compUuid") String compUuid,
                           @RequestParam("isAppLogin") boolean isAppLogin);

    @RequestMapping(value = "/account/users/check_pwd", method = RequestMethod.GET)
    Result<Boolean> checkPwd(@RequestParam("uuid") String uuid,
                             @RequestParam("password") String password);
}