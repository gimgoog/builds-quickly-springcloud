package com.xunmall.account.service;

import com.xunmall.account.dto.ApiDto;
import com.xunmall.account.vo.ApiVo;
import com.xunmall.base.dto.Result;
import com.xunmall.base.page.PagingModel;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@FeignClient(name = "gim-account", url = "${xunmall.feign-client.gim-account:}")
public interface ApiRemoteService {
    @RequestMapping(value = "/account/apis/create", method = RequestMethod.POST)
    Result<ApiDto> create(@RequestBody ApiDto api);

    @RequestMapping(value = "/account/apis/update", method = RequestMethod.POST)
    Result<ApiDto> update(@RequestBody ApiDto api);

    @RequestMapping(value = "/account/apis/{uuid}", method = RequestMethod.GET)
    Result<ApiDto> getByUuid(@PathVariable("uuid") String uuid);
}