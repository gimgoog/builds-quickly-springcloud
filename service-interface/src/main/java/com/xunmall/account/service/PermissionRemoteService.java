package com.xunmall.account.service;


import com.xunmall.account.dto.PermissionDto;
import com.xunmall.base.dto.Result;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author wangyanjing
 */
@FeignClient(name = "gim-account", url = "${xunmall.feign-client.gim-account:}")
public interface PermissionRemoteService {
    @RequestMapping(value = "/account/permissions/check_permission_code/{code}", method = RequestMethod.GET)
    Result<Boolean> checkPermissionCode(String code);

    @RequestMapping(value = "/account/permissions/create", method = RequestMethod.POST)
    Result<PermissionDto> create(@RequestBody PermissionDto permission);

    @RequestMapping(value = "/account/permissions/update", method = RequestMethod.POST)
    Result<PermissionDto> update(@RequestBody PermissionDto permission);

    @RequestMapping(value = "/account/permissions/{uuid}", method = RequestMethod.GET)
    Result<PermissionDto> getByUuid(@PathVariable("uuid") String uuid);
}