package com.xunmall.account.service;


import com.xunmall.account.dto.RoleDto;
import com.xunmall.account.dto.UrlDto;
import com.xunmall.account.dto.VersionDto;
import com.xunmall.base.dto.Result;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@FeignClient(name = "gim-account", url = "${xunmall.feign-client.gim-account:}")
public interface RoleRemoteService {
    @RequestMapping(value = "/account/roles/get_meta_source", method = RequestMethod.GET)
    Result<Map<String, List<UrlDto>>> getMetaSource();

    @RequestMapping(value = "/account/roles/get_by_user", method = RequestMethod.GET)
    Result<List<RoleDto>> getByUser(@RequestParam("userId") String userId,
                                    @RequestParam("compId") String compId);

    @RequestMapping(value = "/account/roles/get_by_comp", method = RequestMethod.GET)
    Result<List<RoleDto>> getByComp(@RequestParam("compId") String compId);

    @RequestMapping(value = "/account/roles/check_role_code/{roleCode}", method = RequestMethod.GET)
    Result<Boolean> checkRoleCode(@PathVariable("roleCode") String roleCode);

    @RequestMapping(value = "/account/roles/create", method = RequestMethod.POST)
    Result<RoleDto> create(@RequestBody RoleDto role);

    @RequestMapping(value = "/account/roles/update", method = RequestMethod.POST)
    Result<RoleDto> update(@RequestBody RoleDto role);

    @RequestMapping(value = "/account/roles/{uuid}", method = RequestMethod.GET)
    Result<RoleDto> getByUuid(@PathVariable("uuid") String uuid);
}