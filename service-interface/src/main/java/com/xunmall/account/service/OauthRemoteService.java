package com.xunmall.account.service;


import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "gim-gateway", url = "${xunmall.feign-client.gim-gateway:}")
public interface OauthRemoteService {
    @RequestMapping(value = "/oauth/revoke_token", method = RequestMethod.GET)
    void revokeToken(@RequestParam("userId") String userId);
}