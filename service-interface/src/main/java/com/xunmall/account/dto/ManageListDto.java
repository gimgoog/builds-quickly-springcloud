package com.xunmall.account.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by Yvan on 2017/7/4.
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ManageListDto extends UserRoleDto {
    //姓名
    private String name;
    //手机号
    private String mobile;
    //角色名称
    private String roleName;
    //qq号码
    private String qq;
    //最后登录时间
    private String lastLoginTime;
    //角色登记
    private Short roleLevel;
}