package com.xunmall.account.dto;

import com.xunmall.base.entity.BaseEntity;
import lombok.Data;

@Data
public class UserRoleDto extends BaseEntity {
    /**
     * 公司主键
     */
    private String compUuid;

    /**
     * 用户主键
     */
    private String userUuid;

    /**
     * 角色主键
     */
    private String roleUuid;

    /**
     * 员工主键
     */
    private String emplUuid;

    /**
     * 启/禁标示:0-启用;1-禁用
     */
    private String onOffFlag;

    /**
     * 关联状态:0-未关联;1-已关联
     */
    private String relaStatus;
}