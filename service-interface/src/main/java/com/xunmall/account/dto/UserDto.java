package com.xunmall.account.dto;

import com.xunmall.base.entity.BaseEntity;
import lombok.Data;

import java.util.Date;

@Data
public class UserDto extends BaseEntity {
    private String mobile;
    private String password;
    private String salt;
    private String showName;
    private String gender;
    private String idType;
    private String idNumber;
    private String idNumberForwardFile;
    private String idNumberBackFile;
    private Date birthday;
    private String email;
    private String qq;
    private String weixin;
    private String compUuid;
}