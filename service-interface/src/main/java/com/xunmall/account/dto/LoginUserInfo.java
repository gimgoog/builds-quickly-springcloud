package com.xunmall.account.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by hezqi on 2017/7/3.
 */
@Data
public class LoginUserInfo implements Serializable {
    private static final long serialVersionUID = -24901851892234572L;
    private String uuid;
    private String mobile;
    private String name;
    private String userPhoto;
    private String compUuid;
    private String compName;
    private String emplUuid;
    private Boolean isMultiCompany;
    private String initFlag;
    private List<String> permissions;
    private List<String> roles;
    private Boolean dptManager;
    private String dptManagerUuid;
    private String dptManagerName;
    private String comAcntStatus; //公司账号状态（启用、禁用）
    private String version; //公司使用的班步版本类型
    @JsonFormat(pattern = "yyyy/MM", timezone = "GMT+08:00")
    private Date comAcntTime; //公司账号创建时间（格式：YYYY/mm）
    private String bizArea; //公司所属行业
    private String channel; //注册渠道
}