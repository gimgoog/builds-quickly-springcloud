package com.xunmall.account.dto;

import lombok.Data;

@Data
public class VersionDto {
    /**
     * 记录uuid
     */
    private String uuid;

    /**
     * 名称
     */
    private String name;
}