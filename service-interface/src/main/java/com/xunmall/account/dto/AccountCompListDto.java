package com.xunmall.account.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
public class AccountCompListDto implements Serializable {

    /**
     * 公司编号
     */
    private String compUuid;
    /**
     * 公司名称
     */
    private String compName;
    /**
     * 身份
     */
    private String identity;

}
