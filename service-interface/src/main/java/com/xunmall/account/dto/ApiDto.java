package com.xunmall.account.dto;

import com.xunmall.base.entity.BaseEntity;
import lombok.Data;

/**
 * Created by hezqi on 2017/6/28.
 */
@Data
public class ApiDto extends BaseEntity {
    /**
     * api访问地址
     */
    private String url;

    /**
     * 排序号
     */
    private Integer orderNum;

    /**
     * http请求方式,多个逗号分隔;留空-任意方法
     */
    private String httpMethod;
}