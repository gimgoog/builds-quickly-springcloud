package com.xunmall.account.dto;

import com.xunmall.base.entity.BaseEntity;
import lombok.Data;

@Data
public class RoleDto extends BaseEntity {
    /**
     * 公司主键
     */
    private String compUuid;

    /**
     * 角色编号
     */
    private String roleCode;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 0-系统运维,2-集团管理员,4-公司管理员,16-普通角色。低级的角色不能给他人设置高等级的角色。
     */
    private Short roleLevel;

    /**
     * 描述
     */
    private String description;
}