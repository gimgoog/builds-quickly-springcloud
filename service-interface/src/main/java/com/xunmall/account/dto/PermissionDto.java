package com.xunmall.account.dto;

import com.xunmall.base.entity.BaseEntity;
import lombok.Data;

/**
 * Created by hezqi on 2017/6/28.
 */
@Data
public class PermissionDto extends BaseEntity {
    /**
     * 权限编号
     */
    private String code;

    /**
     * 权限名称
     */
    private String name;

    /**
     * 是否可用:0-不可用;1-可用
     */
    private Boolean enable;

    /**
     * 上级权限
     */
    private String pUuid;

    /**
     * 权限等级:0-系统运维,2-业务管理
     */
    private Integer permissionLevel;
}