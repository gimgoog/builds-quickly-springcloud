package com.xunmall.account.dto;

import lombok.Data;

import java.util.Date;

@Data
public class LoginCompDto {
    private String compUuid;
    private String compName;
    private String comAcntStatus; //公司账号状态（启用、禁用）
    private Date comAcntTime; //公司账号创建时间（格式：YYYY/mm）
    private String bizArea; //公司所属行业
    private String channel; //注册渠道
}