package com.xunmall.account.dto;

import lombok.Data;

/**
 * Created by hezqi on 2017/7/20.
 */
@Data
public class DptManagerDto {
    private boolean dptManager;
    private String dptManagerUuid;
    private String dptManagerName;
}
