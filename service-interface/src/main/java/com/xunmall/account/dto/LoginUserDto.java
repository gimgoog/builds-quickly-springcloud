package com.xunmall.account.dto;

import lombok.Data;

@Data
public class LoginUserDto {
    private String uuid;
    private String mobile;
    private String password;
    private String salt;
    private String username;
    private Boolean enabled = Boolean.TRUE;
    /**
     * 记录用户最后一次切换的公司uuid
     */
    private String compUuid;
}