package com.xunmall.email.service;

import com.xunmall.base.dto.KafkaMessage;
import com.xunmall.email.dto.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Lazy;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@ConditionalOnBean(value = KafkaTemplate.class)
@Component
public class EmailService {
	@Autowired
	@Lazy
	private KafkaTemplate<String, KafkaMessage> kafkaTemplate;

	public void sendEmail(Email email) {
		EmailValidate.Validate(email);

		KafkaMessage<Email> msg = new KafkaMessage(email);
		kafkaTemplate.send("xunmall-mail","mail-key-" + System.currentTimeMillis(), msg);
	}
}
