package com.xunmall.email.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;


import com.xunmall.base.constants.EmailType;
import lombok.Data;

@Data
public class Email implements Serializable {

	private static final long serialVersionUID = -6956927950658231103L;

	// 发件人
	private String sender;

	// 收件人
	@NotNull(message = "收件人不能为空")
	private String addressees;

	// 抄送人
	private String[] cc;

	// 安送人
	private String bcc;

	//邮件类型
	@NotNull(message = "邮件类型不能为空")
	private EmailType type;

	// 邮件主题
	@NotNull(message = "邮件主题不能为空")
	private String subject;

	// 邮件内容
	@NotNull(message = "邮件内容不能为空")
	private String content;

	/**
	 * 附件名称
	 */
	private String affixName;

	// 附件内容
	private byte[] affixContent;
}