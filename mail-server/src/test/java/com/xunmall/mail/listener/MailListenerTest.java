package com.xunmall.mail.listener;

import com.xunmall.base.constants.EmailType;
import com.xunmall.email.dto.Email;
import com.xunmall.email.service.EmailService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by Gimgoog on 2018/6/29.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MailListenerTest {

    @Autowired
    private EmailService emailService;

    @Test
    public void testSendMessage() throws InterruptedException, IOException {
        Email email = createEmailDto();
        ExecutorService executorService = Executors.newFixedThreadPool(20);
        CountDownLatch countDownLatch = new CountDownLatch(10);
        for (int i = 0; i < 10; i++) {
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        countDownLatch.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    emailService.sendEmail(email);
                }
            });
            countDownLatch.countDown();
        }
        executorService.shutdown();
        executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
    }

    private Email createEmailDto() throws IOException {
        Email email = new Email();
        email.setSender("wangyanjing");
        email.setAddressees("owen5263@qq.com");
        email.setType(EmailType.TEXT);
        email.setSubject("subject attchment");
        email.setContent("content <br> test 11111111 <br> test 22222222");
        return email;
    }

}