package com.xunmall.mail.component.operation.service;

import com.xunmall.base.dto.Result;
import com.xunmall.email.dto.Email;
import com.xunmall.mail.constan.enums.SendStatus;
import org.springframework.stereotype.Service;

@Service
public interface OperationRecordService {

	Result<String> addOperationRecord(Email email, SendStatus status);

}
