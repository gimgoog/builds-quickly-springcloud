package com.xunmall.common.entity;

import com.xunmall.base.entity.ICurrentUser;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Entity
@Table(name = "tn_uc_operation")
@Data
public class Operation implements ICurrentUser, Serializable {
    @Column
    private String name;
    @Column
    private String memo;

}
