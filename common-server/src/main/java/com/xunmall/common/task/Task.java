package com.xunmall.common.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import java.util.Random;
import java.util.concurrent.Future;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Component
@Slf4j
public class Task {

    private static Random random = new Random();

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    public void doOneTask() throws Exception {
        System.out.println("开始任务一:");
        Long start = System.currentTimeMillis();
        Thread.sleep(random.nextInt(1000));
        Long end = System.currentTimeMillis();
        System.out.println("结束任务一,耗时" + (end - start) + "毫秒！");
    }

    public void doTwoTask() throws Exception {
        System.out.println("开始任务二:");
        Long start = System.currentTimeMillis();
        Thread.sleep(random.nextInt(1000));
        Long end = System.currentTimeMillis();
        System.out.println("结束任务二,耗时" + (end - start) + "毫秒！");
    }

    public void doThressTask() throws Exception {
        System.out.println("开始任务三:");
        Long start = System.currentTimeMillis();
        Thread.sleep(random.nextInt(1000));
        Long end = System.currentTimeMillis();
        System.out.println("结束任务三,耗时" + (end - start) + "毫秒！");
    }

    @Async("taskExecutor")
    public Future<String> doOneAsyncTask() throws Exception {
        log.info("开始任务一:");
        Long start = System.currentTimeMillis();
        Thread.sleep(random.nextInt(1000));
        Long end = System.currentTimeMillis();
        log.info("结束任务一,耗时" + (end - start) + "毫秒！");
        return new AsyncResult<>("任务一完成");
    }

    @Async("taskExecutor")
    public Future<String> doTwoAsyncTask() throws Exception {
        log.info("开始任务二:");
        Long start = System.currentTimeMillis();
        Thread.sleep(random.nextInt(1000));
        Long end = System.currentTimeMillis();
        log.info("结束任务二,耗时" + (end - start) + "毫秒！");
        return new AsyncResult<>("任务二完成");
    }

    @Async("taskExecutor")
    public Future<String> doThressAsyncTask() throws Exception {
        log.info("开始任务三:");
        Long start = System.currentTimeMillis();
        Thread.sleep(random.nextInt(1000));
        Long end = System.currentTimeMillis();
        log.info("结束任务三,耗时" + (end - start) + "毫秒！");
        return new AsyncResult<>("任务三完成");
    }

    @Async("taskExecutor")
    public Future<String> doOneAsyncJob() throws Exception {
        log.info("开始任务一:");
        Long start = System.currentTimeMillis();
        log.info(stringRedisTemplate.randomKey());
        Long end = System.currentTimeMillis();
        log.info("结束任务一,耗时" + (end - start) + "毫秒！");
        return new AsyncResult<>("任务一完成");
    }

    @Async("taskExecutor")
    public Future<String> doTwoAsyncJob() throws Exception {
        log.info("开始任务二:");
        Long start = System.currentTimeMillis();
        log.info(stringRedisTemplate.randomKey());
        Long end = System.currentTimeMillis();
        log.info("结束任务二,耗时" + (end - start) + "毫秒！");
        return new AsyncResult<>("任务二完成");
    }

    @Async("taskExecutor")
    public Future<String> doThressAsyncJob() throws Exception {
        log.info("开始任务三:");
        Long start = System.currentTimeMillis();
        log.info(stringRedisTemplate.randomKey());
        Long end = System.currentTimeMillis();
        log.info("结束任务三,耗时" + (end - start) + "毫秒！");
        return new AsyncResult<>("任务三完成");
    }

}
