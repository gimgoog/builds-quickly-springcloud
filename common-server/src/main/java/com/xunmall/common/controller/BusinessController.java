package com.xunmall.common.controller;

import com.xunmall.base.annotation.AutoIdempotent;
import com.xunmall.base.dto.Result;
import com.xunmall.base.util.JsonUtils;
import com.xunmall.base.util.StringUtils;
import com.xunmall.common.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: BusinessController
 * @Description: TODO
 * @author: WangYanjing
 * @date: 2020年04月14日 10:27
 */
@RestController
public class BusinessController {

    @Autowired
    private TokenService tokenService;

    @RequestMapping("/get/token")
    public String getToken(){
        String token = tokenService.createToken();
        if(StringUtils.isNotEmpty(token)){
            Result result = new Result();
            result.setCode(200);
            result.setMessage("SUCCESS");
            result.setData(token);
            return JsonUtils.toJson(result);
        }
        return JsonUtils.toJson(Result.ofParamsError("参数有误"));
    }

    @RequestMapping("/test/idempotent")
    @AutoIdempotent
    public String testIdempotent(){
        return JsonUtils.toJson(Result.ofSuccess("SUCCESS"));
    }






}
