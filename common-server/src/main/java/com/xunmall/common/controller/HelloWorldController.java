package com.xunmall.common.controller;

import com.xunmall.base.controller.BaseController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Controller
@RequestMapping("/hello")
@Slf4j
public class HelloWorldController extends BaseController {

    @RequestMapping(value = "/main", method = RequestMethod.GET)
    public String main() {
        HttpSession session = getSession();
        String username = (String) session.getAttribute("username");
        // sessionId不为空
        if (null != username) {
            System.out.println("main username:" + username);
            return "main";
        } else { // sessionId为空
            return "redirect:/hello/login";
        }
    }


    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {
        return "login";
    }

    @RequestMapping(value = "/doLogin", method = RequestMethod.POST)
    public String doLogin(String username) {
        System.out.println("I do real login here");
        HttpSession session = getSession();
        session.setAttribute("username", username);
        System.out.println("login username:" + username);
        return "redirect:/hello/main";
    }
}
