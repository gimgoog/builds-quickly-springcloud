package com.xunmall.common.controller;

import com.xunmall.base.controller.BaseController;
import com.xunmall.common.entity.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Slf4j
@RestController
@RequestMapping("/ribbon")
public class ConsumerController extends BaseController {

    @GetMapping("/getInfo")
    public String getInfo() {
        Operation operation = new Operation();
        log.info("服务正常提供！" + operation.currentUserId());
        return operation.currentUserId();
    }

    @GetMapping("/getSession")
    public String getRemoteSession() {
        HttpSession session = getSession();
        String username = (String) session.getAttribute("username");
        System.out.println("client username:" + username);
        return username;
    }
}
