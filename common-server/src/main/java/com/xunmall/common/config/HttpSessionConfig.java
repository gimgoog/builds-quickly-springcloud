package com.xunmall.common.config;

import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@EnableRedisHttpSession
public class HttpSessionConfig {
}
