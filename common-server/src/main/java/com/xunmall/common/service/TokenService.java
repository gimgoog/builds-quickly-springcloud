package com.xunmall.common.service;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by wangyanjing on 2020/4/13.
 */
public interface TokenService {

    /**
     * 创建token
     * @return
     */
    String createToken();

    /**
     * 验证token
     * @param request
     * @return
     * @throws Exception
     */
    boolean checkToken(HttpServletRequest request) throws Exception;
}
