package com.xunmall.study;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Gimgoog on 2018/5/3.
 */
@Data
public class User implements Serializable {
    private String username;
    private String address;
    private String email;

    public User(){
        super();
    }

    public User(String username,String address, String email){
        this.username = username;
        this.address = address;
        this.email = email;
    }
}
