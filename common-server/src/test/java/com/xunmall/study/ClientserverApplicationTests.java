package com.xunmall.study;

import com.xunmall.common.task.Task;
import lombok.SneakyThrows;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.Future;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ClientserverApplicationTests {

    @Autowired
    private Task task;


    @Test
    public void contextLoads() {
    }

    @Test
    public void testSynTask() throws Exception {
        task.doOneTask();
        task.doTwoTask();
        task.doThressTask();
    }

    @Test
    public void testAsynTask() throws Exception {
        Long start = System.currentTimeMillis();

        Future<String> futureOne = task.doOneAsyncTask();
        Future<String> futureTwo = task.doTwoAsyncTask();
        Future<String> futureThree = task.doThressAsyncTask();

        while (true) {
            if (futureOne.isDone() && futureTwo.isDone() && futureThree.isDone()) {
                break;
            }
            Thread.sleep(1000);
        }

        Long end = System.currentTimeMillis();

        System.out.println("程序执行完成，耗时：" + (end - start) + "毫秒！");
    }


    @Test
    public void testAsyncPoolConfig() throws Exception {
        task.doOneAsyncTask();
        task.doTwoAsyncTask();
        task.doThressAsyncTask();
        Thread.currentThread().join();
    }

    @Test
    @SneakyThrows
    public void testAsyncPoolShutdown() {
        for (int i = 0; i < 10000; i++) {
            task.doOneAsyncJob();
            task.doTwoAsyncJob();
            task.doThressAsyncJob();
            if (i == 9999) {
                System.exit(0);
            }
        }
    }

}
