package com.xunmall.web.aop;

import lombok.Data;

/**
 * @author wangyj03@zenmen.com
 * @description
 * @date 2020/11/11 9:44
 */
@Data
public class RequestInfo {
    private String ip;
    private String url;
    private String httpMethod;
    private String classMethod;
    private Object requestParams;
    private Object result;
    private Long timeCost;
}
