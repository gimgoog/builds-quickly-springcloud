package com.xunmall.web.mapper;

import com.xunmall.web.entity.UserDO;
import com.xunmall.web.model.vo.UserVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface UserDOMapper extends Mapper<UserDO> {
    UserDO getByLoginName(String loginName);

    List<UserDO> findUserListByCondition(@Param("userVO") UserVO userVO);
}