package com.xunmall.web.mapper;

import com.xunmall.web.entity.UserRoleDO;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface UserRoleDOMapper extends Mapper<UserRoleDO> {
}