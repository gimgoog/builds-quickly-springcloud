package com.xunmall.web.mapper;

import com.xunmall.web.entity.RolePermissionDO;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface RolePermissionDOMapper extends Mapper<RolePermissionDO> {
}