package com.xunmall.web.mapper;

import com.xunmall.web.entity.OfficialAccountDO;
import com.xunmall.web.model.vo.WeChatVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * @Author: WangYanjing
 * @Date: 2019/1/22 13:16
 * @Description:
 */
@Repository
public interface OfficialAccountDOMapper extends Mapper<OfficialAccountDO> {
    List<OfficialAccountDO> findOfficialAccountByCondition(@Param("vo") WeChatVO weChatVO);

    OfficialAccountDO findOfficialAccountByChannel(@Param("channel")String channel);
}
