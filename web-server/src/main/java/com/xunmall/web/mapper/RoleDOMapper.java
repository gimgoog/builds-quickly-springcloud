package com.xunmall.web.mapper;

import com.xunmall.web.entity.RoleDO;
import com.xunmall.web.model.vo.RoleVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface RoleDOMapper extends Mapper<RoleDO> {
    List<RoleDO> findRoleListByUserId(String userId);

    List<RoleDO> findRoleListByCondition(@Param("roleVO") RoleVO roleVO);

    RoleDO findRoleByName(@Param("roleName") String roleName);
}