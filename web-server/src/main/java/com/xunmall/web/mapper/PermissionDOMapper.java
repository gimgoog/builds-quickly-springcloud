package com.xunmall.web.mapper;

import com.xunmall.web.entity.PermissionDO;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface PermissionDOMapper extends Mapper<PermissionDO> {
}