package com.xunmall.web.service;


import com.xunmall.base.dto.Result;
import com.xunmall.web.entity.RoleDO;
import com.xunmall.web.model.PageResult;
import com.xunmall.web.model.dto.RoleDTO;
import com.xunmall.web.model.vo.RoleVO;

import java.util.List;

/**
 * Created by wangyanjing on 2018/11/2.
 */
public interface RoleService {
    List<RoleDO> getRoleListByUserId(String userId);

    PageResult findRoleListByCondition(RoleVO roleVO);

    Result saveRole(RoleDTO roleDTO);

    Result findRoleById(String id);

    Result updateRole(RoleVO roleVO);

    Result deleteUserById(String id);
}
