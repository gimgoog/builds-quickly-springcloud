package com.xunmall.web.service.impl;
import com.xunmall.base.dto.Result;
import com.xunmall.base.page.Pages;
import com.xunmall.base.page.PagingModel;
import com.xunmall.base.util.RandomUtils;
import com.xunmall.web.entity.UserDO;
import com.xunmall.web.mapper.UserDOMapper;
import com.xunmall.web.model.PageResult;
import com.xunmall.web.model.vo.UserVO;
import com.xunmall.web.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDOMapper userDOMapper;

    @Override
    public Result select() {
        List<UserDO> list = userDOMapper.selectAll();
        return Result.ofSuccess(list);
    }

    @Override
    public Result saveUser(UserDO info) {
        UserDO userDO = userDOMapper.getByLoginName(info.getLoginName());
        if (userDO != null) {
            return Result.ofParamsError("登陆名称已存在，请重新命名");
        }
        Md5PasswordEncoder encoder = new Md5PasswordEncoder();
        info.setSalt(RandomUtils.generateString(5));
        info.setPassword(encoder.encodePassword(info.getPassword(), info.getSalt()));
        info.setStatus(info.getStatus() == null ? 1 : info.getStatus());
        info.buildForInsert();
        int flag = userDOMapper.insertSelective(info);
        return Result.ofSuccess(flag);
    }

    @Override
    public Result getByLoginName(String username) {
        UserDO userDO = userDOMapper.getByLoginName(username);
        return Result.ofSuccess(userDO);
    }

    @Override
    public PageResult findUserListByCondition(UserVO userVO) {
        PagingModel pagingModel = Pages.query(userVO, () -> userDOMapper.findUserListByCondition(userVO));
        PageResult pageResult = new PageResult();
        pageResult.setList(pagingModel.getResults());
        pageResult.setPageCurrent(pagingModel.getPageIndex());
        pageResult.setPageSize(pagingModel.getPageSize());
        pageResult.setTotal((int) pagingModel.getTotal());
        return pageResult;
    }

    @Override
    public Result updateUser(UserDO userDO) {
        UserDO oldData = userDOMapper.selectByPrimaryKey(userDO.getUuid());
        if (oldData == null) {
            return Result.ofParamsError("查询数据不存在");
        }
        oldData.setLoginName(StringUtils.isNotBlank(userDO.getLoginName()) ? userDO.getLoginName() : oldData.getLoginName());
        oldData.setUserName(StringUtils.isNotBlank(userDO.getUserName()) ? userDO.getUserName() : oldData.getLoginName());
        oldData.setMobile(StringUtils.isNotBlank(userDO.getMobile()) ? userDO.getMobile() : oldData.getMobile());
        Md5PasswordEncoder encoder = new Md5PasswordEncoder();
        oldData.setPassword(encoder.encodePassword(userDO.getPassword(), oldData.getSalt()));
        int flag = userDOMapper.updateByPrimaryKey(oldData);
        return Result.ofSuccess(flag);
    }

    @Override
    public Result findUserById(String id) {
        UserDO userDO = userDOMapper.selectByPrimaryKey(id);
        if (userDO == null) {
            return Result.ofParamsError("查询数据不存在");
        }
        return Result.ofSuccess(userDO);
    }

    @Override
    public Result deleteUserById(String id) {
        UserDO userDO = userDOMapper.selectByPrimaryKey(id);
        if (userDO == null) {
            return Result.ofParamsError("查询数据不存在");
        }
        return Result.ofSuccess(userDOMapper.deleteByPrimaryKey(id));
    }

    @Override
    public Result findUserList() {
        List<UserDO> list = userDOMapper.selectAll();
        return Result.ofSuccess(list);
    }

    @Override
    public Result find(String uid) {
        UserDO userDO = userDOMapper.selectByPrimaryKey(uid);
        return Result.ofSuccess(userDO);
    }

}
