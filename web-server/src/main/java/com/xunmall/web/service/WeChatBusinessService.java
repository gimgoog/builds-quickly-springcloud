package com.xunmall.web.service;

import com.xunmall.base.dto.Result;
import com.xunmall.web.model.PageResult;
import com.xunmall.web.model.dto.OfficialAccountDTO;
import com.xunmall.web.model.vo.WeChatVO;

/**
 * @Author: WangYanjing
 * @Date: 2019/1/22 13:31
 * @Description:
 */
public interface WeChatBusinessService {
    PageResult findOfficialAccountByCondition(WeChatVO weChatVO);

    Result saveOfficialAccount(OfficialAccountDTO officialAccountDTO);
}
