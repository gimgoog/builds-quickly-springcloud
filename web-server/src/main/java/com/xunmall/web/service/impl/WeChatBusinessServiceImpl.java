package com.xunmall.web.service.impl;

import com.xunmall.base.dto.Result;
import com.xunmall.base.page.Pages;
import com.xunmall.base.page.PagingModel;
import com.xunmall.base.util.BeanCopyUtils;
import com.xunmall.web.entity.OfficialAccountDO;
import com.xunmall.web.mapper.OfficialAccountDOMapper;
import com.xunmall.web.model.PageResult;
import com.xunmall.web.model.dto.OfficialAccountDTO;
import com.xunmall.web.model.vo.WeChatVO;
import com.xunmall.web.service.WeChatBusinessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: WangYanjing
 * @Date: 2019/1/22 13:31
 * @Description:
 */
@Service
public class WeChatBusinessServiceImpl implements WeChatBusinessService {

    @Autowired
    private OfficialAccountDOMapper officialAccountDOMapper;

    @Override
    public PageResult findOfficialAccountByCondition(WeChatVO weChatVO) {
        PagingModel pagingModel = Pages.query(weChatVO, () -> officialAccountDOMapper.findOfficialAccountByCondition(weChatVO));
        PageResult pageResult = new PageResult();
        pageResult.setList(pagingModel.getResults());
        pageResult.setPageCurrent(pagingModel.getPageIndex());
        pageResult.setPageSize(pagingModel.getPageSize());
        pageResult.setTotal((int) pagingModel.getTotal());
        return pageResult;
    }

    @Override
    public Result saveOfficialAccount(OfficialAccountDTO officialAccountDTO) {
        OfficialAccountDO officialAccountDO = officialAccountDOMapper.findOfficialAccountByChannel(officialAccountDTO.getChannel());
        if (officialAccountDO != null) {
            return Result.ofParamsError("该渠道已存在对应的公众号配置");
        }
        officialAccountDO = new OfficialAccountDO();
        BeanCopyUtils.copyProperties(officialAccountDTO, officialAccountDO);
        officialAccountDO.buildForInsert();
        int flag = officialAccountDOMapper.insertSelective(officialAccountDO);
        return flag > 0 ? Result.ofSuccess(flag) : Result.ofParamsError("保存数据出现异常");
    }
}
