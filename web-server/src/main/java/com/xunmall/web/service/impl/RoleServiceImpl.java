package com.xunmall.web.service.impl;
import com.xunmall.base.dto.Result;
import com.xunmall.base.page.Pages;
import com.xunmall.base.page.PagingModel;
import com.xunmall.web.entity.RoleDO;
import com.xunmall.web.mapper.RoleDOMapper;
import com.xunmall.web.model.PageResult;
import com.xunmall.web.model.dto.RoleDTO;
import com.xunmall.web.model.vo.RoleVO;
import com.xunmall.web.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by wangyanjing on 2018/11/2.
 */
@Service
@Transactional
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDOMapper roleDOMapper;

    @Override
    public List<RoleDO> getRoleListByUserId(String userId) {
        return roleDOMapper.findRoleListByUserId(userId);
    }

    @Override
    public PageResult findRoleListByCondition(RoleVO roleVO) {
        PagingModel pagingModel = Pages.query(roleVO, () -> roleDOMapper.findRoleListByCondition(roleVO));
        PageResult pageResult = new PageResult();
        pageResult.setList(pagingModel.getResults());
        pageResult.setPageCurrent(pagingModel.getPageIndex());
        pageResult.setPageSize(pagingModel.getPageSize());
        pageResult.setTotal((int) pagingModel.getTotal());
        return pageResult;
    }

    @Override
    public Result saveRole(RoleDTO roleDTO) {
        RoleDO roleDO = roleDOMapper.findRoleByName(roleDTO.getRoleName());
        if (roleDO != null) {
            return Result.ofParamsError("角色名称已存在，请重新命名");
        }
        roleDO = new RoleDO();
        roleDO.setRoleName(roleDTO.getRoleName());
        roleDO.setRoleCode(roleDTO.getRoleCode());
        roleDO.setRoleLevel(roleDTO.getRoleLevel());
        roleDO.setDescription(roleDTO.getDescription());
        roleDO.buildForInsert();
        int flag = roleDOMapper.insertSelective(roleDO);
        return Result.ofSuccess(flag);
    }

    @Override
    public Result findRoleById(String id) {
        RoleDO roleDO = roleDOMapper.selectByPrimaryKey(id);
        if (roleDO == null) {
            return Result.ofParamsError("查询不到数据");
        }
        return Result.ofSuccess(roleDO);
    }

    @Override
    public Result updateRole(RoleVO roleVO) {
        RoleDO oldData = roleDOMapper.selectByPrimaryKey(roleVO.getId());
        if (oldData == null) {
            return Result.ofParamsError("查询数据不存在");
        }
        oldData.setRoleName(roleVO.getRoleName());
        oldData.setRoleCode(roleVO.getRoleCode());
        oldData.setRoleLevel(roleVO.getRoleLevel());
        oldData.setDescription(roleVO.getDescription());
        oldData.buildForUpdate();
        int flag = roleDOMapper.updateByPrimaryKey(oldData);
        return Result.ofSuccess(flag);
    }

    @Override
    public Result deleteUserById(String id) {
        RoleDO roleDO = roleDOMapper.selectByPrimaryKey(id);
        if (roleDO == null) {
            return Result.ofParamsError("查询数据不存在");
        }
        return Result.ofSuccess(roleDOMapper.deleteByPrimaryKey(id));
    }
}
