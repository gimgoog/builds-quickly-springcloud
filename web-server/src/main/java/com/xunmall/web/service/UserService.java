package com.xunmall.web.service;

import com.xunmall.base.dto.Result;
import com.xunmall.web.entity.UserDO;
import com.xunmall.web.model.PageResult;
import com.xunmall.web.model.vo.UserVO;

public interface UserService {

    Result select();

    Result find(String uid);

    Result getByLoginName(String username);

    PageResult findUserListByCondition(UserVO userVO);

    Result saveUser(UserDO userDO);

    Result updateUser(UserDO userDO);

    Result findUserById(String id);

    Result deleteUserById(String id);

    Result findUserList();
}
