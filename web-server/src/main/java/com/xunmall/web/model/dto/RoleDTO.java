package com.xunmall.web.model.dto;

import com.xunmall.base.page.PageInfo;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * Created by wangyanjing on 2018/12/14.
 */
@Data
public class RoleDTO extends PageInfo {
    private String id;
    @NotBlank(message = "角色名称不能为空")
    private String roleName;
    @NotBlank(message = "角色代码不能为空")
    private String roleCode;
    @NotNull(message = "角色等级不能为空")
    private Integer roleLevel;
    private String description;
}
