package com.xunmall.web.model.vo;

import com.xunmall.base.page.PageInfo;
import lombok.Data;

/**
 * Created by wangyanjing on 2018/12/14.
 */
@Data
public class RoleVO extends PageInfo {
    private String id;
    private String roleName;
    private String roleCode;
    private Integer roleLevel;
    private String description;
}
