package com.xunmall.web.model.dto;

import com.xunmall.base.page.PageInfo;
import lombok.Data;

/**
 * Created by wangyanjing on 2018/11/2.
 */
@Data
public class UserDTO extends PageInfo {
    private String id;
    private String userName;
    private String loginName;
    private String password;
    private String sex;
    private String mobile;
    private String email;
}
