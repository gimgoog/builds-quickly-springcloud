package com.xunmall.web.model.vo;

import com.xunmall.base.page.PageInfo;
import lombok.Data;

/**
 * @Author: WangYanjing
 * @Date: 2019/1/22 13:38
 * @Description:
 */
@Data
public class WeChatVO extends PageInfo {
    private String channel;
}
