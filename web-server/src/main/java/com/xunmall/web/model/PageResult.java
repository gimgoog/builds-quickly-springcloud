package com.xunmall.web.model;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wangyanjing on 2018/12/7.
 */
@Data
public class PageResult<T> implements Serializable {
    private Integer pageCurrent = 1;                        //当前页索引
    private Integer pageSize = 20;                          //每页记录数
    private Integer total;                                  //总记录数
    private Map<String, Object> param = new HashMap<>();    //传入的参数，param和where只用一个
    private String where;                                   //where条件字符串，where和param只用一个
    private String orderField;                              //排序字段
    private String orderDirection = "ASC";                  //排序方向，升序or降序
    private List<T> list = new ArrayList<>();              //页面数据
    private Map<String, Object> ext = new HashMap<>();
}