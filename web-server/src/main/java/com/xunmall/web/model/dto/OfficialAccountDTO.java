package com.xunmall.web.model.dto;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;

/**
 * @Author: WangYanjing
 * @Date: 2019/1/22 13:49
 * @Description:
 */
@Data
public class OfficialAccountDTO implements Serializable {
    private Integer type;
    private String wechat;
    private String openId;
    private String nickName;
    private String headUrl;
    @NotBlank(message = "公众号AppId不能为空")
    private String appId;
    @NotBlank(message = "公众号AppSecret不能为空")
    private String appSecret;
    private String token;
    private String aesKey;
    private Integer followMode;
    private String subscribeUrl;
    private String kefuQrcodeUrl;
    private String channel;
}
