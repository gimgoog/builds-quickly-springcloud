package com.xunmall.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

@SpringBootApplication(scanBasePackages = {"com.xunmall"})
@EnableFeignClients(basePackages = {"com.xunmall"})
@EnableEurekaClient
@Slf4j
public class WebApplication implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(WebApplication.class, args);
    }

    @Override
    public void run(String... arg0) throws Exception {
        log.info("gim-web start OK");
    }
}