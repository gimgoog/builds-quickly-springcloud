package com.xunmall.web.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


@Slf4j
public class BaseWebController {

    public Map<String, Object> jsonReturn(int statusCode) {
        Map<String, Object> jsonObj = new HashMap<String, Object>();
        if (statusCode == 200) {
            jsonObj.put("statusCode", "200");
            jsonObj.put("message", "操作成功");
        } else if (statusCode == 300) {
            jsonObj.put("statusCode", "300");
            jsonObj.put("message", "操作失败，请重试");
        }
        jsonObj.put("closeCurrent", true);
        return jsonObj;
    }

    public Map<String, Object> jsonReturn(int statusCode, boolean closeCurrent) {
        Map<String, Object> jsonObj = new HashMap<String, Object>();
        if (statusCode == 200) {
            jsonObj.put("statusCode", "200");
            jsonObj.put("message", "操作成功");
        } else if (statusCode == 300) {
            jsonObj.put("statusCode", "300");
            jsonObj.put("message", "操作失败，请重试");
        }
        jsonObj.put("closeCurrent", closeCurrent);
        return jsonObj;
    }

    public Map<String, Object> jsonReturn(int statusCode, String msg) {
        Map<String, Object> jsonObj = new HashMap<String, Object>();
        if (statusCode == 200) {
            jsonObj.put("statusCode", "200");
            jsonObj.put("message", "操作成功 " + msg);
        } else if (statusCode == 300) {
            jsonObj.put("statusCode", "300");
            jsonObj.put("message", "操作失败:" + msg);
        }
        jsonObj.put("closeCurrent", true);
        return jsonObj;
    }

    public Map<String, Object> jsonReturn(int statusCode, String msg, boolean closeCurrent) {
        Map<String, Object> jsonObj = new HashMap<String, Object>();
        if (statusCode == 200) {
            jsonObj.put("statusCode", "200");
            jsonObj.put("message", "操作成功 " + msg);
        } else if (statusCode == 300) {
            jsonObj.put("statusCode", "300");
            jsonObj.put("message", "操作失败:" + msg);
        }
        jsonObj.put("closeCurrent", closeCurrent);
        return jsonObj;
    }

    /**
     * 将字符串以json形式返回
     */
    public void write(Object obj, HttpServletResponse response) {
        response.setCharacterEncoding("UTF-8");//转换编码方式
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.print(obj);
            out.flush();
        } catch (Exception e) {
            log.info(e.getMessage());
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    /**
     * 要跳转的页面
     */
    protected ModelAndView go(String path) {
        return new ModelAndView(path);
    }

    /**
     * 请求方式判断
     */
    public boolean isAjaxRequest(HttpServletRequest request) {
        if (!(request.getHeader("accept").indexOf("application/json") > -1
                || (request.getHeader("X-Requested-With") != null
                && request.getHeader("X-Requested-With").indexOf("XMLHttpRequest") > -1)
                || "XMLHttpRequest".equalsIgnoreCase(request.getParameter("X_REQUESTED_WITH")))) {
            return false;
        }
        return true;
    }

    /**
     * 获得唯一标识(目前用于验证表单提交唯一性)<br>
     */
    protected String getUUID() {
        return UUID.randomUUID().toString();
    }


    /**
     * 获取服务器ip地址
     *
     * @return
     */
    public String getServerIpAddress() {
        InetAddress address;
        String serverIpAddress = null;
        try {
            address = InetAddress.getLocalHost(); //获取的是本地的IP地址 //PC-20140317PXKX/192.168.0.121
            serverIpAddress = address.getHostAddress();//192.168.0.121
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return serverIpAddress;
    }

    public Object getAttribute(String attributeName) {
        return this.getRequest().getAttribute(attributeName);
    }

    public void setAttribute(String attributeName, Object object) {
        this.getRequest().setAttribute(attributeName, object);
    }

    public Object getSession(String attributeName) {
        return this.getRequest().getSession(true).getAttribute(attributeName);
    }

    public void setSession(String attributeName, Object object) {
        this.getRequest().getSession(true).setAttribute(attributeName, object);
    }

    public HttpServletRequest getRequest() {
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        return ((ServletRequestAttributes) ra).getRequest();
    }

    public HttpSession getSession() {
        return this.getRequest().getSession(true);
    }

    public String getParameter(String paraName) {
        return this.getRequest().getParameter(paraName);
    }

    public String getIpAddress() {
        String ip = this.getRequest().getRemoteAddr();
        return "0:0:0:0:0:0:0:1".equals(ip) ? "127.0.0.1" : ip;
    }
}
