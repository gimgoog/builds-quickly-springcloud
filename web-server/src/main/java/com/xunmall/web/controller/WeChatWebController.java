package com.xunmall.web.controller;

import com.xunmall.base.dto.Result;
import com.xunmall.web.entity.UserDO;
import com.xunmall.web.model.PageResult;
import com.xunmall.web.model.dto.OfficialAccountDTO;
import com.xunmall.web.model.vo.WeChatVO;
import com.xunmall.web.service.WeChatBusinessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

/**
 * @Author: WangYanjing
 * @Date: 2019/1/21 13:52
 * @Description:
 */
@Controller
@RequestMapping("/wechat")
public class WeChatWebController extends BaseWebController {

    @Autowired
    private WeChatBusinessService weChatBusinessService;

    private String viewPath = "wechat/";

    @RequestMapping(value = "/generator/base_list")
    public ModelAndView indexWeChatPage(WeChatVO weChatVO) {
        ModelAndView mv = new ModelAndView();
        PageResult pageResult = weChatBusinessService.findOfficialAccountByCondition(weChatVO);
        mv.setViewName(viewPath + "base_list");
        mv.addObject("pageResult",pageResult);
        return mv;
    }

    @RequestMapping(value = "/generator/base_add_page")
    public ModelAndView addPage() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName(viewPath + "base_add");
        return mv;
    }

    @RequestMapping(value = "/generator/add", method = RequestMethod.POST)
    @ResponseBody
    public Object addOfficialAccount(@Valid OfficialAccountDTO officialAccountDTO) {
        Result result = weChatBusinessService.saveOfficialAccount(officialAccountDTO);
        if (result.getSuccess()) {
            return jsonReturn(200);
        } else {
            return jsonReturn(300, result.getMessage());
        }
    }

}
