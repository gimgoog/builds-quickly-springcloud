package com.xunmall.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by wangyanjing on 2018/12/10.
 */
@Controller
public class LoginController {
    @RequestMapping(value = "/login_index", method = RequestMethod.GET)
    public String loginIndex(@RequestParam(value = "error", required = false) String error, HttpServletRequest request) {
        request.setAttribute("error", error);
        return "login";
    }

    @RequestMapping(value = {"/success", "/"})
    public String success() {
        return "main";
    }
}
