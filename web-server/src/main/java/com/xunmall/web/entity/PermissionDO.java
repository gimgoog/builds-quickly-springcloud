package com.xunmall.web.entity;

import com.xunmall.base.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

@Table(name = "sys_permission")
@Data
public class PermissionDO extends BaseEntity {

    @Column(name = "org_code")
    private String orgCode;

    /**
     * 配置名称
     */
    @Column(name = "org_name")
    private String orgName;

    @Column(name = "parent_id")
    private String parentId;

    private String url;

    /**
     * 选择权限时,是否显示该权限 2:不显示, 1显示
     */
    private Integer display;

    /**
     * 显示顺序
     */
    @Column(name = "sort_number")
    private Integer sortNumber;

    /**
     * 权限描述
     */
    private String descritp;

    /**
     * 有效：1；无效：0
     */
    private Integer status;
}