package com.xunmall.web.entity;

import com.xunmall.base.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * @Author: WangYanjing
 * @Date: 2019/1/22 11:54
 * @Description:
 */
@Table(name = "tn_wechat_channel")
@Data
public class OfficialAccountDO extends BaseEntity {
    @Column(name = "type")
    private Integer type;
    @Column(name = "wechat")
    private String wechat;
    @Column(name = "open_id")
    private String openId;
    @Column(name = "nick_name")
    private String nickName;
    @Column(name = "head_url")
    private String headUrl;
    @Column(name = "app_id")
    private String appId;
    @Column(name = "app_secret")
    private String appSecret;
    @Column(name = "token")
    private String token;
    @Column(name = "aes_key")
    private String aesKey;
    @Column(name = "follow_mode")
    private Integer followMode;
    @Column(name = "subscribe_url")
    private String subscribeUrl;
    @Column(name = "kefu_qrcode_url")
    private String kefuQrcodeUrl;
    @Column(name = "channel")
    private String channel;
}
