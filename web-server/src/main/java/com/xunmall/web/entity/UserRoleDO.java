package com.xunmall.web.entity;

import com.xunmall.base.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

@Table(name = "sys_user_role")
@Data
public class UserRoleDO extends BaseEntity {
    /**
     * 用户的主键ID
     */
    @Column(name = "user_id")
    private String userId;

    /**
     * 角色主键ID
     */
    @Column(name = "role_id")
    private String roleId;
}