package com.xunmall.web.entity;

import com.xunmall.base.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

@Table(name = "sys_user")
@Data
public class UserDO extends BaseEntity {
    /**
     * 用户名
     */
    @Column(name = "user_name")
    private String userName;

    /**
     * 登陆名称
     */
    @Column(name = "login_name")
    private String loginName;

    /**
     * 登录密码
     */
    private String password;

    /**
     * 加密盐
     */
    private String salt;

    private String sex;

    /**
     * 账号状态
     */
    private Integer status;

    private String mobile;

    /**
     * 登录账号
     */
    private String email;

    /**
     * 备注信息
     */
    private String remark;
}