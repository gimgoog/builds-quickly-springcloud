package com.xunmall.web.entity;

import com.xunmall.base.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

@Table(name = "sys_role")
@Data
public class RoleDO extends BaseEntity {

    /**
     * 角色编号
     */
    @Column(name = "ROLE_CODE")
    private String roleCode;

    /**
     * 角色名称
     */
    @Column(name = "ROLE_NAME")
    private String roleName;

    /**
     * 0-系统运维,2-集团管理员,4-公司管理员,16-普通角色。低级的角色不能给他人设置高等级的角色。
     */
    @Column(name = "ROLE_LEVEL")
    private Integer roleLevel;

    /**
     * 描述
     */
    @Column(name = "DESCRIPTION")
    private String description;
}