package com.xunmall.web.entity;

import com.xunmall.base.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

@Table(name = "sys_role_permission")
@Data
public class RolePermissionDO extends BaseEntity {
    /**
     * 角色主键ID
     */
    @Column(name = "role_id")
    private String roleId;

    /**
     * 权限主键ID
     */
    @Column(name = "permission_id")
    private String permissionId;

}