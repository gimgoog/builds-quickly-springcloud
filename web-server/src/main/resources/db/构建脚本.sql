DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
`uuid` char(22) NOT NULL,
`user_name` varchar(50) DEFAULT NULL COMMENT '用户名',
`login_name` varchar(50) DEFAULT NULL COMMENT '登陆名称',
`password` char(32) DEFAULT NULL COMMENT '登录密码',
`salt` char(32) DEFAULT NULL COMMENT '加密盐',
`sex` varchar(2) DEFAULT NULL,
`status` int(11) DEFAULT '1' COMMENT '账号状态',
`mobile` varchar(15) DEFAULT NULL,
`email` varchar(50) DEFAULT NULL COMMENT '登录账号',
`remark` varchar(255) DEFAULT '' COMMENT '备注信息',
`create_user` char(22) DEFAULT NULL COMMENT '创建者',
`update_user` char(22) DEFAULT NULL COMMENT '修改者',
`create_time` timestamp NOT NULL DEFAULT '2020-01-01 00:00:00' COMMENT '创建时间',
`update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
`delete_flag` tinyint(1) DEFAULT '0' COMMENT '删除状态（0-未删除，1-已删除）',
PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统用户表';

DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
`uuid` varchar(50) NOT NULL COMMENT '主键',
`user_id` char(22) DEFAULT NULL COMMENT '用户的主键ID',
`role_id` char(22) DEFAULT NULL COMMENT '角色主键ID',
`create_user` char(22) DEFAULT NULL COMMENT '创建者',
`update_user` char(22) DEFAULT NULL COMMENT '修改者',
`create_time` timestamp NOT NULL DEFAULT '2020-01-01 00:00:00' COMMENT '创建时间',
`update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
`delete_flag` tinyint(1) DEFAULT '0' COMMENT '删除状态（0-未删除，1-已删除）',
PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户角色关联表';

DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
`uuid` char(22) NOT NULL COMMENT '角色主键id',
`role_name` varchar(150) DEFAULT NULL COMMENT '角色名称',
`org_code` varchar(100) DEFAULT NULL,
`org_name` varchar(100) DEFAULT NULL COMMENT '配置名称',
`descript` varchar(128) DEFAULT NULL,
`sort_num`int(11) DEFAULT NULL COMMENT '排序号',
`is_system` int(11) DEFAULT '0' COMMENT '是否为系统默认角色, 1:是; 2:否',
`create_user` char(22) DEFAULT NULL COMMENT '创建者',
`update_user` char(22) DEFAULT NULL COMMENT '修改者',
`create_time` timestamp NOT NULL DEFAULT '2020-01-01 00:00:00' COMMENT '创建时间',
`update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
`delete_flag` tinyint(1) DEFAULT '0' COMMENT '删除状态（0-未删除，1-已删除）',
PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表';

DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission` (
`uuid` char(22) NOT NULL COMMENT '主键',
`role_id` varchar(50) DEFAULT NULL COMMENT '角色主键ID',
`permission_id` varchar(50) DEFAULT NULL COMMENT '权限主键ID',
`create_user` char(22) DEFAULT NULL COMMENT '创建者',
`update_user` char(22) DEFAULT NULL COMMENT '修改者',
`create_time` timestamp NOT NULL DEFAULT '2020-01-01 00:00:00' COMMENT '创建时间',
`update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
`delete_flag` tinyint(1) DEFAULT '0' COMMENT '删除状态（0-未删除，1-已删除）',
PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色权限关联表';

DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission` (
`uuid` char(22) NOT NULL COMMENT '权限主键id',
`org_code` varchar(100) DEFAULT NULL,
`org_name` varchar(100) DEFAULT NULL COMMENT '配置名称',
`parent_id` char(22) DEFAULT NULL,
`url` varchar(150) DEFAULT NULL,
`display` int(11) DEFAULT NULL COMMENT '选择权限时,是否显示该权限 2:不显示, 1显示',
`sort_number` int(11) DEFAULT NULL COMMENT '显示顺序',
`descritp` varchar(128) DEFAULT NULL COMMENT '权限描述',
`status` int(11) DEFAULT NULL COMMENT '有效：1；无效：0',
`create_user` char(22) DEFAULT NULL COMMENT '创建者',
`update_user` char(22) DEFAULT NULL COMMENT '修改者',
`create_time` timestamp NOT NULL DEFAULT '2020-01-01 00:00:00' COMMENT '创建时间',
`update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
`delete_flag` tinyint(1) DEFAULT '0' COMMENT '删除状态（0-未删除，1-已删除）',
PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限表';

