package com.xunmall.web.service.impl;

import com.xunmall.web.entity.UserDO;
import com.xunmall.web.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author wangyj03@zenmen.com
 * @description
 * @date 2020/11/11 10:32
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceImplTest {

    @Autowired
    private UserService userService;

    @Test
    public void testAddUser(){
        UserDO userDo = createUserDO();
        userService.saveUser(userDo);
    }

    private UserDO createUserDO() {
        UserDO userDO = new UserDO();
        userDO.setLoginName("wangyanjing");
        return userDO;
    }

}