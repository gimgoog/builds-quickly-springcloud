package com.xunmall.message.component.operation.service;

import com.xunmall.base.dto.Result;
import com.xunmall.message.constan.enums.SendStatus;
import com.xunmall.message.dto.MessagesForm;
import org.springframework.stereotype.Service;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Service
public interface OperationRecordService {
    Result<String> addMsgOperation(MessagesForm entity, SendStatus status, String result);
}
