package com.xunmall.message.component.message.service;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Data
@Component
@ConfigurationProperties(prefix = "message")
public class MessageProperties {
    private String serverIp;

    private String port;

    private String token;

    private String appId;

    private String sid;

    private String appToken;
}
