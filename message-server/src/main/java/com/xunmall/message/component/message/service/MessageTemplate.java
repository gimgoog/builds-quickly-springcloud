package com.xunmall.message.component.message.service;

import com.xunmall.message.dto.MessagesForm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Configuration
@Component
@Slf4j
public class MessageTemplate {

    @Autowired
    private MessageProperties messageProperties;

    public HashMap<String, Object> send(MessagesForm messagesForm) {
        HashMap<String, Object> result = new HashMap<>();
        //TODO 具体处理短信发送业务逻辑，并返回响应状态
        return result;
    }
}
