package com.xunmall.message.constan.dto;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description: 自定义响应状态码
 */
public enum GlobalErrorCode {
    CODE_FU_1(-1, "系统繁忙，请稍后再试"),
    CODE_200(200, "请求成功"),
    CODE_40000(40000, "请求失败"),
    CODE_40001(40001, "必填参数不能为空"),

    CODE_112300(112300, "接收短信的手机号码为空"),
    CODE_112301(112301, "短信正文为空"),
    CODE_112302(112302, "群发短信已暂停"),
    CODE_112303(112303, "应用未开通短信功能"),
    CODE_112304(112304, "短信内容的编码转换有误"),
    CODE_112305(112305, "应用未上线，短信接收号码外呼受限"),
    CODE_112306(112306, "接收模板短信的手机号码为空"),
    CODE_112307(112307, "模板短信模板ID为空"),
    CODE_112308(112308, "模板短信模板data参数为空"),
    CODE_112309(112309, "模板短信内容的编码转换有误"),
    CODE_112310(112310, "应用未上线，模板短信接收号码外呼受限"),
    CODE_112311(112311, "短信模板不存在"),
    CODE_000000(000000, "发送成功"),
    CODE_160000(160000, "系统错误"),
    CODE_160031(160031, "参数解析失败"),
    CODE_160032(160032, "短信模板无效"),
    CODE_160033(160033, "短信存在黑词"),
    CODE_160034(160034, "号码黑名单"),
    CODE_160035(160035, "短信下发内容为空"),
    CODE_160036(160036, "短信模板类型未知"),
    CODE_160037(160037, "短信内容长度限制"),
    CODE_160038(160038, "短信验证码发送过频繁"),
    CODE_160039(160039, "超出同模板同号天发送次数上限"),
    CODE_160040(160040, "验证码超出同模板同号码天发送上限"),
    CODE_160041(160041, "通知超出同模板同号码天发送上限"),
    CODE_160042(160042, "号码格式有误"),
    CODE_160043(160043, "应用与模板id不匹配"),
    CODE_160050(160050, "短信发送失败"),
    CODE_111318(111318, "语音验证码内容为空"),
    CODE_111319(111319, "语音验证码内容长度有误"),
    CODE_111320(111320, "语音验证码被叫电话号码为空"),
    CODE_111321(111321, "语音验证码被叫电话号码存在非法字符"),
    CODE_111322(111322, "语音验证码被叫电话号码位数不足"),
    CODE_111323(111323, "语音验证码被叫电话号码位数超长"),
    CODE_111324(111324, "应用未上线，语音验证码被叫号码外呼受");

    private int code;
    private String desc;

    GlobalErrorCode(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static GlobalErrorCode getDescByCode(int code) {
        for (GlobalErrorCode globalErrorCodeEnum : GlobalErrorCode.values()) {
            if (globalErrorCodeEnum.getCode() == code) {
                return globalErrorCodeEnum;
            }
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
