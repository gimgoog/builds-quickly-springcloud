package com.xunmall.message.constan.enums;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@JsonFormat(shape= JsonFormat.Shape.OBJECT)
public enum SendStatus {

	wait("完成"),
	
	finish("等待"),
	
	exception("异常");
	

	private String value;
	private String name;

    private SendStatus(String name) {
    	this.value= this.toString();
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
	public String getValue() {
		return value;
	}
 
    
    public static SendStatus formatEnum(String key){
    	
    	if(StringUtils.isBlank(key)){
    		return null;
    	}
    	try {
			return SendStatus.valueOf(key);
		} catch (Exception e) {
			return exception;
		}    	
    }
	
}