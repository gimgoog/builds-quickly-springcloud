package com.xunmall.message.component.message.service;

import com.xunmall.message.constan.enums.SendStatus;
import com.xunmall.message.MessageApplication;
import com.xunmall.message.component.operation.service.OperationRecordService;
import com.xunmall.message.dto.MessagesForm;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.stream.Stream;

/**
 * Created by Gimgoog on 2017/8/18.
 */
@SpringBootTest(classes = MessageApplication.class)
@RunWith(SpringJUnit4ClassRunner.class)
@Slf4j
public class MessageTemplateTest {

    @Autowired
    private MessageTemplate messageTemplate;
    @Autowired
    private OperationRecordService operationRecordService;

    @Test
    public void testSendMessage() throws Exception {
        try {
            MessagesForm messagesForm = createMessagesFormDto();
            Stream.of(messagesForm.getAddress()).forEach(item -> {
                log.info("prepare for send message to " + item);
            });
            HashMap<String, Object> result = messageTemplate.send(messagesForm);
            if ("000000".equals(result.get("statusCode"))) {
                operationRecordService.addMsgOperation(messagesForm, SendStatus.finish, "发送完成");
            } else {
                operationRecordService.addMsgOperation(messagesForm, SendStatus.exception, result.get("statusMsg").toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private MessagesForm createMessagesFormDto() {
        /** 短信
         * 模板id：69521
         * 模板格式：【云才网络】{1}是您的"班步"注册验证码,{2}分钟内使用有效.
         */
        MessagesForm form = new MessagesForm();
        form.setAddress("15021618549");
        form.setTemplateId("69521");
        form.setExpires(10);
        form.setContent("wangyanjing");
        form.setParams(new String[]{"6878", "30"});
        return form;
    }
}
