package com.xunmall.account.dto;

import lombok.Data;

@Data
public class Jcaptcha {
    private String jcaptchaId;
    private String jcaptchaCode;
}
