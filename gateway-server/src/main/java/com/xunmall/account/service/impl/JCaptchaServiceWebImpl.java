package com.xunmall.account.service.impl;

import com.octo.captcha.component.image.backgroundgenerator.BackgroundGenerator;
import com.octo.captcha.component.image.backgroundgenerator.UniColorBackgroundGenerator;
import com.octo.captcha.component.image.color.RandomRangeColorGenerator;
import com.octo.captcha.component.image.deformation.ImageDeformation;
import com.octo.captcha.component.image.deformation.ImageDeformationByFilters;
import com.octo.captcha.component.image.fontgenerator.FontGenerator;
import com.octo.captcha.component.image.fontgenerator.RandomFontGenerator;
import com.octo.captcha.component.image.textpaster.GlyphsPaster;
import com.octo.captcha.component.image.textpaster.TextPaster;
import com.octo.captcha.component.image.textpaster.glyphsdecorator.GlyphsDecorator;
import com.octo.captcha.component.image.textpaster.glyphsdecorator.RandomLinesGlyphsDecorator;
import com.octo.captcha.component.image.textpaster.glyphsvisitor.GlyphsVisitors;
import com.octo.captcha.component.image.textpaster.glyphsvisitor.HorizontalSpaceGlyphsVisitor;
import com.octo.captcha.component.image.textpaster.glyphsvisitor.RotateGlyphsRandomVisitor;
import com.octo.captcha.component.image.textpaster.glyphsvisitor.TranslateAllToRandomPointVisitor;
import com.octo.captcha.component.image.wordtoimage.DeformedComposedWordToImage;
import com.octo.captcha.component.image.wordtoimage.WordToImage;
import com.octo.captcha.component.word.wordgenerator.RandomWordGenerator;
import com.octo.captcha.component.word.wordgenerator.WordGenerator;
import com.octo.captcha.engine.image.ListImageCaptchaEngine;
import com.octo.captcha.image.gimpy.GimpyFactory;
import lombok.extern.slf4j.Slf4j;

import java.awt.*;
import java.awt.image.ImageFilter;

@Slf4j
public class JCaptchaServiceWebImpl extends ListImageCaptchaEngine {
    @Override
    protected void buildInitialFactories() {
        int minWordLength = 4;
        int maxWordLength = 4;
        int fontSize = 22;
        int imageWidth = 100;
        int imageHeight = 40;
        int[] R = new int[]{0, 0};
        int[] G = new int[]{0, 0};
        int[] B = new int[]{0, 0};

        WordGenerator words = new RandomWordGenerator("qwertyuipasdfghjklzxcvbnm123456789");
        FontGenerator font = new RandomFontGenerator(fontSize, fontSize,
                new Font[]{new Font("Arial", Font.PLAIN, fontSize)});
        BackgroundGenerator background = new UniColorBackgroundGenerator(imageWidth, imageHeight, Color.white);

        RandomRangeColorGenerator colors = new RandomRangeColorGenerator(R, G, B);
        GlyphsVisitors[] glyphVisitors = new GlyphsVisitors[]{
                new HorizontalSpaceGlyphsVisitor(7),
                new RotateGlyphsRandomVisitor(0.2),
                new TranslateAllToRandomPointVisitor(0.1, 0.1)
        };
        GlyphsDecorator[] glyphsDecorators = new GlyphsDecorator[]{
                new RandomLinesGlyphsDecorator(2.8, colors, 0.7, 4.2)};
        TextPaster textPaster = new GlyphsPaster(minWordLength, maxWordLength, colors,
                true, glyphVisitors, glyphsDecorators);

        ImageDeformation backDef = new ImageDeformationByFilters(new ImageFilter[]{});
        ImageDeformation textDef = new ImageDeformationByFilters(new ImageFilter[]{});
        ImageDeformation postDef = new ImageDeformationByFilters(new ImageFilter[]{});

        WordToImage word2image = new DeformedComposedWordToImage(font, background, textPaster,
                backDef, textDef, postDef);
        addFactory(new GimpyFactory(words, word2image));
    }
}