package com.xunmall.account.service;

import java.util.Map;

/**
 * Created by hezqi on 2017/7/3.
 */
public interface AccountService {
    void login(Map<String, Object> profile);
}