package com.xunmall.account.service.impl;

import com.xunmall.account.service.AccountService;
import com.xunmall.account.service.PermissionRemoteService;
import com.xunmall.account.service.RoleRemoteService;
import com.xunmall.account.service.UserRemoteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Created by hezqi on 2017/7/3.
 */
@Service
@Slf4j
public class AccountServiceImpl implements AccountService {
    @Autowired
    UserRemoteService userRemoteService;
    @Autowired
    RoleRemoteService roleRemoteService;
    @Autowired
    PermissionRemoteService permissionRemoteService;

    @Override
    public void login(Map<String, Object> profile) {

    }
}