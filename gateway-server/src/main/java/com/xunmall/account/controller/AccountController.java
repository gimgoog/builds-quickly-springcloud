package com.xunmall.account.controller;

import com.xunmall.account.dto.*;
import com.xunmall.account.service.*;
import com.xunmall.base.exception.UnauthorizedException;
import com.xunmall.base.util.StringUtils;
import com.xunmall.gateway.security.GlobalTokenServices;
import com.xunmall.security.GlobalSessionUtil;
import com.xunmall.security.SecUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/v1/ac/user")
@Slf4j
public class AccountController {
    @Value("${server.port}")
    private String port;

    @Autowired
    private AccountService accountService;
    @Autowired
    private UserRemoteService userRemoteService;
    @Autowired
    private GlobalTokenServices tokenServices;

    /**
     * 获取当前用户信息
     *
     * @return
     */
    @RequestMapping(value = "/login_user_info", method = RequestMethod.GET)
    public LoginUserInfo getLoginInfo() {
        SecUser secUser = GlobalSessionUtil.getUserInfo();
        if (secUser == null) {
            throw new UnauthorizedException("error.0011", this);
        }

        UserDto user = userRemoteService.getByUuid(secUser.getUuid()).getData();
        LoginUserInfo loginUserInfo = new LoginUserInfo();
        loginUserInfo.setUuid(secUser.getUuid());
        //loginUserInfo.setName(StringUtils.isBlank(user.getShowName()) ? user.getName() : user.getShowName());
        loginUserInfo.setMobile(secUser.getMobile());
        //loginUserInfo.setUserPhoto(user.getUserPhoto());
        loginUserInfo.setIsMultiCompany(GlobalSessionUtil.getMultiCompany());
        //loginUserInfo.setInitFlag(user.getInitFlag());
        List<String> permissions = new ArrayList<>();
        if (GlobalSessionUtil.getPermissions() != null) {
            permissions.addAll(GlobalSessionUtil.getPermissions());
        }
        List<String> roles = new ArrayList<>();
        if (GlobalSessionUtil.getRoles() != null) {
            roles.addAll(GlobalSessionUtil.getRoles());
        }
        loginUserInfo.setPermissions(permissions);
        loginUserInfo.setRoles(roles);

        return loginUserInfo;
    }

    /**
     * 登出
     *
     * @param request
     * @throws Exception
     */
    @ResponseBody
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public void logout(HttpServletRequest request, OAuth2Authentication authentication) throws Exception {
        if (authentication != null) {
            //注销令牌
            OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) authentication.getDetails();
            if (details != null) {
                tokenServices.revokeToken(details.getTokenValue());
            }
        }

        //清空session
        GlobalSessionUtil.removeProfile();

        SecurityContextHolder.clearContext();
    }
}