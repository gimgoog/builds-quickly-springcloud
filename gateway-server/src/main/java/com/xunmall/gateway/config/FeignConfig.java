package com.xunmall.gateway.config;

import feign.Request;
import feign.Retryer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignConfig {
    @Value("${ribbon.ConnectTimeout:0}")
    private int connectTimeout;
    @Value("${ribbon.ReadTimeout:0}")
    private int readTimeout;

    @Bean
    Request.Options feignOptions() {
        return new Request.Options(connectTimeout, readTimeout);
    }

    @Bean
    Retryer feignRetryer() {
        return Retryer.NEVER_RETRY;
    }
}
