package com.xunmall.gateway.controller;

import com.xunmall.gateway.enums.Limit;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicInteger;


/**
 * @author wangyanjing
 */
@RestController
@RequestMapping(value = "/v1/sys/limit")
public class LimiterController {

    private static final AtomicInteger ATOMIC_INTEGER = new AtomicInteger();

    @Limit(name = "resource", key = "test", prefix = "limit", period = 10, count = 10)
    @GetMapping("/test")
    public int testLimiter() {
        // 意味着10S内最多可以访问10次
        return ATOMIC_INTEGER.incrementAndGet();
    }
}