package com.xunmall.gateway;

import com.xunmall.gateway.security.SecurityMetadataSourceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @author wangyanjing
 */
@SpringBootApplication(scanBasePackages = {"com.xunmall"})
@EnableFeignClients(basePackages = {"com.xunmall"})
@EnableZuulProxy
@EnableEurekaClient
@Slf4j
public class GatewayApplication implements CommandLineRunner {


    public static void main(String[] args) {
        System.out.println("服务正在启动中。。。");
        SpringApplication.run(GatewayApplication.class, args);
        System.out.println("启动完成！");
    }

    @Autowired
    private  SecurityMetadataSourceService metadataSourceService;

    @Override
    public void run(String... args) throws Exception {
        //metadataSourceService.loadMetadataSource();
        log.info("gateway start OK");
    }
}
