package com.xunmall.gateway.interceptor;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.xunmall.base.constants.SecurityConstants;
import com.xunmall.base.interceptor.GlobalProcessIdFilter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

@Slf4j
public class TrackerZuulFilter extends ZuulFilter {
    @Override
    public String filterType() {
        return "route";
    }

    @Override
    public int filterOrder() {
        return -1;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        if (StringUtils.isNotEmpty(GlobalProcessIdFilter.getAccessToken())) {
            ctx.addZuulRequestHeader(SecurityConstants.ACCESS_TOKEN, GlobalProcessIdFilter.getAccessToken());
        }
        return null;
    }
}