package com.xunmall.gateway.interceptor;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by Gimgoog on 2017/12/25.
 */
@Slf4j
public class ErrorZuulFilter extends ZuulFilter {
    @Override
    public String filterType() {
        return "post";
    }

    @Override
    public int filterOrder() {
        return -1;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        log.info("This is a pre filter,it will throw a RunningException!");
        RequestContext context = RequestContext.getCurrentContext();
        try {
            doSomeThing();
        } catch (Exception ex) {
            context.set("error.status_code", HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            context.set("error.exception", ex);
        }
        return null;
    }

    private void doSomeThing() {
        //throw new RuntimeException("Exists some error...");
    }
}
