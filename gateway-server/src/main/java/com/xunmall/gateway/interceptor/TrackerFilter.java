package com.xunmall.gateway.interceptor;

import com.xunmall.base.interceptor.GlobalProcessIdFilter;
import com.xunmall.security.GlobalSessionUtil;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Fouction : 存入身份信息，用于accesslog
 */
public class TrackerFilter implements Filter {
    /**
     * 初始化
     *
     * @param filterConfig
     * @throws ServletException
     */
    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
        // left blank intentionally
    }

    /**
     * 过滤
     *
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(final ServletRequest req, final ServletResponse res, final FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) res;
        HttpServletRequest request = (HttpServletRequest) req;
        String token = GlobalSessionUtil.getToken();
        if (StringUtils.isNotEmpty(token)) {
            GlobalProcessIdFilter.setAccessToken(token);
        }
        String userUid = GlobalSessionUtil.getUserUuid();
        if (StringUtils.isNotEmpty(userUid)) {
            GlobalProcessIdFilter.setUserUuid(userUid);
        }
        filterChain.doFilter(request, response);
    }

    /**
     * do nothing
     */
    @Override
    public void destroy() {
        // left blank intentionally
    }
}
