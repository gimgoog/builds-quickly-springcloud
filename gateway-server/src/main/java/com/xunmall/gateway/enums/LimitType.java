package com.xunmall.gateway.enums;

/**
 * @ClassName: LimitType
 * @Description: TODO
 * @author: WangYanjing
 * @date: 2020年04月03日 14:34
 */
public enum LimitType {
    /**
     * 自定义key
     */
    CUSTOMER,
    /**
     * 根据请求者IP
     */
    IP;
}
