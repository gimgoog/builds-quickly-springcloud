package com.xunmall.gateway.exception;

import com.xunmall.base.exception.UnauthorizedException;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

public class RestTemplateErrorHandler implements ResponseErrorHandler {
    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        return response.getStatusCode().value() != 200;
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
//        ResponseEntity<MessageVo> error = (ResponseEntity<MessageVo>) decoder.decode(response, new TypeReference<ResponseEntity<MessageVo>>() {}.getType());

        int status = response.getStatusCode().value();
        switch (status) {
//                case 301:
//                    throw new MovedPermanentlyException(error.getBody(), methodKey);
//                case 400:
//                    throw new BadRequestException(error.getBody(), methodKey);
            case 401:
                throw new UnauthorizedException("error.gateway.0019", this);
//                case 403:
//                    throw new ForbiddenException(error.getBody(), methodKey);
//                case 404:
//                    throw new NotFoundException(error.getBody(), methodKey);
//                case 500:
//                    throw new BusinessException(error.getBody(), methodKey);
//                default:
//                    throw new BusinessException(error.getBody(), methodKey);
        }
    }
}
