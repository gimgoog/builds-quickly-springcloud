package com.xunmall.gateway.util;

import com.xunmall.base.util.JsonUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName: TokenLocalTest
 * @Description: TODO
 * @author: WangYanjing
 * @date: 2020年04月08日 16:12
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TokenLocalTest {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void testImageCode() throws Exception {
        String url = "/oauth/token";
        Map<String,String> param = new HashMap<>(16);
        param.put("grant_type","client_credentials");
        param.put("client_id","client_android");
        param.put("client_secret","client_secret");
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post(url)
                .content(JsonUtils.toJson(param))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED);
        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();
        System.out.println(mvcResult.getResponse().getStatus());
    }

}
