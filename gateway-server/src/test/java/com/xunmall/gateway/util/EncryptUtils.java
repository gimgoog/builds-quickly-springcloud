package com.xunmall.gateway.util;


import okhttp3.*;


public class EncryptUtils {

    private static OkHttpClient okHttpClient;

    public static void main(String[] args) throws Exception {
        okHttpClient = new OkHttpClient();
        String key = encrypt("as$#dfasdfad=");
        System.out.println(key);
        String value = decrypt(key);
        System.out.println(value);
    }

    private static String encrypt(String value) throws Exception {
        String url = "http://47.100.98.42:8888/encrypt";
        String credential = Credentials.basic("wgjf", "abc123!@#");
        RequestBody body = RequestBody.create(MediaType.parse("text/plain"), value.getBytes());
        Request request = new Request.Builder()
                .url(url)
                .header("Authorization", credential)
                .post(body)
                .build();
        Call call = okHttpClient.newCall(request);
        Response response = call.execute();
        ResponseBody responseBody = response.body();
        return responseBody.string();
    }

    private static String decrypt(String value) throws Exception {
        String url = "http://47.100.98.42:8888/decrypt";
        String credential = Credentials.basic("wgjf", "abc123!@#");
        RequestBody body = RequestBody.create(MediaType.parse("text/plain"), value.getBytes());
        Request request = new Request.Builder()
                .url(url)
                .header("Authorization", credential)
                .post(body)
                .build();
        Call call = okHttpClient.newCall(request);
        Response response = call.execute();
        ResponseBody responseBody = response.body();
        return responseBody.string();
    }
}
