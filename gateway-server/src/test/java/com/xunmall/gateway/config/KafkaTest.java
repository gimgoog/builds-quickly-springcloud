package com.xunmall.gateway.config;

import com.xunmall.base.dto.KafkaMessage;
import com.xunmall.base.util.RandomUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.SendResult;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by fanjun on 2017/5/31.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class KafkaTest {
    @Autowired
    KafkaTemplate<String, KafkaMessage> kafkaTemplate;

    @Test
    public void testKafkaMessage() throws InterruptedException {

        KafkaMessage<String> testMsg1 = new KafkaMessage(new HashMap<String, Object>() {{
            put("createTime", new Date());
        }}, RandomUtils.generateString(5));
        sendMessage(testMsg1);
        KafkaMessage<String> testMsg2 = new KafkaMessage(new HashMap<String, Object>() {{
            put("createTime", new Date());
        }}, RandomUtils.generateString(5));
        sendMessage(testMsg2);
        KafkaMessage<String> testMsg3 = new KafkaMessage(new HashMap<String, Object>() {{
            put("createTime", new Date());
        }}, RandomUtils.generateString(5));
        sendMessage(testMsg3);

        Thread.sleep(60000);
    }

    // 发送消息
    public void sendMessage(KafkaMessage message) {

        ListenableFuture<SendResult<String, KafkaMessage>> future = kafkaTemplate.send("banbu-v3",
                "key:" + System.currentTimeMillis(), message);

        future.addCallback(new ListenableFutureCallback<SendResult<String, KafkaMessage>>() {
            @Override
            public void onSuccess(SendResult<String, KafkaMessage> result) {
                log.info("sent message='{}' with topic={},pattition={},offset={},timestamp={},key={}",
                        result.getProducerRecord().value().getContent(),
                        result.getRecordMetadata().topic(), result.getRecordMetadata().partition(),
                        result.getRecordMetadata().offset(), result.getRecordMetadata().timestamp(),
                        result.getProducerRecord().key());
            }

            @Override
            public void onFailure(Throwable ex) {
                log.error("unable to send message='{}'", ex);
            }
        });
    }

    // 接受消息的几种方式
    @KafkaListener(topics = "banbu-v3")
    public void receive1(List<KafkaMessage> messages) {
        for (KafkaMessage message : messages) {
            log.info("receiver_1={}", message.getContent());
            System.out.println(message);
        }
    }

    @KafkaListener(topics = "banbu-v3")
    public void receive2(List<ConsumerRecord<String, KafkaMessage>> messages) {
        for (ConsumerRecord<String, KafkaMessage> msg : messages) {
            log.info("receiver_2 message={},topic={},pattition={},offset={},timestamp={},key={}",
                    msg.value().getContent(), msg.topic(), msg.partition(), msg.offset(), msg.timestamp()
                    , msg.key());
        }
    }

    @KafkaListener(topics = "banbu-v3")
    public void receive3(@Payload List<KafkaMessage> messages,
                         @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) List<Integer> keys,
                         @Header(KafkaHeaders.RECEIVED_PARTITION_ID) List<Integer> partitions,
                         @Header(KafkaHeaders.RECEIVED_TOPIC) List<String> topics,
                         @Header(KafkaHeaders.OFFSET) List<Long> offsets) {
        for (int i = 0; i < messages.size(); i++) {
            log.info("receiver_3 message={},topic={},pattition={},offset={},key={}",
                    messages.get(i).getContent(), topics.get(i), partitions.get(i), offsets.get(i), keys.get(i));
        }
    }
}
