package com.xunmall.gateway.lock;

import com.xunmall.base.annotation.Lockable;
import org.springframework.stereotype.Component;

@Component
public class TestLockBean {

    @Lockable(key = "#productId", expirationTime = 20000)
    public void lock(int index, Integer productId) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("TestLockBean error : " + e.getMessage());
        }
        System.out.println("index:" + index + "; productId:" + productId + "; time:" + System.currentTimeMillis());
    }
}
