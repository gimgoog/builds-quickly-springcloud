package com.xunmall.gateway.lock;

import com.xunmall.base.exception.BusinessException;
import com.xunmall.gateway.GatewayApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.concurrent.CountDownLatch;

@SpringBootTest(classes = GatewayApplication.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class LockTest {
    @Autowired
    TestLockBean t;

    @Test
    public void testLock() {
        int threadCount = 10;
        CountDownLatch endCount = new CountDownLatch(threadCount);
        CountDownLatch beginCount = new CountDownLatch(1);

        Thread[] threads = new Thread[threadCount];
        for(int i= 0;i < threadCount;i++){
            final int index = i;
            threads[i] = new Thread(() -> {
                    try {
                        beginCount.await();
                        t.lock(index, 1234);
                    } catch (BusinessException e) {
                        throw e;    // 将此错误释放到全局异常处理
                    } catch (Exception e) {
                        System.out.println("LockThread " + index + " error : " + e.getMessage());
                    } finally {
                        endCount.countDown();
                    }
            });
            threads[i].start();
        }

        try {
            beginCount.countDown();
            endCount.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
