package com.xunmall.hystrixdashboard;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

@SpringBootApplication
@EnableHystrixDashboard
@Slf4j
public class HystrixdashboardApplication implements CommandLineRunner{

    public static void main(String[] args) {
        SpringApplication.run(HystrixdashboardApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
      log.info("gim-hystrixdashboard start success");
    }
}
