package com.xunmall.account.entity;

import com.xunmall.base.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Data
@Table(name = "account_user_role")
public class UserRoleDO extends BaseEntity {
    /**
     * 公司主键
     */
    @Column(name = "COMP_UUID")
    private String compUuid;

    /**
     * 用户主键
     */
    @Column(name = "USER_UUID")
    private String userUuid;

    /**
     * 角色主键
     */
    @Column(name = "ROLE_UUID")
    private String roleUuid;

    /**
     * 启/禁标示:0-启用;1-禁用
     */
    @Column(name = "ON_OFF_FLAG")
    private String onOffFlag;

    /**
     * 关联状态:0-未关联;1-已关联
     */
    @Column(name = "RELA_STATUS")
    private String relaStatus;
}