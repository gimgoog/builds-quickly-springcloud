package com.xunmall.account.entity;

import com.xunmall.base.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Data
@Table(name = "account_role")
public class RoleDO extends BaseEntity {
    /**
     * 公司主键
     */
    @Column(name = "COMP_UUID")
    private String compUuid;

    /**
     * 角色编号
     */
    @Column(name = "ROLE_CODE")
    private String roleCode;

    /**
     * 角色名称
     */
    @Column(name = "ROLE_NAME")
    private String roleName;

    /**
     * 0-系统运维,2-集团管理员,4-公司管理员,16-普通角色。低级的角色不能给他人设置高等级的角色。
     */
    @Column(name = "ROLE_LEVEL")
    private Short roleLevel;

    /**
     * 描述
     */
    @Column(name = "DESCRIPTION")
    private String description;
}