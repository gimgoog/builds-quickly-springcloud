package com.xunmall.account.entity;

import com.xunmall.base.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;
import java.util.Date;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Data
@Table(name = "account_user")
public class UserDO extends BaseEntity {
    /**
     * 手机号码
     */
    @Column(name = "MOBILE")
    private String mobile;

    /**
     * 密码
     */
    @Column(name = "PASSWORD")
    private String password;

    /**
     * 加密盐
     */
    @Column(name = "SALT")
    private String salt;

    /**
     * 展示名称
     */
    @Column(name = "SHOW_NAME")
    private String showName;

    /**
     * 性别
     */
    @Column(name = "GENDER")
    private String gender;

    /**
     * 证件类型
     */
    @Column(name = "ID_TYPE")
    private String idType;

    /**
     * 证件号码
     */
    @Column(name = "ID_NUMBER")
    private String idNumber;

    /**
     * 证件照正面路径
     */
    @Column(name = "ID_NUMBER_FORWARD_FILE")
    private String idNumberForwardFile;

    /**
     * 证件照反面路径
     */
    @Column(name = "ID_NUMBER_BACK_FILE")
    private String idNumberBackFile;

    /**
     * 出生年月
     */
    @Column(name = "BIRTHDAY")
    private Date birthday;

    /**
     * 邮箱地址
     */
    @Column(name = "EMAIL")
    private String email;

    /**
     * qq
     */
    @Column(name = "QQ")
    private String qq;

    /**
     * 微信
     */
    @Column(name = "WEIXIN")
    private String weixin;

    /**
     * 状态
     */
    @Column(name = "STATUS")
    private Boolean status;

    /**
     * 纪录最后所在公司
     */
    @Column(name = "COMP_UUID")
    private String compUuid;

}