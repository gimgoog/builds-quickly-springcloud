package com.xunmall.account.entity;

import com.xunmall.base.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Data
@Table(name = "account_permission")
public class PermissionDO extends BaseEntity {
    /**
     * 权限编号
     */
    @Column(name = "CODE")
    private String code;

    /**
     * 权限名称
     */
    @Column(name = "NAME")
    private String name;

    /**
     * 是否可用:0-不可用;1-可用
     */
    @Column(name = "ENABLE")
    private Boolean enable;

    /**
     * 上级权限
     */
    @Column(name = "P_UUID")
    private String pUuid;

    /**
     * 权限等级:0-系统运维,2-业务管理
     */
    @Column(name = "PERMISSION_LEVEL")
    private Integer permissionLevel;
}