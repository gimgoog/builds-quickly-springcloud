package com.xunmall.account.entity;

import com.xunmall.base.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;
import java.util.Date;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Data
@Table(name = "account_company")
public class CompanyDO extends BaseEntity {
    /**
     * 营业执照全称
     */
    @Column(name = "REAL_NAME")
    private String realName;

    /**
     * 显示名称
     */
    @Column(name = "NICK_NAME")
    private String nickName;

    /**
     * 地址
     */
    @Column(name = "ADDRESS")
    private String address;

    /**
     * 电话
     */
    @Column(name = "TEL")
    private String tel;

    /**
     * 传真
     */
    @Column(name = "FAX")
    private String fax;

    /**
     * 公司LOGO
     */
    @Column(name = "LOGO_FILE")
    private String logoFile;

    /**
     * 联系人
     */
    @Column(name = "CONTACTS")
    private String contacts;

    /**
     * 联系人手机
     */
    @Column(name = "CONT_MOBILE")
    private String contMobile;

    /**
     * 联系人邮箱
     */
    @Column(name = "CONT_EMAIL")
    private String contEmail;

    /**
     * 联系人详细地址
     */
    @Column(name = "CONT_ADDRESS")
    private String contAddress;

    /**
     * 联系人邮编
     */
    @Column(name = "CONT_ZIP_CODE")
    private String contZipCode;

    /**
     * 公司类型
     */
    @Column(name = "COMP_TYPE")
    private String compType;

    /**
     * 初始化标志
     */
    @Column(name = "INIT_FLAG")
    private String initFlag;

    /**
     * 初始化时间
     */
    @Column(name = "INIT_TIME")
    private Date initTime;

    /**
     * 注册渠道
     */
    @Column(name = "REG_CHANNEL")
    private String regChannel;

    /**
     * 注册来源
     */
    @Column(name = "SOURCE")
    private String source;

    /**
     * 行业
     */
    @Column(name = "INDUSTRY")
    private String industry;

    /**
     * 企业规模
     */
    @Column(name = "SCALE")
    private String scale;

    /**
     * 企业状态（0-启用，1-禁用）
     */
    @Column(name = "STATUS")
    private String status;
}