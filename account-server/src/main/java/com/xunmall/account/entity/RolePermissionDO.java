package com.xunmall.account.entity;

import com.xunmall.base.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Data
@Table(name = "account_role_permission")
public class RolePermissionDO extends BaseEntity {
    /**
     * 公司主键
     */
    @Column(name = "COMP_UUID")
    private String compUuid;

    /**
     * 角色主键
     */
    @Column(name = "ROLE_UUID")
    private String roleUuid;

    /**
     * 权限主键
     */
    @Column(name = "PERMISSION_UUID")
    private String permissionUuid;
}