package com.xunmall.account.entity;

import com.xunmall.base.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Data
@Table(name = "account_api")
public class ApiDO extends BaseEntity {
    /**
     * api访问地址
     */
    @Column(name = "URL")
    private String url;

    /**
     * 排序号
     */
    @Column(name = "ORDER_NUM")
    private Integer orderNum;

    /**
     * http请求方式,多个逗号分隔;留空-任意方法
     */
    @Column(name = "HTTP_METHOD")
    private String httpMethod;
}