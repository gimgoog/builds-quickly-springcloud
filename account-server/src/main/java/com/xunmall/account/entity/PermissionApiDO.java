package com.xunmall.account.entity;

import com.xunmall.base.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Data
@Table(name = "account_permission_api")
public class PermissionApiDO extends BaseEntity {
    /**
     * 权限主键
     */
    @Column(name = "PERMISSION_UUID")
    private String permissionUuid;

    /**
     * api主键
     */
    @Column(name = "API_UUID")
    private String apiUuid;
}