package com.xunmall.account;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@SpringBootApplication(scanBasePackages = {"com.xunmall"})
@EnableFeignClients(basePackages = {"com.xunmall"})
@EnableEurekaClient
@Slf4j
public class AccountApplication implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(AccountApplication.class, args);
    }

    @Override
    public void run(String... arg0) throws Exception {
        log.info("gim-account start OK");
    }
}