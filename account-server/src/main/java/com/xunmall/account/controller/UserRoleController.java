package com.xunmall.account.controller;

import com.xunmall.account.dto.UserDto;
import com.xunmall.account.dto.UserRoleDto;
import com.xunmall.account.entity.UserDO;
import com.xunmall.account.entity.UserRoleDO;
import com.xunmall.account.service.UserRoleRemoteService;
import com.xunmall.account.service.UserRoleService;
import com.xunmall.account.service.UserService;
import com.xunmall.base.dto.Result;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@RestController
@RequestMapping(value = "/account/userroles")
public class UserRoleController implements UserRoleRemoteService {
    @Autowired
    UserRoleService userRoleService;
    @Autowired
    UserService userService;

    @Override
    @RequestMapping(value = "/{uuid}", method = RequestMethod.GET)
    public Result<UserRoleDto> getByUuid(@PathVariable("uuid") String uuid) {
        UserRoleDto dto = null;
        UserRoleDO entity = userRoleService.getUserRoleByUuid(uuid);
        if (entity != null) {
            dto = new UserRoleDto();
            BeanUtils.copyProperties(entity, dto);
        }

        return Result.ofSuccess(dto);
    }

    @Override
    @RequestMapping(value = "/comp-admin", method = RequestMethod.GET)
    public Result<List<UserDto>> getAdminUuid(@RequestParam("compUuid") String compUuid) {
        UserRoleDO userRoleDO = new UserRoleDO();
        userRoleDO.setCompUuid(compUuid);
        userRoleDO.setOnOffFlag("0");
        userRoleDO.setRelaStatus("1");
        List<UserRoleDO> select = userRoleService.getUserRoleByObject(userRoleDO);
        List<UserDto> collect = new ArrayList<>();
        if (select != null && select.size() > 0) {
            select.stream().forEach(n -> {
                UserDO userDO = userService.getByUuid(n.getUserUuid());
                UserDto userDto = new UserDto();
                BeanUtils.copyProperties(userDO, userDto);
                collect.add(userDto);
            });
        }
        return Result.ofSuccess(collect);
    }
}