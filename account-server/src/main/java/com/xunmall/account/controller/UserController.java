package com.xunmall.account.controller;

import com.xunmall.account.dto.*;
import com.xunmall.account.entity.UserDO;
import com.xunmall.account.entity.UserRoleDO;
import com.xunmall.account.service.UserRemoteService;
import com.xunmall.account.service.UserRoleService;
import com.xunmall.account.service.UserService;
import com.xunmall.base.dto.Result;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@RestController
@RequestMapping(value = "/account/users")
public class UserController implements UserRemoteService {
    @Autowired
    UserService userService;
    @Autowired
    UserRoleService userRoleService;

    @Override
    @RequestMapping(value = "/check_user_name/{userName}", method = RequestMethod.GET)
    public Result<Boolean> checkUserName(@PathVariable("userName") String userName) {
        Boolean isUnique = userService.checkUserName(userName);
        return Result.ofSuccess(isUnique);
    }

    @Override
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public Result<UserDto> create(@RequestBody UserDto userDto) {
        UserDO userDO = new UserDO();
        BeanUtils.copyProperties(userDto, userDO);
        userDO = userService.create(userDO);
        if (userDO != null) {
            userDto = new UserDto();
            BeanUtils.copyProperties(userDO, userDto);
        }

        return Result.ofSuccess(userDto);
    }

    @Override
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public Result<UserDto> update(@RequestBody UserDto userDto) {
        UserDO userDO = new UserDO();
        BeanUtils.copyProperties(userDto, userDO);
        userDO = userService.update(userDO);
        if (userDO != null) {
            userDto = new UserDto();
            BeanUtils.copyProperties(userDO, userDto);
        }

        return Result.ofSuccess(userDto);
    }

    @Override
    @RequestMapping(value = "/{uuid}", method = RequestMethod.GET)
    public Result<UserDto> getByUuid(@PathVariable("uuid") String uuid) {
        UserDto userDto = null;
        UserDO userDO = userService.getByUuid(uuid);
        if (userDO != null) {
            userDto = new UserDto();
            BeanUtils.copyProperties(userDO, userDto);
        }

        return Result.ofSuccess(userDto);
    }

    @Override
    @RequestMapping(value = "/forget_pwd", method = RequestMethod.PUT)
    public Result<Boolean> forgetPwd(@RequestParam("mobile") String mobile,
                                     @RequestParam("password") String password) {
        if (userService.forgetPwd(mobile, password) > 0) {
            return Result.ofSuccess(true);
        } else {
            return Result.ofSuccess(false);
        }
    }

    @Override
    @RequestMapping(value = "/reset_pwd", method = RequestMethod.PUT)
    public Result<Boolean> resetPwd(@RequestParam("uuid") String uuid,
                                    @RequestParam("curPassword") String curPassword,
                                    @RequestParam("newPassword") String newPassword) {
        if (userService.resetPwd(uuid, curPassword, newPassword) > 0) {
            return Result.ofSuccess(true);
        } else {
            return Result.ofSuccess(false);
        }
    }

    @Override
    @RequestMapping(value = "/update_mobile", method = RequestMethod.PUT)
    public Result<Boolean> updateMobile(@RequestParam("uuid") String uuid,
                                        @RequestParam("mobile") String mobile,
                                        @RequestParam("password") String password) {
        if (userService.updateMobile(uuid, mobile, password) > 0) {
            return Result.ofSuccess(true);
        } else {
            return Result.ofSuccess(false);
        }
    }

    @Override
    @RequestMapping(value = "/get_by_name/{userName}", method = RequestMethod.GET)
    public Result<LoginUserDto> getByUserName(@PathVariable("userName") String userName) {
        LoginUserDto loginUserDto = null;
        UserDO userDO = userService.getByUserName(userName);
        if (userDO != null) {
            loginUserDto = new LoginUserDto();
            BeanUtils.copyProperties(userDO, loginUserDto);
            loginUserDto.setUsername(userDO.getShowName());
            // 用户被禁用后不可登陆
            loginUserDto.setEnabled(userDO.getStatus());
        }
        return Result.ofSuccess(loginUserDto);
    }

    @Override
    @RequestMapping(value = "/get_company", method = RequestMethod.GET)
    public Result<CompanyDto> getUserCompany(@RequestParam("userUuid") String userUuid) {
        return Result.ofSuccess(userService.getUserCompany(userUuid));
    }

    @Override
    @RequestMapping(value = "/is_user_in_comp", method = RequestMethod.GET)
    public Result<Boolean> isUserInComp(@RequestParam("userUuid") String userUuid,
                                        @RequestParam("companyUuid") String companyUuid) {
        return Result.ofSuccess(userService.isUserInComp(userUuid, companyUuid));
    }

    @Override
    @RequestMapping(value = "/get_comp_managers/{compUuid}", method = RequestMethod.GET)
    public List<ManageListDto> queryCompManageList(@PathVariable("compUuid") String compUuid) {
        return userService.queryCompManageList(compUuid);
    }

    @Override
    @RequestMapping(value = "/correlation_roles", method = RequestMethod.POST)
    public int correlationRoles(@RequestParam("compUuid") String compUuid,
                                @RequestParam("userUuid") String userUuid,
                                @RequestParam("roles") List<String> roles,
                                @RequestParam("onOffFlag") String onOffFlag,
                                @RequestParam("relaStatus") String relaStatus,
                                @RequestParam(value = "emplUuid", required = false) String emplUuid) {
        return userRoleService.correlationRoles(compUuid, userUuid, roles, onOffFlag, relaStatus, emplUuid);
    }

    @Override
    @RequestMapping(value = "/get_user_role", method = RequestMethod.GET)
    public UserRoleDto getUserRole(@RequestParam("userUuid") String userUuid,
                                   @RequestParam("compUuid") String compUuid,
                                   @RequestParam("roleId") String roleId) {
        UserRoleDto dto = null;
        try {
            UserRoleDO entity = userRoleService.getUserRole(userUuid, compUuid, roleId);
            if (entity != null) {
                dto = new UserRoleDto();
                BeanUtils.copyProperties(entity, dto);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dto;
    }

    @Override
    @RequestMapping(value = "/uncorrelation_roles", method = RequestMethod.POST)
    public int uncorrelationRoles(@RequestParam("compUuid") String compUuid,
                                  @RequestParam("userUuid") String userUuid,
                                  @RequestParam("roles") List<String> roles) {
        return userRoleService.uncorrelationRoles(compUuid, userUuid, roles);
    }

    @Override
    @RequestMapping(value = "/comp_manage_onoff", method = RequestMethod.POST)
    public int modifyCompManageOnOff(@RequestParam("userRoleUuid") String userRoleUuid,
                                     @RequestParam("onOffFlag") String onOffFlag) {
        return userRoleService.modifyCompManageOnOff(userRoleUuid, onOffFlag);
    }

    @Override
    @RequestMapping(value = "/comp_manage_role", method = RequestMethod.PUT)
    public int modifyCompManageRole(@RequestParam("userRoleUuid") String userRoleUuid,
                                    @RequestParam("roleUuid") String roleUuid) {
        return userRoleService.modifyCompManageRole(userRoleUuid, roleUuid);
    }

    @Override
    @RequestMapping(value = "/user_role_rela", method = RequestMethod.POST)
    public int modifyUserRoleRela(@RequestParam("userRoleUuid") String userRoleUuid,
                                  @RequestParam("relaStatus") String relaStatus) {
        return userRoleService.modifyUserRoleRela(userRoleUuid, relaStatus);
    }

    @Override
    @RequestMapping(value = "/transfer_admin", method = RequestMethod.POST)
    public int transferAdmin(@RequestParam("compUuid") String compUuid,
                             @RequestParam("oldMobile") String oldMobile,
                             @RequestParam("newMobile") String newMobile) {
        return userRoleService.transferAdmin(compUuid, oldMobile, newMobile);
    }

    @Override
    @RequestMapping(value = "/uncorrelation_emplrole", method = RequestMethod.POST)
    public void uncorrelationEmplRole(@RequestParam("compUuid") String compUuid,
                                      @RequestParam("emplUuid") String emplUuid) {
        userRoleService.uncorrelationEmplRole(compUuid, emplUuid);
    }

    @Override
    @RequestMapping(value = "/modify_last_comp", method = RequestMethod.PUT)
    public Integer modifyLastComp(@RequestParam("uuid") String uuid,
                                  @RequestParam("compUuid") String compUuid,
                                  @RequestParam("isAppLogin") boolean isAppLogin) {
        return userService.modifyComp(uuid, compUuid, isAppLogin);
    }

    @Override
    @RequestMapping(value = "/check_pwd", method = RequestMethod.GET)
    public Result<Boolean> checkPwd(@RequestParam("uuid") String uuid,
                                    @RequestParam("password") String password) {
        if (userService.checkPwd(uuid, password)) {
            return Result.ofSuccess(true);
        } else {
            return Result.ofSuccess(false);
        }
    }

}