package com.xunmall.account.controller;

import com.xunmall.account.dto.ApiDto;
import com.xunmall.account.entity.ApiDO;
import com.xunmall.account.service.ApiRemoteService;
import com.xunmall.account.service.ApiService;
import com.xunmall.account.vo.ApiVo;
import com.xunmall.base.dto.Result;
import com.xunmall.base.page.Pages;
import com.xunmall.base.page.PagingModel;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@RestController
@RequestMapping(value = "/account/apis")
public class ApiController implements ApiRemoteService {
    @Autowired
    ApiService apiService;

    @Override
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public Result<ApiDto> create(ApiDto apiDto) {
        ApiDO apiDO = new ApiDO();
        BeanUtils.copyProperties(apiDto, apiDO);
        apiDO = apiService.create(apiDO);
        if (apiDO != null) {
            apiDto = new ApiDto();
            BeanUtils.copyProperties(apiDO, apiDto);
        }

        return Result.ofSuccess(apiDto);
    }

    @Override
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public Result<ApiDto> update(ApiDto dto) {
        ApiDO entity = new ApiDO();
        BeanUtils.copyProperties(dto, entity);
        entity = apiService.update(entity);
        if (entity != null) {
            dto = new ApiDto();
            BeanUtils.copyProperties(entity, dto);
        }

        return Result.ofSuccess(dto);
    }

    @Override
    @RequestMapping(value = "/{uuid}", method = RequestMethod.GET)
    public Result<ApiDto> getByUuid(@PathVariable("uuid") String uuid) {
        ApiDto apiDto = null;
        ApiDO apiDO = apiService.getApiByUuid(uuid);
        if (apiDO != null) {
            apiDto = new ApiDto();
            BeanUtils.copyProperties(apiDO, apiDto);
        }

        return Result.ofSuccess(apiDto);
    }
}