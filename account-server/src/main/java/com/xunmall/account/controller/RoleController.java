package com.xunmall.account.controller;

import com.xunmall.account.dto.RoleDto;
import com.xunmall.account.dto.UrlDto;
import com.xunmall.account.dto.VersionDto;
import com.xunmall.account.entity.RoleDO;
import com.xunmall.account.service.RoleRemoteService;
import com.xunmall.account.service.RoleService;
import com.xunmall.base.dto.Result;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@RestController
@RequestMapping(value = "/account/roles")
public class RoleController implements RoleRemoteService {
    @Autowired
    RoleService roleService;

    @Override
    @RequestMapping(value = "/get_meta_source", method = RequestMethod.GET)
    public Result<Map<String, List<UrlDto>>> getMetaSource() {
        Map<String, List<UrlDto>> metas = roleService.getMetaSource();
        return Result.ofSuccess(metas);
    }

    @Override
    @RequestMapping(value = "/get_by_user", method = RequestMethod.GET)
    public Result<List<RoleDto>> getByUser(@RequestParam("userId") String userId,
                                           @RequestParam("compId") String compId) {
        List<RoleDto> roleDtos = new ArrayList<RoleDto>();
        List<RoleDO> roleDOS = roleService.getByUser(userId, compId);
        for (RoleDO roleDO : roleDOS) {
            RoleDto roleDto = new RoleDto();
            BeanUtils.copyProperties(roleDO, roleDto);

            roleDtos.add(roleDto);
        }

        return Result.ofSuccess(roleDtos);
    }

    @Override
    @RequestMapping(value = "/get_by_comp", method = RequestMethod.GET)
    public Result<List<RoleDto>> getByComp(@RequestParam("compId") String compId) {
        List<RoleDto> roleDtos = new ArrayList<RoleDto>();
        List<RoleDO> roleDOS = roleService.getByComp(compId);
        for (RoleDO roleDO : roleDOS) {
            RoleDto roleDto = new RoleDto();
            BeanUtils.copyProperties(roleDO, roleDto);

            roleDtos.add(roleDto);
        }

        return Result.ofSuccess(roleDtos);
    }

    @Override
    @RequestMapping(value = "/check_role_code/{roleCode}", method = RequestMethod.GET)
    public Result<Boolean> checkRoleCode(@PathVariable("roleCode") String roleCode) {
        Boolean isUnique = roleService.checkRoleCode(roleCode);

        return Result.ofSuccess(isUnique);
    }

    @Override
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public Result<RoleDto> create(@RequestBody RoleDto roleDto) {
        RoleDO roleDO = new RoleDO();
        BeanUtils.copyProperties(roleDto, roleDO);
        roleDO = roleService.create(roleDO);
        if (roleDO != null) {
            roleDto = new RoleDto();
            BeanUtils.copyProperties(roleDO, roleDto);
        }

        return Result.ofSuccess(roleDto);
    }

    @Override
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public Result<RoleDto> update(@RequestBody RoleDto dto) {
        RoleDO entity = new RoleDO();
        BeanUtils.copyProperties(dto, entity);
        entity = roleService.update(entity);
        if (entity != null) {
            dto = new RoleDto();
            BeanUtils.copyProperties(entity, dto);
        }

        return Result.ofSuccess(dto);
    }

    @Override
    @RequestMapping(value = "/{uuid}", method = RequestMethod.GET)
    public Result<RoleDto> getByUuid(@PathVariable("uuid") String uuid) {
        RoleDto roleDto = null;
        RoleDO roleDO = roleService.getByUuid(uuid);
        if (roleDO != null) {
            roleDto = new RoleDto();
            BeanUtils.copyProperties(roleDO, roleDto);
        }

        return Result.ofSuccess(roleDto);
    }
}