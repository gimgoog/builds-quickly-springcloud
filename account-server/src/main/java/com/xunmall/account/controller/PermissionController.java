package com.xunmall.account.controller;

import com.xunmall.account.dto.PermissionDto;
import com.xunmall.account.entity.PermissionDO;
import com.xunmall.account.service.PermissionRemoteService;
import com.xunmall.account.service.PermissionService;
import com.xunmall.base.dto.Result;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@RestController
@RequestMapping(value = "/account/permissions")
public class PermissionController implements PermissionRemoteService {
    @Autowired
    PermissionService permissionService;

    @Override
    @RequestMapping(value = "/check_permission_code/{code}", method = RequestMethod.GET)
    public Result<Boolean> checkPermissionCode(String code) {
        Boolean isUnique = permissionService.checkPermissionCode(code);

        return Result.ofSuccess(isUnique);
    }

    @Override
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public Result<PermissionDto> create(PermissionDto permissionDto) {
        PermissionDO permissionDO = new PermissionDO();
        BeanUtils.copyProperties(permissionDto, permissionDO);
        permissionDO = permissionService.create(permissionDO);
        if (permissionDO != null) {
            permissionDto = new PermissionDto();
            BeanUtils.copyProperties(permissionDO, permissionDto);
        }

        return Result.ofSuccess(permissionDto);
    }

    @Override
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public Result<PermissionDto> update(PermissionDto dto) {
        PermissionDO entity = new PermissionDO();
        BeanUtils.copyProperties(dto, entity);
        entity = permissionService.update(entity);
        if (entity != null) {
            dto = new PermissionDto();
            BeanUtils.copyProperties(entity, dto);
        }

        return Result.ofSuccess(dto);
    }

    @Override
    @RequestMapping(value = "/{uuid}", method = RequestMethod.GET)
    public Result<PermissionDto> getByUuid(@PathVariable("uuid") String uuid) {
        PermissionDto permissionDto = null;
        PermissionDO permissionDO = permissionService.getPermissionByUuid(uuid);
        if (permissionDO != null) {
            permissionDto = new PermissionDto();
            BeanUtils.copyProperties(permissionDO, permissionDto);
        }

        return Result.ofSuccess(permissionDto);
    }
}