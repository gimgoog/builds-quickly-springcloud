package com.xunmall.account.controller;

import com.xunmall.account.dto.CompanyDto;
import com.xunmall.account.entity.CompanyDO;
import com.xunmall.account.service.CompanyService;
import com.xunmall.account.vo.AddCompanyVo;
import com.xunmall.account.vo.UpdateAuthCompanyVo;
import com.xunmall.account.vo.UpdateBasicCompanyVo;
import com.xunmall.base.constants.SystemConstants;
import com.xunmall.base.controller.BaseController;
import com.xunmall.base.exception.BadRequestException;
import com.xunmall.base.exception.BaseException;
import com.xunmall.base.exception.BusinessException;
import com.xunmall.base.util.BeanCopyUtils;
import com.xunmall.security.GlobalSessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@RestController
@RequestMapping(value = "/account/company")
public class CompanyController extends BaseController {

    @Autowired
    private CompanyService companyService;
    @Value("${businessLiceFile_filepath:C\\}")
    private String businessLiceLocation;
    @Value("${logoFile_filepath:C:\\}")
    private String logoLocation;

    /**
     * 检查公司名称是否存在
     *
     * @param companyName
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/check_company_real")
    public boolean checkCompany(@RequestParam("companyName") String companyName) {
        List<CompanyDO> list = companyService.checkCompRealName(companyName);
        if (CollectionUtils.isEmpty(list)) {
            return false;
        }
        return true;
    }

    /**
     * 创建企业
     *
     * @param addCompanyVo
     */
    @RequestMapping(method = RequestMethod.POST, value = "/create")
    public CompanyDto create(@RequestBody @Valid AddCompanyVo addCompanyVo) {
        return companyService.insert(addCompanyVo);
    }

    /**
     * 根据公司uuid获取公司信息
     *
     * @param compUuid
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public CompanyDto getCompanyByUuid(@PathVariable("compUuid") String compUuid) {
        CompanyDO company = companyService.findByUuid(compUuid);
        CompanyDto companyDto = new CompanyDto();
        BeanCopyUtils.copyProperties(company, companyDto);
        return companyDto;
    }

    /**
     * 修改企业基本信息
     *
     * @param updateBasicCompanyVo
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT, value = "/basic")
    public int updateCompany(@RequestBody @Valid UpdateBasicCompanyVo updateBasicCompanyVo) {
        return companyService.updateBasicCompany(updateBasicCompanyVo);
    }

    /**
     * 企业初始化完成
     *
     * @return
     */
    @Deprecated
    @RequestMapping(method = RequestMethod.PUT, value = "/update_comp_init_flag")
    public void updateCompanyInit(@PathVariable("compUuid") String compUuid) {
        CompanyDO company = new CompanyDO();
        company.setUuid(compUuid);
        company.setInitFlag(SystemConstants.CODE_YES);
        companyService.updateCompany(company);
    }
}
