package com.xunmall.account.mapper;

import com.xunmall.account.entity.PermissionApiDO;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Repository
public interface PermissionApiMapper extends Mapper<PermissionApiDO> {
}