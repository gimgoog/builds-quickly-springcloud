package com.xunmall.account.mapper;

import com.xunmall.account.entity.UserDO;
import com.xunmall.account.dto.*;
import com.xunmall.account.vo.AccountCompListVo;
import com.xunmall.account.vo.AccountListVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Repository
public interface UserMapper extends Mapper<UserDO> {
    CompanyDto getUserCompanies(@Param("userUuid") String userUuid);

    Boolean isUserInComp(@Param("userUuid") String userUuid,
                         @Param("companyUuid") String companyUuid);

    String getUserEmployeeId(@Param("userUuid") String userUuid,
                             @Param("companyUuid") String companyUuid);

    int updateDevIdByMobile(@Param("mobile") String mobile,
                            @Param("devId") String devId);

    Integer modifyComp(@Param("uuid") String uuid,
                       @Param("compUuid") String compUuid,
                       @Param("isAppLogin") boolean isAppLogin);


    Integer batchUpdateIsActive(@Param("list") List<String> userUuids,
                                @Param("isActive") Boolean isActive);


    void updateLastLoginTime(@Param("userId") String userId,
                             @Param("loginTime") Date loginTime);

    void updateInitFlag(@Param("userId") String userId,
                        @Param("initFlag") String initFlag);
}