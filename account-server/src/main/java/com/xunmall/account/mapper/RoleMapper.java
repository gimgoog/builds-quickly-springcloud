package com.xunmall.account.mapper;

import com.xunmall.account.dto.VersionDto;
import com.xunmall.account.entity.RoleDO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Repository
public interface RoleMapper extends Mapper<RoleDO> {
    List<Map<String, String>> getMetaSource();

    List<Map<String, String>> getDefaultMetaSource(@Param("type") String type);

    List<RoleDO> getRolesByUserId(@Param("userId") String userId,
                                  @Param("compId") String compId);

    void cleanAuthorization(@Param("compId") String compId,
                            @Param("roleId") String roleId);

    boolean checkRela(@Param("roleId") String roleId);

    List<RoleDO> getRoles(@Param("role") RoleDO entity);
}