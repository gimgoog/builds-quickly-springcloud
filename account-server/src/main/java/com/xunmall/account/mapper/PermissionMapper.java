package com.xunmall.account.mapper;

import com.xunmall.account.dto.PermissionDto;
import com.xunmall.account.entity.PermissionDO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Repository
public interface PermissionMapper extends Mapper<PermissionDO> {
    List<PermissionDto> getPermissionsByRole(@Param("compId") String compId,
                                             @Param("roleId") String roleId);
}