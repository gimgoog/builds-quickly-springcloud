package com.xunmall.account.mapper;

import com.xunmall.account.dto.ManageListDto;
import com.xunmall.account.entity.UserRoleDO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Repository
public interface UserRoleMapper extends Mapper<UserRoleDO> {
    /**
     * 查询公司员工列表
     *
     * @param compUuid
     * @return
     */
    List<ManageListDto> selectCompManageList(@Param("compUuid") String compUuid);

    int uncorrelationRoles(@Param("compUuid") String compUuid,
                           @Param("userUuid") String userUuid,
                           @Param("roles") List<String> roles);

    /**
     * 修改用户编号
     *
     * @param compUuid    公司编号
     * @param oldUserUuid 老的用户编号
     * @param newUserUuid 新的用户编号
     * @return
     */
    int transferAdmin(@Param("compUuid") String compUuid,
                      @Param("oldUserUuid") String oldUserUuid,
                      @Param("newUserUuid") String newUserUuid);

    void uncorrelationEmplRole(@Param("compUuid") String compUuid,
                               @Param("emplUuid") String emplUuid);

    boolean hasTheRole(@Param("userUuid") String userUuid,
                          @Param("compUuid") String compUuid,
                          @Param("roleId") String roleId);
}