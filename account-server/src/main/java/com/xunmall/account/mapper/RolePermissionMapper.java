package com.xunmall.account.mapper;

import com.xunmall.account.entity.RolePermissionDO;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Repository
public interface RolePermissionMapper extends Mapper<RolePermissionDO> {
}