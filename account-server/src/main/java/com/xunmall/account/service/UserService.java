package com.xunmall.account.service;

import com.xunmall.account.dto.CompanyDto;
import com.xunmall.account.dto.ManageListDto;
import com.xunmall.account.entity.UserDO;

import java.util.List;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
public interface UserService {
    Boolean checkUserName(String userName);

    UserDO create(UserDO userDO);

    UserDO update(UserDO userDO);

    Integer forgetPwd(String mobile, String password);

    Integer resetPwd(String uuid, String curPassword, String newPassword);

    Integer updateMobile(String uuid, String mobile, String password);

    UserDO getByUserName(String userName);

    CompanyDto getUserCompany(String userUuid);

    Boolean isUserInComp(String userUuid, String companyUuid);

    List<ManageListDto> queryCompManageList(String compUuid);

    Integer modifyComp(String uuid, String compUuid, boolean isAppLogin);

    Boolean checkPwd(String uuid, String password);

    UserDO getByUuid(String uuid);
}