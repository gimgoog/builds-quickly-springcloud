package com.xunmall.account.service;

import com.xunmall.account.dto.PermissionDto;
import com.xunmall.account.entity.PermissionDO;

import java.util.List;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
public interface PermissionService {
    Boolean checkPermissionCode(String code);

    PermissionDO create(PermissionDO permissionDO);

    PermissionDO update(PermissionDO entity);

    List<PermissionDto> getByRole(String compId, String roleId);

    PermissionDO getPermissionByUuid(String uuid);
}