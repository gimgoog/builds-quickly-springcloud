package com.xunmall.account.service;

import com.xunmall.account.dto.CompanyDto;
import com.xunmall.account.entity.CompanyDO;
import com.xunmall.account.vo.AddCompanyVo;
import com.xunmall.account.vo.UpdateAuthCompanyVo;
import com.xunmall.account.vo.UpdateBasicCompanyVo;

import java.util.List;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
public interface CompanyService {
    /**
     * 检查公司昵称是否存在 一个用户不能有多个公司昵称相同的公司
     * @param userUuid
     * @param companyName
     * @return
     */
    List<CompanyDO> checkNickName(String userUuid, String companyName);

    /**
     * 检查公司名称是否存在
     * @param name
     * @return
     */
    List<CompanyDO> checkCompRealName(String name);

    /**
     * //检查用户是否可以创建公司默认创建5个
     * @param userUuid
     * @return
     */
    boolean checkCanCreateCompany(String userUuid);
    /**
     * 创建企业
     *
     * @param addCompanyVo 企业信息
     * @return
     */
    CompanyDto insert(AddCompanyVo addCompanyVo);

    /**
     * 修改认证信息
     *
     * @param updateAuthCompanyVo
     * @return
     */
    int updateAuthCompany(UpdateAuthCompanyVo updateAuthCompanyVo);

    /**
     * 修改基本信息
     *
     * @param updateBasicCompanyVo
     * @return
     */

    int updateBasicCompany(UpdateBasicCompanyVo updateBasicCompanyVo);

    CompanyDO findByUuid(String uuid);

    int updateCompany(CompanyDO company);
}
