package com.xunmall.account.service.impl;

import com.xunmall.account.dto.ManageListDto;
import com.xunmall.account.entity.UserDO;
import com.xunmall.account.entity.UserRoleDO;
import com.xunmall.account.mapper.UserRoleMapper;
import com.xunmall.account.service.UserRoleService;
import com.xunmall.account.service.UserService;
import com.xunmall.base.exception.BadRequestException;
import com.xunmall.base.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Service
public class UserRoleServiceImpl implements UserRoleService {
    @Autowired
    UserRoleMapper userRoleMapper;
    @Autowired
    UserService userService;

    /**
     * 校验唯一性，唯一返回true
     *
     * @param userUuid
     * @param compUuid
     * @param roleId
     * @return
     */
    public boolean checkIsUnique(String userUuid, String compUuid, String roleId) {
        return !userRoleMapper.hasTheRole(userUuid, compUuid, roleId);
    }

    @Override
    public int create(UserRoleDO entity) {
        if (!checkIsUnique(entity.getUserUuid(), entity.getCompUuid(), entity.getRoleUuid())) {
            throw new BusinessException("error.account.0006", this);
        }
        entity.buildForInsert();
        return userRoleMapper.insertSelective(entity);
    }

    @Override
    public List<ManageListDto> selectCompManageList(String compUuid) {
        return userRoleMapper.selectCompManageList(compUuid);
    }

    @Override
    public int correlationRoles(String compUuid, String userUuid, List<String> roles, String onOffFlag, String relaStatus, String emplUuid) {
        roles.stream().forEach(roleId -> {
            UserRoleDO userRoleDO = new UserRoleDO();
            userRoleDO.setCompUuid(compUuid);
            userRoleDO.setUserUuid(userUuid);
            userRoleDO.setRoleUuid(roleId);
            userRoleDO.setOnOffFlag(onOffFlag);
            userRoleDO.setRelaStatus(relaStatus);
            this.create(userRoleDO);
        });
        return 1;
    }

    @Override
    public UserRoleDO getUserRole(String userUuid, String compUuid, String roleId) {
        UserRoleDO userRoleDO = new UserRoleDO();
        userRoleDO.setUserUuid(userUuid);
        userRoleDO.setCompUuid(compUuid);
        userRoleDO.setRoleUuid(roleId);
        return userRoleMapper.selectOne(userRoleDO);
    }

    @Override
    public int uncorrelationRoles(String compUuid, String userUuid, List<String> roles) {
        return userRoleMapper.uncorrelationRoles(compUuid, userUuid, roles);
    }

    @Override
    public int modifyCompManageOnOff(String userRoleUuid, String onOffFlag) {
        UserRoleDO userRoleDO = new UserRoleDO();
        userRoleDO.setUuid(userRoleUuid);
        userRoleDO.setOnOffFlag(onOffFlag);
        userRoleDO.buildForUpdate();
        return userRoleMapper.updateByPrimaryKeySelective(userRoleDO);
    }

    @Override
    public int modifyCompManageRole(String userRoleUuid, String roleUuid) {
        UserRoleDO uRole = userRoleMapper.selectByPrimaryKey(userRoleUuid);
        if (uRole == null) {
            throw new BadRequestException("error.account.0003", this);
        }

        if (!checkIsUnique(uRole.getUserUuid(), uRole.getCompUuid(), roleUuid)) {
            throw new BusinessException("error.account.0006", this);
        }

        UserRoleDO userRoleDO = new UserRoleDO();
        userRoleDO.setUuid(userRoleUuid);
        userRoleDO.setRoleUuid(roleUuid);
        userRoleDO.buildForUpdate();
        return userRoleMapper.updateByPrimaryKeySelective(userRoleDO);
    }

    @Override
    public int modifyUserRoleRela(String userRoleUuid, String relaStatus) {
        UserRoleDO userRoleDO = new UserRoleDO();
        userRoleDO.setUuid(userRoleUuid);
        userRoleDO.setRelaStatus(relaStatus);
        userRoleDO.buildForUpdate();
        return userRoleMapper.updateByPrimaryKeySelective(userRoleDO);
    }

    @Override
    public int transferAdmin(String compUuid, String oldMobile, String newMobile) {
        UserDO oldUserDO = userService.getByUserName(oldMobile);
        if (oldUserDO == null) {
            throw new BadRequestException("error.account.0003", this);
        }

        UserDO newUserDO = userService.getByUserName(newMobile);
        if (newUserDO == null) {
            throw new BadRequestException("error.account.0008", this);
        }

        return userRoleMapper.transferAdmin(compUuid, oldUserDO.getUuid(), newUserDO.getUuid());
    }

    @Override
    public void uncorrelationEmplRole(String compUuid, String emplUuid) {
        userRoleMapper.uncorrelationEmplRole(compUuid, emplUuid);
    }

    @Override
    public UserRoleDO getUserRoleByUuid(String uuid) {
        return userRoleMapper.selectByPrimaryKey(uuid);
    }

    @Override
    public List<UserRoleDO> getUserRoleByObject(UserRoleDO userRoleDO) {
        return userRoleMapper.select(userRoleDO);
    }
}