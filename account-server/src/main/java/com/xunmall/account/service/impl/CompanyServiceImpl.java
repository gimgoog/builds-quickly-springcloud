package com.xunmall.account.service.impl;

import com.xunmall.account.dto.CompanyDto;
import com.xunmall.account.dto.RoleDto;
import com.xunmall.account.entity.CompanyDO;
import com.xunmall.account.mapper.CompanyMapper;
import com.xunmall.account.service.CompanyService;
import com.xunmall.account.service.RoleService;
import com.xunmall.account.service.UserRoleService;
import com.xunmall.account.vo.*;
import com.xunmall.base.constants.SystemConstants;
import com.xunmall.base.exception.BadRequestException;
import com.xunmall.base.exception.BusinessException;
import com.xunmall.base.util.StringUtils;
import com.xunmall.security.GlobalSessionUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Service
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private CompanyMapper companyMapper;
    @Autowired
    private UserRoleService userRoleService;
    @Autowired
    private RoleService roleService;

    @Transactional
    @Override
    public CompanyDto insert(AddCompanyVo addCompanyVo) {
        String userUuid = GlobalSessionUtil.getUserUuid();
        if (!checkCanCreateCompany(userUuid)) {
            throw new BadRequestException("error.0029", this);
        }
        List<CompanyDO> list = checkNickName(userUuid, addCompanyVo.getCompanyName());
        if (CollectionUtils.isNotEmpty(list)) {
            throw new BadRequestException("error.0013", this, addCompanyVo.getCompanyName());
        }
        CompanyDO company = new CompanyDO();
        company.setNickName(addCompanyVo.getCompanyName());
        //初始化状态
        company.setInitFlag(SystemConstants.CODE_NO);
        company.buildForInsert();
        companyMapper.insertSelective(company);

        RoleDto roleDto = roleService.getCompAdmin();
        List<String> roles = new ArrayList<>();
        roles.add(roleDto.getUuid());
        Integer insertNum = userRoleService.correlationRoles(company.getUuid(), userUuid, roles, SystemConstants.CODE_NO, SystemConstants.CODE_YES, null);
        if (insertNum == 0) {
            throw new BadRequestException("error.0003", this);
        }
        CompanyDto companyDto = new CompanyDto();
        BeanUtils.copyProperties(company, companyDto);
        return companyDto;
    }

    @Override
    public boolean checkCanCreateCompany(String userUuid) {
        CompanyDO company = new CompanyDO();
        company.setCreateUser(userUuid);
        company.setDeleteFlag(false);
        List<CompanyDO> list = companyMapper.select(company);
        if (CollectionUtils.isNotEmpty(list)) {
            if (list.size() > 5) {
                return false;
            }
        }
        return true;
    }

    @Override
    public List<CompanyDO> checkNickName(String userUuid, String name) {
        if (StringUtils.isBlank(name)) {
            throw new BusinessException("error.0012", this);
        }
        CompanyDO company = new CompanyDO();
        company.setNickName(name);
        company.setCreateUser(userUuid);
        company.setDeleteFlag(false);
        List<CompanyDO> companies = companyMapper.select(company);
        return companies;
    }

    @Override
    public List<CompanyDO> checkCompRealName(String name) {
        if (StringUtils.isBlank(name)) {
            throw new BusinessException("error.0012", this);
        }
        CompanyDO company = new CompanyDO();
        company.setRealName(name);
        company.setDeleteFlag(false);
        List<CompanyDO> companies = companyMapper.select(company);
        return companies;
    }

    @Override
    public int updateAuthCompany(UpdateAuthCompanyVo updateAuthCompanyVo) {
        //检查认证名称是否存在
        List<CompanyDO> companies = checkCompRealName(updateAuthCompanyVo.getRealName());
        if (CollectionUtils.isNotEmpty(companies)) {
            if (companies.size() > 1) {
                throw new BadRequestException("error.0013", this, updateAuthCompanyVo.getRealName());
            } else {
                if (!companies.get(0).getUuid().equals(updateAuthCompanyVo.getUuid())) {
                    throw new BadRequestException("error.0013", this, updateAuthCompanyVo.getRealName());
                }
            }
        }
        CompanyDO company = findByUuid(updateAuthCompanyVo.getUuid());
        if (company == null) {
            throw new BadRequestException("error.0000", this);
        }
        BeanUtils.copyProperties(updateAuthCompanyVo, company);
        company.buildForUpdate();
        return companyMapper.updateByPrimaryKeySelective(company);
    }

    @Override
    public int updateBasicCompany(UpdateBasicCompanyVo updateBasicCompanyVo) {
        CompanyDO company = findByUuid(updateBasicCompanyVo.getUuid());
        CompContactsInfoVo compContactsInfoVo = updateBasicCompanyVo.getCompContactsInfoVo();
        if (compContactsInfoVo != null) {
            BeanUtils.copyProperties(compContactsInfoVo, company);
        }
        CompBasicInfoVo basicInfoVo = updateBasicCompanyVo.getCompBasicInfo();
        if (basicInfoVo != null) {
            BeanUtils.copyProperties(basicInfoVo, company);
        }
        if (StringUtils.isNotBlank(updateBasicCompanyVo.getNickName())) {
            company.setNickName(updateBasicCompanyVo.getNickName());
        }
        company.buildForUpdate();
        return companyMapper.updateByPrimaryKeySelective(company);
    }

    @Override
    public CompanyDO findByUuid(String uuid) {
        if (StringUtils.isBlank(uuid)) {
            throw new BusinessException("error.0012", this);
        }
        CompanyDO company = companyMapper.selectByPrimaryKey(uuid);
        if (company == null || company.getDeleteFlag()) {
            throw new BusinessException("error.0000", this);
        }
        return company;
    }

    @Override
    public int updateCompany(CompanyDO company) {
        return companyMapper.updateByPrimaryKeySelective(company);
    }
}
