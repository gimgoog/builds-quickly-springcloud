package com.xunmall.account.service;

import com.xunmall.account.dto.ManageListDto;
import com.xunmall.account.entity.UserRoleDO;

import java.util.List;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
public interface UserRoleService {
    int create(UserRoleDO entity);

    List<ManageListDto> selectCompManageList(String compUuid);

    int correlationRoles(String compUuid, String userUuid, List<String> roles, String onOffFlag, String relaStatus, String emplUuid);

    UserRoleDO getUserRole(String userUuid, String compUuid, String roleId);

    int uncorrelationRoles(String compUuid, String userUuid, List<String> roles);

    int modifyCompManageOnOff(String userRoleUuid, String onOffFlag);

    int modifyCompManageRole(String userRoleUuid, String roleUuid);

    int modifyUserRoleRela(String userRoleUuid, String relaStatus);

    int transferAdmin(String compUuid, String uuid, String uuid1);

    void uncorrelationEmplRole(String compUuid, String emplUuid);

    UserRoleDO getUserRoleByUuid(String uuid);

    List<UserRoleDO> getUserRoleByObject(UserRoleDO userRoleDO);
}