package com.xunmall.account.service;

import com.xunmall.account.entity.RolePermissionDO;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
public interface RolePermissionService {
    String create(RolePermissionDO entity);
}