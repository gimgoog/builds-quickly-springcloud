package com.xunmall.account.service.impl;

import com.xunmall.account.mapper.PermissionApiMapper;
import com.xunmall.account.service.PermissionApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Service
public class PermissionApiServiceImpl implements PermissionApiService {
    @Autowired
    PermissionApiMapper permissionApiMapper;
}