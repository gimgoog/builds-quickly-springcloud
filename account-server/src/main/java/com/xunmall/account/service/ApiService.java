package com.xunmall.account.service;

import com.xunmall.account.entity.ApiDO;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
public interface ApiService {
    ApiDO create(ApiDO apiDO);

    ApiDO update(ApiDO entity);

    ApiDO getApiByUuid(String uuid);
}