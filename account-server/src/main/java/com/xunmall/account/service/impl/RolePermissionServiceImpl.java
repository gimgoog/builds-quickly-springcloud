package com.xunmall.account.service.impl;

import com.xunmall.account.mapper.RolePermissionMapper;
import com.xunmall.account.service.RolePermissionService;
import com.xunmall.account.entity.RolePermissionDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.common.Mapper;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Service
public class RolePermissionServiceImpl implements RolePermissionService {
    @Autowired
    RolePermissionMapper mapper;

    @Override
    public String create(RolePermissionDO entity) {
        entity.buildForInsert();
        mapper.insertSelective(entity);
        return entity.getUuid();
    }

}
