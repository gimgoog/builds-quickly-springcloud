package com.xunmall.account.service;

import com.xunmall.account.dto.RoleDto;
import com.xunmall.account.dto.UrlDto;
import com.xunmall.account.entity.RoleDO;

import java.util.List;
import java.util.Map;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
public interface RoleService {
    /**
     * 获取权限url与permission的对照关系,以及默认访问权限,url按照ORDER_NUM排序
     *
     * @return perms   : url->permission集合 <br>
     * defaults: 默认访问权限集合 <br>
     * holdUp  : 兜底访问权限集合 <br>
     */
    Map<String, List<UrlDto>> getMetaSource();

    List<RoleDO> getByUser(String userId, String compId);

    List<RoleDO> getByComp(String compId);

    Boolean checkRoleCode(String roleCode);

    RoleDO create(RoleDO roleDO);

    RoleDO update(RoleDO entity);

    RoleDto getCompAdmin();

    RoleDO getByUuid(String uuid);
}