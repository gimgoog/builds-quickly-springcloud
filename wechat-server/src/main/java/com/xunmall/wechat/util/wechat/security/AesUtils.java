package com.xunmall.wechat.util.wechat.security;

import com.xunmall.wechat.model.constant.WeChatLinkConstant;

public class AesUtils {

    public static String descMessage(String encrypt, String msgSignature,
                                     String timestamp, String nonce) throws AesException {
        String format = "<xml><ToUserName><![CDATA[toUser]]></ToUserName><Encrypt><![CDATA[%1$s]]></Encrypt></xml>";
        String fromXML = String.format(format, encrypt);
        WXBizMsgCrypt pc = new WXBizMsgCrypt(WeChatLinkConstant.TOKEN, WeChatLinkConstant.EncodingAESKey, WeChatLinkConstant.APP_ID);
        String result = pc.decryptMsg(msgSignature, timestamp, nonce, fromXML);
        return result;
    }

    public static String aescMessage(String replyMsg, String timestamp,
                                     String nonce) throws AesException {
        WXBizMsgCrypt pc = new WXBizMsgCrypt(WeChatLinkConstant.TOKEN, WeChatLinkConstant.EncodingAESKey, WeChatLinkConstant.APP_ID);
        String mingwen = pc.encryptMsg(replyMsg, timestamp, nonce);
        return mingwen;
    }
}
