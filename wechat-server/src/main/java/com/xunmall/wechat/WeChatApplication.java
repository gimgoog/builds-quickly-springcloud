package com.xunmall.wechat;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

/**
 * @Author: WangYanjing
 * @Date: 2019/1/17 11:29
 * @Description:
 */
@SpringBootApplication(scanBasePackages = {"com.xunmall"})
@EnableFeignClients(basePackages = {"com.xunmall"})
@EnableEurekaClient
@Slf4j
public class WeChatApplication implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(WeChatApplication.class, args);
    }

    @Override
    public void run(String... arg0) throws Exception {
        log.info("gim-wechat start OK");
    }
}
