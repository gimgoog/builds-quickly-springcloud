package com.xunmall.wechat.service;

import java.util.Map;

/**
 * @Author: WangYanjing
 * @Date: 2019/1/21 14:54
 * @Description:
 */
public interface WeChatOfficialService {
    Map<String,String> getBaseConfigByChannel(String lev2Domain);
}
