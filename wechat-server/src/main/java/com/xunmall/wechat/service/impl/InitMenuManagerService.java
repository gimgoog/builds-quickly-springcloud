package com.xunmall.wechat.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xunmall.wechat.domain.AccessUrlDO;
import com.xunmall.wechat.mapper.AccessUrlDOMapper;
import com.xunmall.wechat.model.AccessToken;
import com.xunmall.wechat.model.constant.WeChatLinkConstant;
import com.xunmall.wechat.model.ui.*;
import com.xunmall.wechat.util.wechat.WeChatGlobalUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by wangyanjing on 2018/11/1.
 */
@Service
public class InitMenuManagerService {

    @Autowired
    private WeChatService weChatService;
    @Autowired
    private AccessUrlDOMapper accessUrlDOMapper;

    public void initWeChatMenu(String channel) {
        List<AccessUrlDO> list = accessUrlDOMapper.getFirstAvailableUrlByType("main");
        ViewButton viewButton1 = new ViewButton();
        ViewButton viewButton2 = new ViewButton();
        if (list != null && list.size() >= 2) {
            viewButton1.setName(list.get(0).getName());
            viewButton1.setType(WeChatLinkConstant.MENU_TYPE_VIEW);
            viewButton1.setUrl(list.get(0).getUrl());
            viewButton2.setName(list.get(1).getName());
            viewButton2.setType(WeChatLinkConstant.MENU_TYPE_VIEW);
            viewButton2.setUrl(list.get(1).getUrl());
        } else {
            viewButton1.setName("上海PM2.5");
            viewButton1.setType(WeChatLinkConstant.MENU_TYPE_VIEW);
            viewButton1.setUrl("http://www.86pm25.com/city/shanghai.html");
            viewButton2.setName("博客园");
            viewButton2.setType(WeChatLinkConstant.MENU_TYPE_VIEW);
            viewButton2.setUrl("https://www.cnblogs.com/");
        }
        ComplexButton complexButton1 = new ComplexButton();
        complexButton1.setName("view菜单");
        complexButton1.setSub_button(new Button[]{viewButton1, viewButton2});

        ClickButton clickButton1 = new ClickButton();
        clickButton1.setName("获取书单");
        clickButton1.setType(WeChatLinkConstant.MENU_TYPE_CLICK);
        clickButton1.setKey("button1");
        ClickButton location_select = new ClickButton();
        location_select.setName("发送位置");
        location_select.setType(WeChatLinkConstant.MENU_TYPE_LOCATION_SELECT);
        location_select.setKey("location_select");
        ComplexButton complexButton2 = new ComplexButton();
        complexButton2.setName("click菜单");
        complexButton2.setSub_button(new Button[]{clickButton1, location_select});

        ClickButton scancode_waitmsg = new ClickButton();
        scancode_waitmsg.setName("扫码带提示");
        scancode_waitmsg.setType(WeChatLinkConstant.MENU_TYPE_SCANCODE_WAITMSG);
        scancode_waitmsg.setKey("waitscancode_waitmsgmsg");
        ClickButton scancode_push = new ClickButton();
        scancode_push.setName("扫码推事件");
        scancode_push.setType(WeChatLinkConstant.MENU_TYPE_SCANCODE_PUSH);
        scancode_push.setKey("scancode_push");
        ClickButton pic_sysphoto = new ClickButton();
        pic_sysphoto.setName("系统拍照发图");
        pic_sysphoto.setType(WeChatLinkConstant.MENU_TYPE_PIC_SYSPHOTO);
        pic_sysphoto.setKey("pic_sysphoto");
        ClickButton pic_photo_or_album = new ClickButton();
        pic_photo_or_album.setName("拍照或者相册发图");
        pic_photo_or_album.setType(WeChatLinkConstant.MENU_TYPE_PIC_PHOTO_OR_ALBUM);
        pic_photo_or_album.setKey("pic_photo_or_album");
        ClickButton pic_weixin = new ClickButton();
        pic_weixin.setName("微信相册发图");
        pic_weixin.setType(WeChatLinkConstant.MENU_TYPE_PIC_WEIXIN);
        pic_weixin.setKey("pic_weixin");
        ComplexButton complexButton3 = new ComplexButton();
        complexButton3.setName("其他菜单");
        complexButton3.setSub_button(new Button[]{scancode_waitmsg, scancode_push, pic_sysphoto, pic_photo_or_album, pic_weixin});

        Menu menu = new Menu();
        menu.setButton(new Button[]{complexButton1, complexButton2, complexButton3});
        AccessToken accessToken = weChatService.getGlobalAccessToken("", "");
        String token = accessToken.getAccessToken();
        ObjectMapper objectMapper = new ObjectMapper();
        String json = null;
        try {
            json = objectMapper.writeValueAsString(menu);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        System.out.println(json);
        boolean flag = WeChatGlobalUtils.createMenu(token, json);
        if (flag) {
            System.out.println("初始化微信公众号Menu成功");
        }
    }
}
