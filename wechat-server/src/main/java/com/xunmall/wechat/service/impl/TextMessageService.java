package com.xunmall.wechat.service.impl;

import com.xunmall.base.util.DateUtils;
import com.xunmall.wechat.model.constant.WeChatLinkConstant;
import com.xunmall.wechat.model.message.req.TextMessage;
import com.xunmall.wechat.util.MessageUtils;
import org.springframework.stereotype.Service;

/**
 * Created by wangyanjing on 2018/11/1.
 */
@Service
public class TextMessageService {

    public String convertTextMessage(TextMessage text) {
        com.xunmall.wechat.model.message.resp.TextMessage textMessage = new com.xunmall.wechat.model.message.resp.TextMessage();
        textMessage.setFromUserName(text.getToUserName());
        textMessage.setToUserName(text.getFromUserName());
        textMessage.setContent(text.getContent());
        textMessage.setMsgType(WeChatLinkConstant.RESP_MESSAGE_TYPE_TEXT);
        textMessage.setCreateTime(DateUtils.now().getTime());
        return MessageUtils.textMessageToXml(textMessage);
    }
}
