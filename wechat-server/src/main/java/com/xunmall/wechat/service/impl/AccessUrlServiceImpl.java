package com.xunmall.wechat.service.impl;

import com.xunmall.wechat.domain.AccessUrlDO;
import com.xunmall.wechat.mapper.AccessUrlDOMapper;
import com.xunmall.wechat.service.AccessUrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: WangYanjing
 * @Date: 2019/1/16 16:58
 * @Description:
 */
@Service
public class AccessUrlServiceImpl implements AccessUrlService {

    @Autowired
    private InitMenuManagerService initMenuManagerService;
    @Autowired
    private AccessUrlDOMapper accessUrlDOMapper;
    @Override
    public void checkAndChangeDomainForWeChat(String url) {
        AccessUrlDO accessUrlDO = accessUrlDOMapper.findAccessUrlByUrl(url);
        if (accessUrlDO != null){
            if(accessUrlDO.getStatus() == 1){
                accessUrlDO.setStatus(0);
                accessUrlDOMapper.updateByPrimaryKey(accessUrlDO);
                initMenuManagerService.initWeChatMenu("");
            }
        }
    }
}
