package com.xunmall.wechat.service;

/**
 * @Author: WangYanjing
 * @Date: 2019/1/16 16:57
 * @Description:
 */
public interface AccessUrlService {
    void checkAndChangeDomainForWeChat(String url);
}
