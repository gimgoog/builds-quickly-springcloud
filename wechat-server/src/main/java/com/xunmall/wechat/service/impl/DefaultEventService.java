package com.xunmall.wechat.service.impl;

import com.xunmall.base.util.DateUtils;
import com.xunmall.wechat.mapper.AccessTokenMapper;
import com.xunmall.wechat.model.AccessToken;
import com.xunmall.wechat.model.constant.WeChatLinkConstant;
import com.xunmall.wechat.model.message.event.ClickEvent;
import com.xunmall.wechat.model.message.event.SubscribeEvent;
import com.xunmall.wechat.model.message.req.BaseMessage;
import com.xunmall.wechat.model.message.resp.Article;
import com.xunmall.wechat.model.message.resp.NewsMessage;
import com.xunmall.wechat.model.message.resp.TextMessage;
import com.xunmall.wechat.util.MessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by wangyanjing on 2018/11/1.
 */
@Service
public class DefaultEventService {
    @Autowired
    private WeChatService weChatService;
    @Autowired
    private AccessTokenMapper accessTokenMapper;

    // 关注公众号时的事件
    public String handlerSubscribeEvent(SubscribeEvent event,String channel) throws IOException {
        String result = "";
        TextMessage textMessage = new TextMessage();
        textMessage.setFromUserName(event.getToUserName());
        textMessage.setToUserName(event.getFromUserName());
        textMessage.setMsgType(WeChatLinkConstant.RESP_MESSAGE_TYPE_TEXT);
        textMessage.setCreateTime(DateUtils.now().getTime());
        String parentOpenId = null;
        if (!"".equals(event.getEventKey())) {
            if (event.getEventKey().startsWith("qrscene")) {
                String eventKye = event.getEventKey();
                String[] eventKyeArray = eventKye.split(";");
                textMessage.setContent("您通过扫描" + eventKyeArray[0].substring(8) + "二维码关注了我们,可回复关键字领取红包");
                parentOpenId = eventKyeArray[1];
            }
            result = MessageUtils.textMessageToXml(textMessage);
        } else {
            NewsMessage newsMessage = new NewsMessage();
            newsMessage.setFromUserName(event.getToUserName());
            newsMessage.setToUserName(event.getFromUserName());
            newsMessage.setMsgType(WeChatLinkConstant.RESP_MESSAGE_TYPE_NEWS);
            newsMessage.setArticleCount(1);
            List<Article> list = new ArrayList<>();
            Article article = new Article();
            article.setTitle("欢迎关注");
            article.setDescription("我愿意等你来，但我不会等太久");
            article.setUrl("https://mp.weixin.qq.com/s/fb8srq2N0DPILJCd9MTcUA");
            article.setPicUrl("http://mmbiz.qpic.cn/mmbiz_jpg/Ya8tLXUQf75q1NMibhvKpz5FiaTZhicibUNzLrEw1msQV9qK66ibiaKXSv14UPFoDibdAx5e1QRGRiaL6WC8Pnj5NQ3KRQ/0");
            list.add(article);
            newsMessage.setArticles(list);
            result = MessageUtils.newsMessageToXml(newsMessage);
        }
        // 生成带参数的二维码地址，并将拉取到的用户信息和二维码地址存入数据库，用户邀请朋友扫码时直接从数据库取出二维码地址分享出去
        Map<String,String> accessTokenMap = accessTokenMapper.getAccessTokenByChannel(channel);
        if(!accessTokenMap.isEmpty()){
            AccessToken accessToken =weChatService.getGlobalAccessToken(accessTokenMap.get("APP_ID"),accessTokenMap.get("APP_SECRET"));
            weChatService.getQRCode(accessToken.getAccessToken(),event.getFromUserName(), parentOpenId);
        }
        return result;
    }

    // 点击菜单拉取消息时的事件推送
    public String handlerClickEvent(ClickEvent event) {
        String eventKey = event.getEventKey();
        TextMessage textMessage = new TextMessage();
        if ("button1".equals(eventKey)) {
            textMessage.setFromUserName(event.getToUserName());
            textMessage.setToUserName(event.getFromUserName());
            textMessage.setContent("抓紧筹备中");
            textMessage.setMsgType(WeChatLinkConstant.RESP_MESSAGE_TYPE_TEXT);
        }
        return MessageUtils.textMessageToXml(textMessage);
    }

    // 点击菜单跳转链接时的事件推送,因为消息类型和InClickEvent类结构相同，所以没有单独建一个类
    public String handlerViewEvent(ClickEvent event) {
        TextMessage textMessage = new TextMessage();
        textMessage.setFromUserName(event.getToUserName());
        textMessage.setToUserName(event.getFromUserName());
        textMessage.setContent("还在构建中");
        textMessage.setMsgType(WeChatLinkConstant.RESP_MESSAGE_TYPE_TEXT);
        return MessageUtils.textMessageToXml(textMessage);
    }

    public String handlerUnSubscribeEvent(BaseMessage event) {
        TextMessage textMessage = new TextMessage();
        textMessage.setFromUserName(event.getToUserName());
        textMessage.setToUserName(event.getFromUserName());
        textMessage.setContent("欢迎再来呀,客官");
        textMessage.setMsgType(WeChatLinkConstant.RESP_MESSAGE_TYPE_TEXT);
        return MessageUtils.textMessageToXml(textMessage);
    }
}
