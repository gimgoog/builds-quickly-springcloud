package com.xunmall.wechat.service.impl;

import com.xunmall.base.util.DateUtils;
import com.xunmall.wechat.model.constant.WeChatLinkConstant;
import com.xunmall.wechat.model.message.req.ImageMessage;
import com.xunmall.wechat.model.message.resp.TextMessage;
import com.xunmall.wechat.util.MessageUtils;
import com.xunmall.wechat.util.wechat.WeChatGlobalUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by wangyanjing on 2018/11/14.
 */
@Service
public class ImageMessageService {

    @Autowired
    private WeChatService weChatService;

    public String convertImageMessage(ImageMessage text) {
        TextMessage textMessage = new TextMessage();
        textMessage.setFromUserName(text.getToUserName());
        textMessage.setToUserName(text.getFromUserName());
        textMessage.setContent(WeChatGlobalUtils.convertLong2ShortUrl(weChatService.getGlobalAccessToken(WeChatLinkConstant.APP_ID,WeChatLinkConstant.APP_SECRET).getAccessToken(), text.getPicUrl()));
        textMessage.setMsgType(WeChatLinkConstant.RESP_MESSAGE_TYPE_TEXT);
        textMessage.setCreateTime(DateUtils.now().getTime());
        return MessageUtils.textMessageToXml(textMessage);
    }
}
