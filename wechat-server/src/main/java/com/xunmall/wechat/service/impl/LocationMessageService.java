package com.xunmall.wechat.service.impl;

import com.xunmall.base.util.DateUtils;
import com.xunmall.wechat.model.constant.WeChatLinkConstant;
import com.xunmall.wechat.model.message.req.LocationMessage;
import com.xunmall.wechat.model.message.resp.TextMessage;
import com.xunmall.wechat.util.MessageUtils;
import org.springframework.stereotype.Service;

/**
 * Created by wangyanjing on 2018/11/14.
 */
@Service
public class LocationMessageService {

    public String convertLocationMessage(LocationMessage text) {
        TextMessage textMessage = new TextMessage();
        textMessage.setFromUserName(text.getToUserName());
        textMessage.setToUserName(text.getFromUserName());
        textMessage.setContent(text.getLabel());
        textMessage.setMsgType(WeChatLinkConstant.RESP_MESSAGE_TYPE_TEXT);
        textMessage.setCreateTime(DateUtils.now().getTime());
        return MessageUtils.textMessageToXml(textMessage);
    }
}
