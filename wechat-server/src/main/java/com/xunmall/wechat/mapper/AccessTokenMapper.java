package com.xunmall.wechat.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * @Author: WangYanjing
 * @Date: 2019/1/22 11:40
 * @Description:
 */
@Repository
public interface AccessTokenMapper {
    Map<String, String> getAccessTokenByChannel(@Param("channel") String channel);
}
