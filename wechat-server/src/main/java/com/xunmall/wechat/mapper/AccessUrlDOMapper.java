package com.xunmall.wechat.mapper;

import com.xunmall.wechat.domain.AccessUrlDO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * @Author: WangYanjing
 * @Date: 2019/1/16 13:57
 * @Description:
 */
@Repository
public interface AccessUrlDOMapper extends Mapper<AccessUrlDO> {
    List<AccessUrlDO> getFirstAvailableUrlByType(@Param("type") String type);

    AccessUrlDO findAccessUrlByUrl(@Param("url") String url);
}
