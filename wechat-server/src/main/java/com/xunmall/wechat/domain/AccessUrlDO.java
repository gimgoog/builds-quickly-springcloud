package com.xunmall.wechat.domain;

import com.xunmall.base.entity.BaseDomain;
import lombok.Data;

import javax.persistence.Table;

/**
 * @Author: WangYanjing
 * @Date: 2019/1/16 13:31
 * @Description:
 */
@Data
@Table(name = "tn_wechat_domain")
public class AccessUrlDO extends BaseDomain {
    private String name;
    private String url;
    private String type;
    private Integer status;
}
