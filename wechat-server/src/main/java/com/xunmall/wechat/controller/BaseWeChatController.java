package com.xunmall.wechat.controller;

import com.xunmall.wechat.util.URLUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * @Author: WangYanjing
 * @Date: 2019/1/22 10:32
 * @Description:
 */
@Controller
@Slf4j
public class BaseWeChatController {

    public String parseDomain(HttpServletRequest request) {
        StringBuffer url = request.getRequestURL();
        System.out.println(url);
        String tempContextUrl = url.delete(url.length() - request.getRequestURI().length(), url.length()).append("/").toString();
        //二级域名解析获取渠道商
        String lev2Domain = URLUtil.getDomain(tempContextUrl, 2).split("\\.")[0];
        return lev2Domain;
    }

    public void write(Object obj, HttpServletResponse response) {
        response.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.print(obj);
            out.flush();
        } catch (Exception e) {
            log.info(e.getMessage());
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }
}
