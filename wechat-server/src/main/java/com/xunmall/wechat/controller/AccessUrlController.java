package com.xunmall.wechat.controller;

import com.xunmall.wechat.service.AccessUrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author: WangYanjing
 * @Date: 2019/1/16 13:28
 * @Description:
 */
@Controller
@RequestMapping(value = "/domain")
public class AccessUrlController {

    @Autowired
    private AccessUrlService accessUrlService;

    @RequestMapping(value = "check_change")
    @ResponseBody
    public void checkAndChangeDomain(@RequestParam(value = "url") String url) {
        accessUrlService.checkAndChangeDomainForWeChat(url);
    }

}
