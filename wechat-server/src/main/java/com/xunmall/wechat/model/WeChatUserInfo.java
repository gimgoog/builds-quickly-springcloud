package com.xunmall.wechat.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;

/**
 * 微信用户个人信息实体类
 * 
 * @author ranfen
 *
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeChatUserInfo implements Serializable {
	private String openid;
	private Long subscribe;
	private String nickname;
	private Long sex;
	private String city;
	private String country;
	private String province;
	private String language;
	private String headimgurl;					 //用户头像
	private String subscribe_time;			    //用户关注时间，为时间戳。如果用户曾多次关注，则取最后关注时间
	private String unionid;
    private String subscribe_scene;				//返回用户关注的渠道来源	ADD_SCENE_QR_CODE 扫描二维码ADD_SCENE_SEARCH 公众号搜索
    private String qr_scene	;					//二维码扫码场景（开发者自定义）
    private String qrcode_url;					//二维码地址
    private String parent_openId;				//上级openid
	private String remark;
	private String groupid;
}