package com.xunmall.wechat.model.message.event;

import lombok.Data;

/**
 * 微信带参二维码
 * @author ranfen
 * @date 2018年11月2日
 *
 */
@Data
public class QRCodeEvent extends BaseEvent{
	// 事件KEY值
	private String EventKey;
	// 用于换取二维码图片
	private String Ticket;
}