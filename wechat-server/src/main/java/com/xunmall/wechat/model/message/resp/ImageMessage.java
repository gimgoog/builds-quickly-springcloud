package com.xunmall.wechat.model.message.resp;

import lombok.Data;

/**
 * Created by wangyanjing on 2018/11/16.
 */
@Data
public class ImageMessage extends BaseMessage {
    private Image Image;
}
