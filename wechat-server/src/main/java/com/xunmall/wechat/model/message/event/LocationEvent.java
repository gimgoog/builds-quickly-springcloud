package com.xunmall.wechat.model.message.event;

import lombok.Data;

/**
 * Created by wangyanjing on 2018/11/16.
 */
@Data
public class LocationEvent extends BaseEvent {
    // 地理位置纬度
    private String Latitude;
    // 地理位置经度
    private String Longitude;
    // 地理位置精度
    private String Precision;
}
