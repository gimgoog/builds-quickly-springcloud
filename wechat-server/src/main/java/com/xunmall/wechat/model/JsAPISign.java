package com.xunmall.wechat.model;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by wangyanjing on 2018/11/16.
 */
@Data
public class JsAPISign implements Serializable{
    private Boolean flag;
    private String appId;
    private String timestamp;
    private String noncestr;
    private String signature;

    public JsAPISign(Boolean flag, String appId, String timestamp, String noncestr, String signature) {
        this.flag = flag;
        this.appId = appId;
        this.timestamp = timestamp;
        this.noncestr = noncestr;
        this.signature = signature;
    }

}
