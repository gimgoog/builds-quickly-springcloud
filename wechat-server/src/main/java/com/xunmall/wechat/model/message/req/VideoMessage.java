package com.xunmall.wechat.model.message.req;

import lombok.Data;

/**
 * Created by wangyanjing on 2018/11/16.
 */
@Data
public class VideoMessage extends BaseMessage {
    // 语音格式
    private String ThumbMediaId;
    // 媒体ID
    private String MediaId;
}
