package com.xunmall.wechat.model.message.event;

import lombok.Data;

/**
 * Created by wangyanjing on 2018/11/16.
 */
@Data
public class MenuEvent extends BaseEvent{
    // 事件KEY值，与自定义菜单接口中KEY值对应
    private String EventKey;
}
