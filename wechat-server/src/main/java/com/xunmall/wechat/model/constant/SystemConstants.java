package com.xunmall.wechat.model.constant;

/**
 * Created by wangyanjing on 2018/11/1.
 */
public interface SystemConstants {
    int DEFAULT_PAGE_INDEX = 0;
    int DEFAULT_PAGE_SIZE = 20;
}
