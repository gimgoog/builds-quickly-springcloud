package com.xunmall.wechat.model.message.req;

import lombok.Data;

/**
 *  文本消息
 * Created by wangyanjing on 2018/10/31.
 */
@Data
public class TextMessage extends BaseMessage {
    // 消息内容
    private String Content;
}
