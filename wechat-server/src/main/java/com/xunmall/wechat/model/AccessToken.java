package com.xunmall.wechat.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by wangyanjing on 2018/11/1.
 */
@Data
public class AccessToken implements Serializable {
    private String accessToken;
    private int expiresIn;
    private Date date;
}
