package com.xunmall.wechat.model.message.resp;

import lombok.Data;

/**
 * Created by wangyanjing on 2018/11/16.
 */
@Data
public class Video {
    // 媒体文件id
    private String MediaId;
    // 缩略图的媒体id
    private String ThumbMediaId;
}
