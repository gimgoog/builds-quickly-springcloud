package com.xunmall.wechat.model.message.req;

import lombok.Data;

/**
 *  * 图片消息
 * Created by wangyanjing on 2018/10/31.
 */
@Data
public class ImageMessage extends BaseMessage {
    // 图片链接
    private String PicUrl;
    private String MediaId;
}
