package com.xunmall.wechat.model.message.resp;

import lombok.Data;

/**
 * Created by wangyanjing on 2018/10/31.
 */
@Data
public class MusicMessage extends BaseMessage {
    // 音乐
    private Music music;
}
