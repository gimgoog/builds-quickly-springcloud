package com.xunmall.wechat.model.message.event;

import lombok.Data;

/**
 * Created by wangyanjing on 2018/11/1.
 */
@Data
public class SubscribeEvent extends BaseEvent {
    private String eventKey;
}
