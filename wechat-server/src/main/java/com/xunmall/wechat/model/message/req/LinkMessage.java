package com.xunmall.wechat.model.message.req;

import lombok.Data;

/**
 * 链接消息
 * Created by wangyanjing on 2018/10/31.
 */
@Data
public class LinkMessage extends BaseMessage {
    // 消息标题
    private String Title;
    // 消息描述
    private String Description;
    // 消息链接
    private String Url;
}
