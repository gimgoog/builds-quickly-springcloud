package com.xunmall.wechat.model.constant;

/**
 * Created by wangyanjing on 2018/11/15.
 */
public enum SubscribeSceneEnum {
    ADD_SCENE_SEARCH("ADD_SCENE_SEARCH", "公众号搜索"),
    ADD_SCENE_ACCOUNT_MIGRATION("ADD_SCENE_ACCOUNT_MIGRATION", "公众号迁移"),
    ADD_SCENE_PROFILE_CARD("ADD_SCENE_PROFILE_CARD", "名片分享"),
    ADD_SCENE_QR_CODE("ADD_SCENE_QR_CODE", "扫描二维码"),
    ADD_SCENEPROFILE_LINK("ADD_SCENEPROFILE_LINK", "图文页内名称点击"),
    ADD_SCENE_PROFILE_ITEM("ADD_SCENE_PROFILE_ITEM", "图文页右上角菜单"),
    ADD_SCENE_PAID("ADD_SCENE_PAID", "支付后关注"),
    ADD_SCENE_OTHERS("ADD_SCENE_OTHERS ", "其他");

    private String key;
    private String value;

    private SubscribeSceneEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return this.key;
    }

    public String getValue() {
        return this.value;
    }

    public static SubscribeSceneEnum getApiTypeEnum(String key) {
        for (SubscribeSceneEnum item : SubscribeSceneEnum.values()) {
            if (item.getKey().equals(key)) {
                return item;
            }
        }
        return null;
    }
}
