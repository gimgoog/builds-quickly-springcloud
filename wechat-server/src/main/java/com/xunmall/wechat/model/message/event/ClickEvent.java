package com.xunmall.wechat.model.message.event;

import com.xunmall.wechat.model.message.req.BaseMessage;
import lombok.Data;

/**
 * Created by wangyanjing on 2018/11/1.
 */
@Data
public class ClickEvent extends BaseMessage {
    private String Event;
    /*
    * 自定义菜单事件
    */
    private String eventKey;
}
