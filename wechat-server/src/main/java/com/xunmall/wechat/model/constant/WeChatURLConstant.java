package com.xunmall.wechat.model.constant;

/**
 * Created by wangyanjing on 2018/11/12.
 */
public class WeChatURLConstant {
    /**
     * 获取access_token的路径
     * 获取access_token的api，以get方式发送https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APP_ID&secret=APP_SECRET
     * 返回的json格式如下{"access_token":"ACCESS_TOKEN","expires_in":7200}
     */
    public static final String TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APP_ID&secret=APP_SECRET";

    /**
     * 获取微信服务器IP地址
     */
    public static final String CALL_BACK_IPS_URL = "  https://api.weixin.qq.com/cgi-bin/getcallbackip?access_token=ACCESS_TOKEN";

    /**
     * 菜单创建接口
     */
    public static final String CREATE_MENU_URL = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN";

    /**
     * 拉取个人信息
     */
    public static final String GET_USER_INFO_URL = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=accessToken&openid=openId&lang=zh_CN";

    /**
     *  创建二维码
     */
    public static final String CREATE_TICKET_URL = "https://api.weixin.qq.com/cgi-bin/qrcode/create";

    /**
     *  通过ticket换取二维码
     */
    public static final String SHOW_QR_CODE_URL = "https://mp.weixin.qq.com/cgi-bin/showqrcode";

    /**
     *  获取分享js页面ticket
     */
    public static final String GET_JS_TICKET_URL ="https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=ACCESS_TOKEN&type=jsapi";
    /**
     * 长链接转短链接接口
     */
    public static final String CONVERT_LINK_SHORT_URL = "https://api.weixin.qq.com/cgi-bin/shorturl";

    /**
     * 公众号静默登陆方法获取code
     */
    public static final String QUIET_LOGIN_CODE_URL = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=APP_ID&redirect_uri=REDIRECT_URI&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect";

    /**
     * 公众号静默登陆方法获取openid
     */
    public static final String QUIET_LOGIN_OPEN_ID_URL ="https://api.weixin.qq.com/sns/oauth2/access_token?appid=APP_ID&secret=APP_SECRET&code=CODE&grant_type=authorization_code";
}
