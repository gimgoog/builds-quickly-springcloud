package com.xunmall.wechat.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by wangyanjing on 2018/11/16.
 */
@Data
public class JsSdkTicket implements Serializable {
    private String ticket;
    private int expiresIn;
    private Date date;
}
