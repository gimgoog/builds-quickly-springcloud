package com.xunmall.wechat.model.message.req;

import lombok.Data;

/**
 *  * 音频消息
 * Created by wangyanjing on 2018/10/31.
 */
@Data
public class VoiceMessage extends BaseMessage {
    // 语音格式
    private String Format;
    // 媒体ID
    private String MediaId;
}
