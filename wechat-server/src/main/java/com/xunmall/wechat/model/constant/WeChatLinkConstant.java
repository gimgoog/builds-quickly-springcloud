package com.xunmall.wechat.model.constant;

/**
 * Created by wangyanjing on 2018/11/14.
 */
public class WeChatLinkConstant {

    public static final String APP_ID = "wx05999eb313a47206";

    public static final String APP_SECRET = "fe896ddbb01fcf1930675a68c9b0f6b4";

    public static final String TOKEN = "abc123";

    public static final String EncodingAESKey = "XRLfiWvxGs9eT2Kt29RtE8IyPzRSrYjpICwMQbNCIo0";

    /**
     * 请求消息类型：文本
     */
    public static final String REQ_MESSAGE_TYPE_TEXT = "text";

    /**
     * 请求消息类型：图片
     */
    public static final String REQ_MESSAGE_TYPE_IMAGE = "image";

    /**
     * 请求消息类型：链接
     */
    public static final String REQ_MESSAGE_TYPE_LINK = "link";

    /**
     * 请求消息类型：地理位置
     */
    public static final String REQ_MESSAGE_TYPE_LOCATION = "location";

    /**
     * 请求消息类型：音频
     */
    public static final String REQ_MESSAGE_TYPE_VOICE = "voice";

    // 请求消息类型：小视频
    public static final String REQ_MESSAGE_TYPE_SHORTVIDEO = "shortvideo";

    /**
     * 请求消息类型：推送
     */
    public static final String REQ_MESSAGE_TYPE_EVENT = "event";

    /**
     * 返回消息类型：文本
     */
    public static final String RESP_MESSAGE_TYPE_TEXT = "text";

    /**
     * 响应消息类型：语音
     */
    public static final String RESP_MESSAGE_TYPE_VOICE = "voice";

    /**
     * 响应消息类型：视频
     */
    public static final String RESP_MESSAGE_TYPE_VIDEO = "video";

    /**
     * 返回消息类型：音乐
     */
    public static final String RESP_MESSAGE_TYPE_MUSIC = "music";

    /**
     * 返回消息类型：图文
     */
    public static final String RESP_MESSAGE_TYPE_NEWS = "news";

    /**
     * 事件类型：subscribe(订阅)
     */
    public static final String EVENT_TYPE_SUBSCRIBE = "subscribe";

    /**
     * 事件类型：unsubscribe(取消订阅)
     */
    public static final String EVENT_TYPE_UNSUBSCRIBE = "unsubscribe";

    /**
     * 事件类型：LOCATION(上报地理位置)
     */
    public static final String EVENT_TYPE_LOCATION = "LOCATION";

    /**
     * 事件类型：CLICK(自定义菜单点击事件)
     */
    public static final String EVENT_TYPE_CLICK = "CLICK";

    /**
     * 事件类型：VIEW(视图)
     */
    public static final String EVENT_TYPE_VIEW = "VIEW";

    /**
     * 临时二维码
     */
    public static final String QR_SCENE = "QR_SCENE";
    /**
     * 临时二维码(字符串)
     */
    public static final String QR_STR_SCENE = "QR_SCENE";
    /**
     * 永久二维码
     */
    public static final String QR_LIMIT_SCENE = "QR_LIMIT_SCENE";

    /**
     * 永久二维码(字符串)
     */
    public static final String QR_LIMIT_STR_SCENE = "QR_LIMIT_STR_SCENE";

    /**
     * 临时二维码过期时间30天
     */
    public static final String EXPIRE_SECONDS = "2592000";

    /**
     * 自定义菜单类型
     */
    public static final String MENU_TYPE_VIEW = "view";
    public static final String MENU_TYPE_CLICK = "click";
    public static final String MENU_TYPE_MINI_PROGRAM = "miniprogram";
    public static final String MENU_TYPE_SCANCODE_WAITMSG = "scancode_waitmsg";
    public static final String MENU_TYPE_SCANCODE_PUSH = "scancode_push";
    public static final String MENU_TYPE_PIC_SYSPHOTO = "pic_sysphoto";
    public static final String MENU_TYPE_PIC_PHOTO_OR_ALBUM = "pic_photo_or_album";
    public static final String MENU_TYPE_PIC_WEIXIN = "pic_weixin";
    public static final String MENU_TYPE_LOCATION_SELECT = "location_select";
    public static final String MENU_TYPE_MEDIA_ID = "media_id";
    public static final String MENU_TYPE_VIEW_LIMITED = "view_limited";

}
