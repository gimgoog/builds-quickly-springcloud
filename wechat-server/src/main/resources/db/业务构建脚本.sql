DROP TABLE IF EXISTS `tn_wechat_domain`;
CREATE TABLE `tn_wechat_domain` (
`id` VARCHAR (22) NOT NULL,
`name` varchar(255) DEFAULT NULL COMMENT '名称',
`url` varchar(255) DEFAULT NULL COMMENT '域名',
`type` varchar(255) DEFAULT NULL COMMENT '类型',
`status` int DEFAULT 1 COMMENT '可用状态',
`create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
`update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '配置域名表';

DROP TABLE IF EXISTS `tn_wechat_channel`;
CREATE TABLE `tn_wechat_channel` (
`id` VARCHAR (22) NOT NULL,
`type` int(1) NOT NULL DEFAULT '1' COMMENT '类型',
`wechat` varchar(100) NOT NULL COMMENT '微信号',
`open_id` varchar(100) NOT NULL COMMENT '原始ID',
`nickname` varchar(50) NOT NULL COMMENT '昵称',
`head_url` varchar(200) NOT NULL COMMENT '公众号头像地址',
`appid` varchar(100) NOT NULL COMMENT 'App ID',
`appsecret` varchar(100) NOT NULL COMMENT 'App Secret',
`token` varchar(100) DEFAULT NULL COMMENT '令牌',
`aes_key` varchar(100) DEFAULT NULL COMMENT '消息加解密秘钥',
`follow_mode` int(1) DEFAULT '1' COMMENT '关注模式',
`subscribe_url` varchar(100) DEFAULT NULL COMMENT '如果不填，则默认使用公众号的二维码引导关注',
`kefu_qrcode_url` varchar(200) DEFAULT NULL COMMENT '默认由平台客服提供支持',
`channel` varchar(100) NOT NUll COMMENT '渠道商',
`create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
`update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '渠道商与公众号关系表';
























