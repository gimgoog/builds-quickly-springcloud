$.get('/JSSDKConfig', {regurl: location.href.split('#')[0]}, function (res) {
    wx.config({
        debug: res.flag, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
        appId: res.appId, // 必填，公众号的唯一标识
        timestamp: res.timestamp, // 必填，生成签名的时间戳<?= $data['timestamp']?>
        nonceStr: res.noncestr,// 必填，生成签名的随机串<?= $data['noncestr']?>
        signature: res.signature,// 必填，签名<?= $data['signature']?>
        jsApiList: ['onMenuShareTimeline',
            'onMenuShareAppMessage',
            'onMenuShareQQ',
            'onMenuShareWeibo',
            'onMenuShareQZone']// 必填，需要使用的JS接口列表
    });
}).fail(function () {
    console.log('微信分享获取后台参数ajax失败！');
})

var wxShare = {
    ok: function () {
        var self = this;
        self.title = self.title;
        self.desc = self.desc;
        self.link = self.link;
        self.imgUrl = self.imgUrl;
        wx.ready(function () {
            var obj = {//朋友圈
                title: self.title,
                desc: self.desc,
                link: self.link,
                imgUrl: self.imgUrl,
                success: function () {
                    console.log('分享给朋友成功！');
                    if (typeof self.successCallback === 'function') {
                        self.successCallback();
                    }
                },
                cancel: function () {
                    console.log('分享给朋友失败！');
                    if (typeof self.cancelCallback === 'function') {
                        self.cancelCallback();
                    }
                }
            };

            // 2.1 监听“分享给朋友”，按钮点击、自定义分享内容及分享结果接口
            wx.onMenuShareAppMessage(obj);

            // 2.2 监听“分享到朋友圈”按钮点击、自定义分享内容及分享结果接口
            wx.onMenuShareTimeline(obj);

            // 2.3 监听“分享到QQ”按钮点击、自定义分享内容及分享结果接口
            wx.onMenuShareQQ(obj);

            // 2.4 监听“分享到微博”按钮点击、自定义分享内容及分享结果接口
            wx.onMenuShareWeibo(obj);

            // 2.5 监听“分享到QZone”按钮点击、自定义分享内容及分享接口
            wx.onMenuShareQZone(obj);
        });
    }
}

