package com.xunmall.engine;


import com.xunmall.base.util.DateUtils;
import com.xunmall.base.util.OrderNumUtils;
import com.xunmall.engine.entity.EmployeeDO;
import com.xunmall.engine.service.EmployeeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Random;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ElasticTest {

    @Autowired
    private EmployeeService employeeService;

    @Test
    public void contextLoads() {
        System.out.println(employeeService.count());
    }

    @Test
    public void testInsert() {
        EmployeeDO employeeDO = new EmployeeDO();
        employeeDO.setEmpNo(OrderNumUtils.makeOrderNum(""));
        employeeDO.setName("WangYan");
        employeeDO.setGender("M");
        employeeDO.setBirthDate(DateUtils.now());
        employeeDO.setAge(new Random().nextInt(99));
        employeeDO.setAddress(employeeDO.getAge() % 2 == 0 ? "北京" : "上海");
        employeeDO.setDetails(employeeDO.getName() + "来之" + employeeDO.getAddress() + ", 今年" + employeeDO.getAge() + "岁," + "谢谢大家！");
        employeeService.save(employeeDO);

        employeeDO = new EmployeeDO();
        employeeDO.setEmpNo(OrderNumUtils.makeOrderNum(""));
        employeeDO.setName("WangBen");
        employeeDO.setGender("M");
        employeeDO.setBirthDate(DateUtils.now());
        employeeDO.setAge(new Random().nextInt(99));
        employeeDO.setAddress(employeeDO.getAge() % 2 == 0 ? "北京" : "上海");
        employeeDO.setDetails(employeeDO.getName() + "来之" + employeeDO.getAddress() + ", 今年" + employeeDO.getAge() + "岁," + "谢谢大家！");
        employeeService.save(employeeDO);

        employeeDO = new EmployeeDO();
        employeeDO.setEmpNo(OrderNumUtils.makeOrderNum(""));
        employeeDO.setName("WangBuji");
        employeeDO.setGender("M");
        employeeDO.setBirthDate(DateUtils.now());
        employeeDO.setAge(new Random().nextInt(99));
        employeeDO.setAddress(employeeDO.getAge() % 2 == 0 ? "北京" : "上海");
        employeeDO.setDetails(employeeDO.getName() + "来之" + employeeDO.getAddress() + ", 今年" + employeeDO.getAge() + "岁," + "谢谢大家！");
        employeeService.save(employeeDO);

        employeeDO = new EmployeeDO();
        employeeDO.setEmpNo(OrderNumUtils.makeOrderNum(""));
        employeeDO.setName("WangXiaotong");
        employeeDO.setGender("G");
        employeeDO.setBirthDate(DateUtils.now());
        employeeDO.setAge(new Random().nextInt(99));
        employeeDO.setAddress(employeeDO.getAge() % 2 == 0 ? "北京" : "上海");
        employeeDO.setDetails(employeeDO.getName() + "来之" + employeeDO.getAddress() + ", 今年" + employeeDO.getAge() + "岁," + "谢谢大家！");
        employeeService.save(employeeDO);

        employeeDO = new EmployeeDO();
        employeeDO.setEmpNo(OrderNumUtils.makeOrderNum(""));
        employeeDO.setName("LiHei");
        employeeDO.setGender("G");
        employeeDO.setBirthDate(DateUtils.now());
        employeeService.save(employeeDO);

    }

    @Test
    public void testDelete() {
        EmployeeDO employeeDO = new EmployeeDO();
        employeeDO.setEmpNo("20181009162230000009");
        employeeService.delete(employeeDO);
    }

    @Test
    public void testGetAll() {
        Iterable<EmployeeDO> iterable = employeeService.getAll();
        iterable.forEach(e -> System.out.println(e.toString()));
    }

    @Test
    public void testGetByName() {
        List<EmployeeDO> list = employeeService.getByName("WangYan");
        System.out.println(list);
    }

    @Test
    public void testPage() {
        Page<EmployeeDO> page = employeeService.pageQuery(0, 10, "WangYan");
        System.out.println(page.getTotalPages());
        System.out.println(page.getNumber());
        System.out.println(page.getContent());
    }

}
