package com.xunmall.engine.service;

import com.xunmall.engine.entity.EmployeeDO;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 *
 * @author wangyanjing
 * @date 2018/9/11
 */
public interface EmployeeService {
    long count();

    EmployeeDO save(EmployeeDO employeeDO);

    void delete(EmployeeDO employeeDO);

    Iterable<EmployeeDO> getAll();

    List<EmployeeDO> getByName(String name);

    Page<EmployeeDO> pageQuery(Integer pageNo, Integer pageSize, String kw);
}
