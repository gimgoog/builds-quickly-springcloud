package com.xunmall.engine.repository;

import com.xunmall.engine.entity.EmployeeDO;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author wangyanjing
 * @date 2018/9/11
 */
@Repository
public interface EmployeeRepository extends ElasticsearchRepository<EmployeeDO, String> {

}
