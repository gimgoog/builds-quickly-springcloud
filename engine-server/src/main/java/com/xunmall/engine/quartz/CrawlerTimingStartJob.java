package com.xunmall.engine.quartz;

import com.xunmall.base.util.DateUtils;
import com.xunmall.base.util.OrderNumUtils;
import com.xunmall.base.util.RandomUtils;
import com.xunmall.engine.entity.EmployeeDO;
import com.xunmall.engine.repository.EmployeeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Random;
import java.util.concurrent.ExecutionException;

/**
 *
 * @author wangyanjing
 * @date 2018/9/11
 */
@Slf4j
public class CrawlerTimingStartJob {

    @Autowired
    EmployeeRepository employeeRepository;

    public void startCrawlerProcess() throws ExecutionException, InterruptedException {
        log.info("*************************爬虫程序抓取开始*******************************");
        EmployeeDO employeeDO = new EmployeeDO();
        employeeDO.setEmpNo(OrderNumUtils.makeOrderNum(""));
        employeeDO.setName("Wang" + RandomUtils.generateUpperString(3));
        employeeDO.setGender(employeeDO.getEmpNo().endsWith("2") ? "M" : "F");
        employeeDO.setBirthDate(DateUtils.now());
        employeeDO.setAge(new Random().nextInt(99));
        employeeDO.setAddress(employeeDO.getAge() % 2 == 0 ? "北京" : "上海");
        employeeDO.setDetails(employeeDO.getName() + "来之" + employeeDO.getAddress() + ", 今年" + employeeDO.getAge() + "岁," + "谢谢大家！");
        employeeRepository.save(employeeDO);
        log.info("*************************爬虫程序抓取结束*******************************");
    }
}
