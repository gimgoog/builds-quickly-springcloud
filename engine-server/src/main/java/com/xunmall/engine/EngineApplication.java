package com.xunmall.engine;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

/**
 *
 * @author wangyanjing
 * @date 2018/9/11
 */
@EnableEurekaClient
@EnableFeignClients(basePackages = "com.xunmall")
@SpringBootApplication(scanBasePackages = "com.xunmall", exclude = {DataSourceAutoConfiguration.class})
@Slf4j
public class EngineApplication implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(EngineApplication.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {
        log.info("engine-server start OK");
    }
}
