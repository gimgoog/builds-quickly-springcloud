
package com.xunmall.engine.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * @author wangyanjing
 */
@Configuration
@ImportResource(locations = {"classpath:config/spring-quartz.xml"})
public class QuartzConfig {
}

