package com.xunmall.engine.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
/**
 * @author wangyanjing
 */
@Configuration
@EnableElasticsearchRepositories(basePackages = "com.xunmall.engine.repository")
public class ElasticConfig {

}


