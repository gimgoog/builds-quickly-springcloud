package com.xunmall.engine.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;
import java.util.Date;

@Document(indexName = "company", type = "employee", shards = 3, replicas = 0)
@Data
public class EmployeeDO implements Serializable {
    @Id
    private String empNo;

    private String name;

    private String gender;

    private Date birthDate;

    private Integer age;

    private String address;

    private String details;
}
