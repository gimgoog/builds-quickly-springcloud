package com.xunmall.security.password;

import com.xunmall.security.SecUser;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
public class ShaSaltSource implements SaltSource {
	@Override
	public Object getSalt(UserDetails user) {
		return ((SecUser)user).getSalt();
	}
}