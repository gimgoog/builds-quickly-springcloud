package com.xunmall.security.password;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
public interface NoPasswordValid {
    boolean valid(String openId, String encPass, String loginType);
}
