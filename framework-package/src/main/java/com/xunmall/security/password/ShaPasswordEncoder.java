package com.xunmall.security.password;

import com.xunmall.base.util.SpringUtils;
import com.xunmall.base.util.encode.EncodeUtils;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.springframework.security.authentication.encoding.PasswordEncoder;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
public class ShaPasswordEncoder implements PasswordEncoder {
    public static final String HASH_ALGORITHM = "SHA-256";
    public static final int HASH_INTERATIONS = 1024;
    public static final int SALT_SIZE = 8;

    @Override
    public String encodePassword(String rawPass, Object salt) {
        if (salt == null) {
            return null;
        }
        SimpleHash hash = new SimpleHash(HASH_ALGORITHM, rawPass, ByteSource.Util.bytes(EncodeUtils.hexDecode((String) salt)), HASH_INTERATIONS);
        return hash.toHex();
    }

    @Override
    public boolean isPasswordValid(String encPass, String rawPass, Object salt) {
        SimpleHash hash = new SimpleHash(HASH_ALGORITHM, rawPass, ByteSource.Util.bytes(EncodeUtils.hexDecode((String) salt)), HASH_INTERATIONS);
        String hex = hash.toHex();
        return hex.equals(encPass);

    }
}