package com.xunmall.security.acl.utils;

import net.sf.jsqlparser.statement.select.Limit;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
public class SqlLimit extends Limit {

	public SqlLimit(Limit limit){
		this.setOffset(limit.getOffset());
		this.setRowCount(limit.getRowCount());
		this.setRowCountJdbcParameter(limit.isRowCountJdbcParameter());
		this.setOffsetJdbcParameter(limit.isOffsetJdbcParameter());
		this.setLimitAll(limit.isLimitAll());
		this.setLimitNull(limit.isLimitNull());
	}

	@Override
	public String toString() {
		if(this.isOffsetJdbcParameter() || this.isRowCountJdbcParameter()) {
			String retVal = "";
			if (this.isLimitNull()) {
				retVal += " LIMIT NULL";
			} else{
				retVal += " LIMIT " + (this.isOffsetJdbcParameter() ? "?" : Long.toString(this.getOffset()))
						+ "," + (this.isRowCountJdbcParameter() ? "?" : Long.toString(this.getRowCount()));
			}
			return retVal;
		}else{
			return super.toString();
		}
	}
}
