package com.xunmall.security.acl.annotation;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
public enum ControlMode {
	NONE(0),
	SQL_WRAP(1),
	SQL_MODIFY(2);

	private int value;

	ControlMode(int value) {
		this.value= value;
	}

	public int getValue() {
		return value;
	}
}
