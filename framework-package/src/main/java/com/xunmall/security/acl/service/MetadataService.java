package com.xunmall.security.acl.service;

import com.xunmall.security.acl.entity.Metadata;
import com.xunmall.security.acl.mapper.MetadataMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Lazy
@Service
public class MetadataService {
	@Autowired
	MetadataMapper metadataMapper;

	public List<Metadata> getAll(){
		Condition condition=new Condition(Metadata.class);
		Example.Criteria criteria =condition.createCriteria();
		criteria.andEqualTo("deleteFlag",0);
		return metadataMapper.selectByExample(condition);
	}
}
