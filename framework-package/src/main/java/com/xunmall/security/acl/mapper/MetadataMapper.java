package com.xunmall.security.acl.mapper;


import com.xunmall.security.acl.entity.Metadata;
import tk.mybatis.mapper.common.Mapper;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
public interface MetadataMapper extends Mapper<Metadata> {
}
