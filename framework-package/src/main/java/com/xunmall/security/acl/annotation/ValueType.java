package com.xunmall.security.acl.annotation;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
public enum ValueType {
    DEPARTMENT(1),
    ROLE(2),
    PERMISSION(3);

    private int value;
    private static Map<Integer, ValueType> map = Maps.newHashMap();

    static {
        for (ValueType type : ValueType.values()) {
            map.put(type.value, type);
        }
    }

    public static ValueType valueOf(int value) {
        return map.get(value);
    }


    ValueType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}
