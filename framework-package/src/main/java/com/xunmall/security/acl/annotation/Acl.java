package com.xunmall.security.acl.annotation;

import java.lang.annotation.*;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description: 数据权限注解，添加在类或方法上
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
@Inherited
@Documented
public @interface Acl {

    /**
     * ACL控制方式
     *
     * @return
     */
    ControlMode controlMode() default ControlMode.SQL_MODIFY;

    /**
     * sql_wrap时，使用的控制字段（如，depa_uuid）
     *
     * @return
     */
    String columnName() default "";

    /**
     * sql_wrap是，使用的控制值类型（数据从当前登录用户session中获取）
     *
     * @return
     */
    ValueType valueType() default ValueType.DEPARTMENT;
}
