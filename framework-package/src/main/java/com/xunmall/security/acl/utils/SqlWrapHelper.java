package com.xunmall.security.acl.utils;


import com.xunmall.security.acl.annotation.ValueType;

import java.util.Set;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:  通过包裹select，添加where查询，实现ACL
 */
public class SqlWrapHelper extends AclHelper {

    public static String processSql(String originalSql, String columnName, ValueType vt) {
        Set<String> controlValue = getControlValue(vt);
        if (controlValue != null && controlValue.size() > 0) {
            StringBuilder builder = new StringBuilder();
            builder.append("select * from (");
            builder.append(originalSql);

            String inValue = "";
            for (String value : controlValue) {
                inValue += "'" + value + "',";
            }
            inValue = inValue.substring(0, inValue.length() - 1);
            builder.append(") t1 where t1." + columnName + " in (" + inValue + ")");
            return builder.toString();
        } else {
            return originalSql;
        }
    }
}
