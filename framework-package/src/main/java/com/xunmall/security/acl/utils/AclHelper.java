package com.xunmall.security.acl.utils;

import com.google.common.collect.Sets;
import com.xunmall.security.GlobalSessionUtil;
import com.xunmall.security.acl.annotation.ValueType;

import java.util.Set;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description: ACL工具类的父类，提供公用方法
 */
public class AclHelper {
    protected static Set<String> getControlValue(ValueType vt) {
        switch (vt) {
            case PERMISSION:
                return GlobalSessionUtil.getPermissions();
            case ROLE:
                return GlobalSessionUtil.getRoles();
            default:
                return Sets.newHashSet();
        }
    }
}
