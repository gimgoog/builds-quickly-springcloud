package com.xunmall.security.acl.entity;

import com.xunmall.base.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description: ACL的元数据
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Table(name = "t_acl_metadata")
public class Metadata extends BaseEntity {
    /**
     * 表名
     */
    @Column(name = "TABLE_NAME")
    private String tableName;

    /**
     * 字段名
     */
    @Column(name = "COLUMN_NAME")
    private String columnName;

    /**
     * 值类型：1-部门；2-角色；3-权限
     */
    @Column(name = "VALUE_TYPE")
    private int valueType;
}
