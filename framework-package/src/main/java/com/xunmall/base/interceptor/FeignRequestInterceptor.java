package com.xunmall.base.interceptor;


import com.xunmall.base.constants.SecurityConstants;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Component;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Component
@ConditionalOnBean(GlobalProcessIdFilter.class)
@Slf4j
public class FeignRequestInterceptor implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate template) {
        //在header中插入追踪数据
        try {
            if (StringUtils.isNotEmpty(GlobalProcessIdFilter.getAccessToken())) {
                template.header(SecurityConstants.ACCESS_TOKEN, GlobalProcessIdFilter.getAccessToken());
            }
            if (StringUtils.isNotEmpty(GlobalProcessIdFilter.getUserUuid())) {
                template.header(SecurityConstants.USER_UID, GlobalProcessIdFilter.getUserUuid());
            }
        } catch (Exception e) {
            log.error("header set tracking data error.", e);
        }
    }
}