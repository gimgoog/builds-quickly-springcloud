package com.xunmall.base.entity;

import com.xunmall.base.util.OrderNumUtils;
import lombok.Data;
import org.apache.commons.lang.StringUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Data
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@MappedSuperclass
public class BaseDomain implements ICurrentUser, Serializable {

    @Id
    public String id;

    @Column(name = "create_time", updatable = false)
    public Date createTime = new Date();

    @Column(name = "update_time")
    public Date updateTime = new Date();

    @PreUpdate
    void onUpdate() {
        this.updateTime = new Date();
    }

    public BaseDomain buildForInsert() {
        if (StringUtils.isBlank(this.id)) {
            this.setId(OrderNumUtils.makeOrderNum(""));
        }

        if (Objects.isNull(this.createTime)) {
            this.setCreateTime(new Date());
        }
        if (Objects.isNull(this.updateTime)) {
            this.setUpdateTime(new Date());
        }
        return this;
    }

    public BaseDomain buildForUpdate() {
        this.setUpdateTime(new Date());
        return this;
    }
}