package com.xunmall.base.entity;

import com.xunmall.security.GlobalSessionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
public interface ICurrentUser {
    default String currentUserId() {
        Logger log = LoggerFactory.getLogger(getClass());
        String userId = null;
        try {
            userId = GlobalSessionUtil.getUserUuid();
        } catch (Exception e) {
            log.warn("获取当前userUuid失败.", e);
        }
        return userId;
    }
}
