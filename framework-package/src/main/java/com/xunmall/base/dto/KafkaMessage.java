package com.xunmall.base.dto;

import com.google.common.collect.Maps;
import com.xunmall.base.util.UuidUtils;
import lombok.Data;

import java.util.Map;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description: Kafka消息类
 */
@Data
public class KafkaMessage<T> {
    private String id;
    private Map<String, Object> headers;
    private T content;

    public KafkaMessage(){
    }
    public KafkaMessage(T content) {
        this(Maps.newHashMap(), content);
    }
    public KafkaMessage(Map<String, Object> headers, T content) {
        this(UuidUtils.base58Uuid(), headers, content);
    }
    public KafkaMessage(String id,Map<String, Object> headers, T content) {
        this.id = id;
        this.headers = headers;
        this.content = content;
    }

    public void setHeader(String key, Object value) {
        headers.put(key, value);
    }

    public Object getHeader(String key) {
        return headers.get(key);
    }
}
