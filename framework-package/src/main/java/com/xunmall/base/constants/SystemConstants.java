package com.xunmall.base.constants;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
public interface SystemConstants {
	int DEFAULT_PAGE_INDEX = 1;
	int DEFAULT_PAGE_SIZE = 20;

	/**
	 * 是
	 */
	String CODE_YES = "1";
	/**
	 * 否
	 */
	String CODE_NO="0";
}
