package com.xunmall.base.constants;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
public interface SecurityConstants {
    String GLOBAL_PID = "global_pid";
    String ACCESS_TOKEN = "accessToken";
    String USER_UID = "userUid";

    String COMPUUID = "CompUuid";
    String COMPNAME = "compName";
    //是否是多公司
    String IS_MULTI_COMPANY = "isMultiCompany";
    //登陆clientId
    String CLIENT_ID = "clientId";
    //web登陆clientId
    String CLIENT_ID_WEB = "client_web";
    //ios登陆clientId
    String CLIENT_ID_IOS = "client_ios";
    //android登陆clientId
    String CLIENT_ID_ANDROID = "client_android";

    //登陆员工id
    String EMPL_UUID = "EmplUuid";
    String ROLES = "Roles";
    String PERMISSIONS = "Permissions";
    String DEPARTMENTS = "Departments";

    //登录是否需要验证码
    String CODE_JCAPTCHA_NEED = "jcaptchaCodeNeed";
    //验证码
    String CAPTCHA = "captcha";
    //短信验证码
    String SMSCODE = "smsCode";

    //版本ID
    String VERSIONUUID = "versionUuid";
    //版本名称
    String VERSIONNAME = "versionName";
}