package com.xunmall.base.logger;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class TraceLogInfo extends BaseLogInfo {

	private String traceMessage;
	
	private TraceLogInfo(Object obj, String systemName, String traceMessage) {
		super(obj, systemName, "TraceLog");
		this.traceMessage = traceMessage;
	}
	
	public static TraceLogInfo build(Object obj, String systemName, String traceMessage) {
		return new TraceLogInfo(obj, systemName, traceMessage);
	}
}
