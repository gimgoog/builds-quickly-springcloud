package com.xunmall.base.logger;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xunmall.base.interceptor.GlobalProcessIdFilter;
import com.xunmall.base.util.JsonUtils;
import lombok.Data;
import org.springframework.data.mongodb.core.index.Indexed;

import java.util.Date;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Data
public class BaseLogInfo {
    private String type;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS", timezone = "GMT+8")
    @Indexed
    private Date logTime;
    private String systemId;
    private String className;

    /**
     * 全局流水号
     */
    @Indexed
    private String globalProcessUuid;
    @Indexed
    private String token;
    private String userUuid;


    public BaseLogInfo() {
        setLogTime(new Date());
        setToken(GlobalProcessIdFilter.getAccessToken());
        setUserUuid(GlobalProcessIdFilter.getUserUuid());

    }

    public BaseLogInfo(Object obj, String systemName, String type) {
        this();
        setType(type);
        setSystemId(systemName);
        if (obj instanceof String) {
            setClassName((String) obj);
        } else {
            setClassName(obj.getClass().getName());
        }
    }

    public String toJson() {
        return JsonUtils.toJson(this);
    }
}
