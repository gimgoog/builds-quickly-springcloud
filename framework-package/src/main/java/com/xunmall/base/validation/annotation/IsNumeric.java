package com.xunmall.base.validation.annotation;


import com.xunmall.base.validation.validator.IsNumericValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description: 验证数字
 */
@Documented
@Constraint(validatedBy = IsNumericValidator.class)
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
@Retention(RUNTIME)
public @interface IsNumeric {

    String locale() default "zh_CN";

    String type() default "number";

    String message() default "{com.xunmall.base.validation.numeric.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    @Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
    @Retention(RUNTIME)
    @Documented
    public @interface List {
        IsNumeric[] value();
    }
}
