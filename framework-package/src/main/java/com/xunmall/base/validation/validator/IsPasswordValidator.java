package com.xunmall.base.validation.validator;

import com.xunmall.base.util.StringUtils;
import com.xunmall.base.validation.annotation.IsPassword;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
public class IsPasswordValidator implements ConstraintValidator<IsPassword, String> {

    private Map<String, String[]> patterns;

    private String type;

    private String locale;

    @Override
    public void initialize(IsPassword constraintAnnotation) {
        this.patterns = new HashMap<String, String[]>();
        // 密码允许大小写字母 数字 !@#$%^&*
        this.patterns.put("zh_CN", new String[]{"^[\\w!@#＃$%^&＆*.]+$"});
        this.type = constraintAnnotation.type();
        this.locale = constraintAnnotation.locale();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (StringUtils.isEmpty(value)) {
            return true;
        }
        if (!patterns.containsKey(locale)) {
            return true;
        }
        return StringUtils.match(value, patterns.get(locale)[0]);
    }

}
