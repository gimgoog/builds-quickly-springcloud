package com.xunmall.base.validation.validator;


import com.xunmall.base.util.StringUtils;
import com.xunmall.base.validation.annotation.IsChineseChar;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
public class IsChineseCharValidator implements ConstraintValidator<IsChineseChar, String> {

    private String pattern = "^[\\u4e00-\\u9fa5]*$";

    //    private String locale;
    @Override
    public void initialize(IsChineseChar constraintAnnotation) {
//        this.locale = constraintAnnotation.locale();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (StringUtils.isEmpty(value)) {
            return true;
        }
        return StringUtils.match(value, pattern);
    }
}
