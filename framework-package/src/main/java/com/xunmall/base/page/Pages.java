package com.xunmall.base.page;

import java.util.List;

import com.github.pagehelper.PageHelper;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@SuppressWarnings("all")
public class Pages {

	public static<T> PagingModel<T> query(PageInfo condition, Query<T> query) {

		if (condition.getOrder() == null) {
            PageHelper.startPage(condition.getPageIndex(), condition.getPageSize());
        } else {
            PageHelper.startPage(condition.getPageIndex(), condition.getPageSize(), condition.getOrder());
        }

		List<T> results = query.doQuery();
		com.github.pagehelper.Page pageList = (com.github.pagehelper.Page) results;
		PagingModel pm = new PagingModel<>();
		pm.setPageIndex(pageList.getPageNum());
		pm.setPageSize(pageList.getPageSize());
		pm.setTotal(pageList.getTotal());
		pm.setResults(pageList.getResult());
		return pm;
	}
}
