package com.xunmall.base.page;

import java.util.List;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@FunctionalInterface
public interface Query<T> {

	public List<T> doQuery();
}
