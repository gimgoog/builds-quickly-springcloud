package com.xunmall.base.page;

import com.xunmall.base.constants.SystemConstants;
import lombok.Data;
import tk.mybatis.mapper.util.StringUtil;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Data
public class PageInfo implements java.io.Serializable {

	private static final long serialVersionUID = -5214737781026620938L;

	private int pageIndex = SystemConstants.DEFAULT_PAGE_INDEX;

	private int pageSize = SystemConstants.DEFAULT_PAGE_SIZE;

	private long total;
	
	private String order;

	public PageInfo() {}

	public PageInfo(Integer pageIndex, Integer pageSize) {
		if (pageIndex == null || pageIndex == 0) {
            this.pageIndex = SystemConstants.DEFAULT_PAGE_INDEX;
        } else {
            this.pageIndex = pageIndex;
        }
		if (pageSize == null || pageSize == 0) {
            this.pageSize = SystemConstants.DEFAULT_PAGE_SIZE;
        } else {
            this.pageSize = pageSize;
        }
	}
	
	public PageInfo(Integer pageIndex, Integer pageSize, String order) {
		this(pageIndex, pageSize);
		if (order != null) {
            this.order = StringUtil.camelhumpToUnderline(order);
        }
	}

}