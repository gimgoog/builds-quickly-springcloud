package com.xunmall.base.page;

import com.xunmall.base.constants.SystemConstants;
import lombok.Data;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Data
public class PagingModel<T> {

	private int pageIndex = SystemConstants.DEFAULT_PAGE_INDEX;

	private int pageSize = SystemConstants.DEFAULT_PAGE_SIZE;

	private long total;

	private List<T> results;

	public <R> PagingModel<R> convert(Function<? super T, ? extends R> mapper) {
		PagingModel<R> ret = new PagingModel<>();
		ret.setPageIndex(this.getPageIndex());
		ret.setPageSize(this.getPageSize());
		ret.setTotal(this.getTotal());
		if (!CollectionUtils.isEmpty(this.getResults())) {
			ret.setResults(this.getResults().stream()
					.map(mapper)
					.filter(o -> o != null)
					.collect(Collectors.toList())
			);
		}
		return ret;
	}

	public List<T> getResults() {
		if (results == null) {
			return Collections.emptyList();
		} else {
			return results;
		}
	}

}
