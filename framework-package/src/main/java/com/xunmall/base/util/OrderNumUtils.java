package com.xunmall.base.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description: <p>订单流水号生成器，最大并发数量为10万</p>
 */
public class OrderNumUtils {
    /**
     * 锁对象，可以为任意对象
     */
    private static Object lockObj = "Rocky.J";
    /**
     * 订单号生成计数器
     */
    private static long orderNumCount = 1L;
    /**
     * 每毫秒生成订单号数量最大值
     */
    private static int maxPerMSECSize = 1000;

    /**
     * 生成非重复订单号，理论上限1毫秒1000个，可扩展
     *
     * @param startName 测试用
     */
    public static String makeOrderNum(String startName) {
        try {
            // 最终生成的订单号
            String finOrderNum = "";
            synchronized (lockObj) {
                // 取系统当前时间作为订单号变量前半部分，精确到毫秒
                long nowLong = Long.parseLong(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));
                // 计数器到最大值归零，可扩展更大，目前1毫秒处理峰值100000个，1秒10000万
                if (orderNumCount > maxPerMSECSize) {
                    orderNumCount = 1L;
                }
                // 组装订单号
                String countStr = maxPerMSECSize + orderNumCount + "";
                finOrderNum = startName.toUpperCase() + nowLong + countStr.substring(1);
                orderNumCount++;
            }
            return finOrderNum;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 16位订单号
     *
     * @param machineId
     * @return
     */
    public static String genId(String machineId) {
        String orderId =
                machineId +
                        (System.currentTimeMillis() + "").substring(1) +
                        (System.nanoTime() + "").substring(7, 10);
        System.out.println(orderId);
        return orderId;
    }
}
