package com.xunmall.base.util;


import com.xunmall.base.exception.BaseException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Slf4j
public class WebUtils {

    static public void sendJsonError(HttpServletRequest request, HttpServletResponse response, BaseException e, HttpStatus status) {
        ServletOutputStream out = null;
        try {
            response.setStatus(status.value());
            response.setContentType("application/json;charset=UTF-8");
            String msg = e.getJsonMessage();
            byte[] msgbyte = msg.getBytes("UTF-8");
            response.setContentLength(msgbyte.length);
            out = response.getOutputStream();
            out.write(msgbyte);
            out.close();
        } catch (IOException e1) {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    public static String getRemoteAddr(HttpServletRequest request) {
        String remoteAddr = request.getRemoteAddr();
        if (remoteAddr == null || remoteAddr.trim().length() <= 0) {
            remoteAddr = "unknown";
        }

        return "0:0:0:0:0:0:0:1".equals(remoteAddr) ? "127.0.0.1" : remoteAddr;
    }
}
