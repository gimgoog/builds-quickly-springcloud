package com.xunmall.base.util;

import org.apache.commons.lang.StringUtils;

import java.util.Locale;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
public class MessageUtils {

    private MessageUtils() {
    }

    public static String getMessage(Locale locale, String msgId, Object... params) {
        // 妥当性验证
        if (StringUtils.isEmpty(msgId)) {
            return null;
        }
        if (locale == null) {
            locale = Locale.getDefault();
        }
        try {
            return SpringUtils.getApplicationContext().getMessage(msgId, params, locale);
        } catch (Exception e) {
            return msgId;
        }
    }

    public static String getMessage(String msgId, Object... params) {
        return MessageUtils.getMessage(null, msgId, params);
    }
}

