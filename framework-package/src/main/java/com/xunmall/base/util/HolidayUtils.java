package com.xunmall.base.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
public class HolidayUtils {

    private static List<Calendar> holidayList = new ArrayList<Calendar>(); // 节假日列表
    private static List<Calendar> workList = new ArrayList<Calendar>(); // 节假日列表
    private static String timeRemark = " 15:00:00";

    static {
        initHolidayList("2018-01-01");// 初始节假日
        initHolidayList("2018-02-15");// 初始节假日
        initHolidayList("2018-02-16");// 初始节假日
        initHolidayList("2018-02-17");// 初始节假日
        initHolidayList("2018-02-18");// 初始节假日
        initHolidayList("2018-02-19");// 初始节假日
        initHolidayList("2018-02-20");// 初始节假日
        initHolidayList("2018-02-21");// 初始节假日
        initHolidayList("2018-02-15");// 初始节假日

        initHolidayList("2018-04-05");// 初始节假日
        initHolidayList("2018-04-06");// 初始节假日
        initHolidayList("2018-04-07");// 初始节假日


        initHolidayList("2018-04-29");// 初始节假日
        initHolidayList("2018-04-30");// 初始节假日
        initHolidayList("2018-05-01");// 初始节假日

        initHolidayList("2018-06-16");// 初始节假日
        initHolidayList("2018-06-17");// 初始节假日
        initHolidayList("2018-06-18");// 初始节假日

        initHolidayList("2018-09-22");// 初始节假日
        initHolidayList("2018-09-23");// 初始节假日
        initHolidayList("2018-09-24");// 初始节假日


        initHolidayList("2018-10-01");// 初始节假日
        initHolidayList("2018-10-02");// 初始节假日
        initHolidayList("2018-10-03");// 初始节假日
        initHolidayList("2018-10-04");// 初始节假日
        initHolidayList("2018-10-05");// 初始节假日
        initHolidayList("2018-10-06");// 初始节假日
        initHolidayList("2018-10-07");// 初始节假日


        intWorkDay("2018-02-11");
        intWorkDay("2018-02-24");
        intWorkDay("2018-04-08");
        intWorkDay("2018-04-28");
        intWorkDay("2018-09-29");
        intWorkDay("2018-09-30");


    }

    /**
     * 活期上N个工作日的时间点
     *
     * @return
     */
    public static String getLastJobDay(int day, boolean timeMark) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Calendar ca = Calendar.getInstance();
        ca.setTime(new Date());// 设置当前时间
        Calendar c = addDateByWorkDay(ca, day);
        return timeMark ? df.format(c.getTime()) + timeRemark : df.format(c.getTime());
    }

    /**
     * <p>
     * Title: addDateByWorkDay
     * </P>
     * <p>
     * Description: 计算相加day天，并且排除节假日和周末后的日期
     * </P>
     *
     * @param calendar 当前的日期
     * @param day      相加天数
     * @return return Calendar 返回类型 返回相加day天，并且排除节假日和周末后的日期 throws date
     * 2014-11-24 上午10:32:55
     */
    public static Calendar addDateByWorkDay(Calendar calendar, int day) {
        try {
            if (day > 0) {
                for (int i = 0; i < day; i++) {
                    calendar.add(Calendar.DAY_OF_MONTH, 1);
                    if (checkHoliday(calendar)) {
                        i--;
                    }
                }
                return calendar;
            } else {
                for (int i = 0; i < Math.abs(day); i++) {
                    calendar.add(Calendar.DAY_OF_MONTH, -1);
                    if (checkHoliday(calendar)) {
                        i--;
                    }
                }
                return calendar;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return calendar;
    }

    /**
     * <p>
     * Title: checkHoliday
     * </P>
     * <p>
     * Description: 验证日期是否是节假日
     * </P>
     *
     * @param calendar 传入需要验证的日期
     * @return return boolean 返回类型 返回true是节假日，返回false不是节假日 throws date
     * 2014-11-24 上午10:13:07
     */
    public static boolean checkHoliday(Calendar calendar) throws Exception {
        // 判断日期是否是周六周日
        if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
            for (Calendar ca : workList) {
                if (ca.get(Calendar.MONTH) == calendar.get(Calendar.MONTH) && ca.get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH) && ca.get(Calendar.YEAR) == calendar.get(Calendar.YEAR)) {
                    return false;
                }
            }
        }
        // 判断日期是否是节假日
        for (Calendar ca : holidayList) {
            if (ca.get(Calendar.MONTH) == calendar.get(Calendar.MONTH) && ca.get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH) && ca.get(Calendar.YEAR) == calendar.get(Calendar.YEAR)) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkHoliday(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        try {
            return checkHoliday(calendar);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * <p>
     * Title: initHolidayList
     * </P>
     * <p>
     * Description: 把所有节假日放入list
     * </P>
     *
     * @param date 从数据库查 查出来的格式2014-05-09 return void 返回类型 throws date 2014-11-24
     *             上午10:11:35
     */
    public static void initHolidayList(String date) {
        String[] da = date.split("-");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, Integer.valueOf(da[0]));
        calendar.set(Calendar.MONTH, Integer.valueOf(da[1]) - 1);// 月份比正常小1,0代表一月
        calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(da[2]));
        holidayList.add(calendar);
    }

    /**
     * 工作日初始化
     *
     * @param date
     */
    public static void intWorkDay(String date) {
        String[] da = date.split("-");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, Integer.valueOf(da[0]));
        calendar.set(Calendar.MONTH, Integer.valueOf(da[1]) - 1);// 月份比正常小1,0代表一月
        calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(da[2]));
        workList.add(calendar);
    }
}
