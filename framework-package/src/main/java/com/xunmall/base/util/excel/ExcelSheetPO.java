package com.xunmall.base.util.excel;


import lombok.Data;

import java.util.List;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description: 定义表格的数据对象
 */
@Data
public class ExcelSheetPO {

    /**
     * sheet的名称
     */
    private String sheetName;


    /**
     * 表格标题
     */
    private String title;

    /**
     * 头部标题集合
     */
    private String[] headers;

    /**
     * 数据集合
     */
    private List<List<Object>> dataList;

}
