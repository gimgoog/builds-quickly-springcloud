package com.xunmall.base.util.password;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Configuration
public class PasswordManagerConfig {
	@Value("${password.path:}")
	private String password_path;
	
	@Value("${password.root-sec:}")
	private String root_sec;
	
    @Bean
    public PasswordManager passwordManager(){
    	PasswordManager passwordManager = new PasswordManager(password_path, root_sec);
    	return passwordManager;
    }
}
