package com.xunmall.base.util;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
public class IdCardNoUtils {
    private static final int length_15 = 15, length_18 = 18;

    public static String getGander(String idCardNo) {
        String gender = "";
        if (idCardNo.length() == length_18) {
            gender = idCardNo.substring(16, 17);
        } else {
            gender = idCardNo.substring(14, 15);
        }
        gender = Integer.parseInt(gender) % 2 == 0 ? "女士" : "先生";
        return gender;
    }

    public static String getBirthday(String idCardNo) {
        String num = idCardNo.substring(12, 14);
        return num.startsWith("0") ? num.substring(1, num.length()) : num;
    }
}
