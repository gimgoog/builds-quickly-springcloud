package com.xunmall.base.annotation;

import java.lang.annotation.*;

/**
 * @Author: WangYanjing
 * @Date: 2018/12/27 10:53
 * @Description:
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
@Documented
public @interface Lockable {
    /**
     * 锁的key<br/>
     * redis key的拼写规则: "lock-类名.方法名." + Key； key支持SpEL表达式，目前可获取到参数内容<br/>
     */
    String key() default "";

    /**
     * tryLock获取锁时，最大等待时间，默认2000毫秒
     */
    long maxWaiteTime() default 2000;

    /**
     * Lock超时时间，到时自动解锁，默认1000毫秒
     */
    long expirationTime() default 1000;

}