DROP TABLE IF EXISTS `t_file_info`;
CREATE TABLE `t_file_info` (
  `uuid` char(22) NOT NULL COMMENT '主键',
  `file_path` varchar(100) DEFAULT NULL COMMENT '文件的全路径',
  `origin_name` varchar(100) DEFAULT NULL COMMENT '文件名',
  `user_uuid` varchar(50) DEFAULT NULL COMMENT '创建者ID',
  `comp_uuid` varchar(50) DEFAULT NULL COMMENT '创建者Comp id',
  `empl_uuid` varchar(50) DEFAULT NULL COMMENT '创建者emplUuid',
  `privilege` int(11)  DEFAULT NULL COMMENT '权限',
  `priv_group` int(11) DEFAULT NULL COMMENT '权限组',
  `storage_type` varchar(50) DEFAULT NULL COMMENT '存储类型',
  `file_size` int(11) DEFAULT NULL COMMENT '文件大小',
  `content_type` varchar(50) DEFAULT NULL COMMENT '类型',
  `direct_url` varchar(2000) DEFAULT NULL COMMENT '访问URL',
  `logic_path` varchar(2000) DEFAULT NULL COMMENT '逻辑地址',
  `version` int(11) DEFAULT '0' NOT NULL COMMENT '版本',
  `create_user` char(22) DEFAULT NULL COMMENT '创建者',
  `update_user` char(22) DEFAULT NULL COMMENT '修改者',
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `delete_flag` tinyint(1) DEFAULT '0' COMMENT '删除状态（0-未删除，1-已删除）',
   PRIMARY KEY (`uuid`),
   KEY `name_index` (`origin_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文件实体表';