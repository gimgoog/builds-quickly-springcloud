package com.xunmall.security.password;

import org.junit.Test;

/**
 * Author : Gimgoog
 * Date   : 2017/9/30
 * Desc   :
 */
public class ShaPasswordEncoderTest {

	@Test
	public void encodePassword(){
		ShaPasswordEncoder passwordEncoder = new ShaPasswordEncoder();
		String enpass = passwordEncoder.encodePassword("123＆asd","534ba72248d188b5");
		System.out.println(enpass);
		boolean check = passwordEncoder.isPasswordValid(enpass,"123＆asd", "534ba72248d188b5");
		System.out.println(check);
	}
}
