package com.xunmall.security.sqlparse;

import com.google.common.collect.Lists;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.ExpressionVisitorAdapter;
import net.sf.jsqlparser.expression.StringValue;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.relational.ExpressionList;
import net.sf.jsqlparser.expression.operators.relational.InExpression;
import net.sf.jsqlparser.parser.CCJSqlParserManager;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.StatementVisitorAdapter;
import net.sf.jsqlparser.statement.select.*;
import org.junit.Test;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Author : Gimgoog
 * Date   : 2017/8/15
 * Fouction :
 */
public class JSqlParseTest {

	@Test
	public void testGetTableName() throws JSQLParserException {
		long startTime1 = System.currentTimeMillis();
		System.out.println(getTables("select * from  (select * from table_a left outer join table_b on table_a.aa=table_b.bb) limit 20,10"));
		//输出结果：[table_a, table_b]
		long time1 = System.currentTimeMillis() - startTime1;
		System.out.println(time1);

		System.out.println("------------------------------------------------------------------");
		long startTime2 = System.currentTimeMillis();
		System.out.println(getTables("SELECT * FROM MY_TABLE1 as a1, MY_TABLE2, " +
				"(SELECT * FROM MY_TABLE3) LEFT OUTER JOIN MY_TABLE4 WHERE a1.ID = (SELECT MAX(ID) FROM MY_TABLE5) AND ID2 IN (SELECT * FROM MY_TABLE6)"));
		//输出结果：[MY_TABLE1, MY_TABLE2, MY_TABLE3, MY_TABLE4, MY_TABLE5, MY_TABLE6]
		long time2 = System.currentTimeMillis() - startTime2;
		System.out.println(time2);

		System.out.println("------------------------------------------------------------------");
		long startTime3 = System.currentTimeMillis();
		System.out.println(getTables("SELECT\n" +
				"\to. NAME AS depa_name,\n" +
				"\t(SELECT\n" +
				"\t\t\tCOUNT(emp.UUID)\n" +
				"\t\tFROM t_employee_report emp\n" +
				"\t\tWHERE emp.comp_uuid = o.COMP_UUID\n" +
				"\t\tAND emp.DELETE_FLAG = 0\n" +
				"\t\tAND emp.DEPA_UUID IN (\n" +
				"\t\t\tSELECT UUID\n" +
				"\t\t\tFROM t_organization_report\n" +
				"\t\t\tWHERE\n" +
				"\t\t\t\tCOMP_UUID = o.COMP_UUID\n" +
				"\t\t\tAND NODE_LFT >= o.NODE_LFT\n" +
				"\t\t\tAND NODE_RGT <= o.NODE_RGT\n" +
				"\t\t)\n" +
				"\t\tAND emp. STATUS IN ('2', '3', '4')\n" +
				"\t) AS amount\n" +
				"FROM\n" +
				"\tt_organization_report o\n" +
				"WHERE\n" +
				"\to.COMP_UUID = 'SdcXM8npkQMoLLJ3K2RQkZ'\n" +
				"AND o.delete_flag = '0'\n" +
				"AND o.PARENT_UUID = '0'\n" +
				"ORDER BY\n" +
				"\to.NODE_LFT ASC"));
		//输出结果：[t_employee_report emp, t_organization_report o]
		long time3 = System.currentTimeMillis() - startTime3;
		System.out.println(time3);
	}


	//实现从SQL中提取表名
	public static final List<String> getTables(String sql) {
		CCJSqlParserManager parserManager = new CCJSqlParserManager();
		Statement stmt;
		try {
			//解析SQL语句
			stmt = parserManager.parse(new StringReader(sql));
		} catch (JSQLParserException e) {
			return null;
		}
		final List<String> tableNames = new ArrayList<String>();
		//使用visitor模式访问SQL的各个组成部分
		stmt.accept(new StatementVisitor(tableNames));

		System.out.println(sql);
		System.out.println(stmt.toString());
		return tableNames;
	}

	static class SelectVisitor extends SelectVisitorAdapter {
		List<String> tableNames;

		public SelectVisitor(List<String> tableNames) {
			this.tableNames = tableNames;
		}

		@Override
		public void visit(PlainSelect ps) {
			ExpressionVisitor expressionVisitor = new ExpressionVisitor(tableNames);
			expressionVisitor.setSelectVisitor(this);
			SelectItemVisitor selectItemVisitor = new SelectItemVisitor(tableNames);
			selectItemVisitor.setExpressionVisitor(expressionVisitor);
			FromItemVisitor fromItemVisitor = new FromItemVisitor(tableNames);

			//访问select中的select部分
			for(SelectItem item : ps.getSelectItems()){
				item.accept(selectItemVisitor);
			}
			//添加select中子查询的条件


			//访问select中的from部分
			ps.getFromItem().accept(fromItemVisitor);
			//添加where条件

			//访问select中的join部分
			List<Join> joins = ps.getJoins();
			if (joins != null && joins.size() > 0) {
				for (Join join : joins) {
					join.getRightItem().accept(fromItemVisitor);

					//添加on条件
					Expression oldExp = join.getOnExpression();
					List<Expression> expressions = Lists.newArrayList();
					expressions.add(new StringValue("'value1'"));
					Expression newExp = new InExpression(new Column("field"), new ExpressionList(expressions));
					if(oldExp != null) {
						AndExpression and = new AndExpression(oldExp, newExp);
						join.setOnExpression(and);
					}else{
						join.setOnExpression(newExp);
					}
				}
			}

			//访问select中的where部分

			if(ps.getWhere() != null) {
				ps.getWhere().accept(expressionVisitor);
			}
			//添加where中子查询条件
			Expression oldExp = ps.getWhere();
			List<Expression> expressions = Lists.newArrayList();
			expressions.add(new StringValue("'value1'"));
			Expression newExp = new InExpression(new Column("field"), new ExpressionList(expressions));
			if(oldExp != null) {
				AndExpression and = new AndExpression(oldExp, newExp);
				ps.setWhere(and);
			}else{
				ps.setWhere(newExp);
			}

		}

		@Override
		public void visit(SetOperationList setOpList) {
			List<SelectBody> selects =setOpList.getSelects();
			for(SelectBody select : selects){
				select.accept(this);
			}
		}
	}

	static class StatementVisitor extends StatementVisitorAdapter {
		List<String> tableNames;

		public StatementVisitor(List<String> tableNames) {
			this.tableNames = tableNames;
		}

		//访问select语句
		public void visit(Select select) {
			//访问select的各个组成部分
			select.getSelectBody().accept(new SelectVisitor(tableNames));
		}
	}

	static class FromItemVisitor extends FromItemVisitorAdapter {
		List<String> tableNames;

		public FromItemVisitor(List<String> tableNames) {
			this.tableNames = tableNames;
		}

		@Override
		public void visit(Table table) {
			String alias = table.getAlias() == null ? "" : " " + table.getAlias().getName();
			tableNames.add(table.getName() + alias);
		}

		@Override
		public void visit(SubSelect ss) {
			ss.getSelectBody().accept(new SelectVisitor(tableNames));
		}

		@Override
		public void visit(SubJoin sj) {
			sj.getLeft().accept(this);
			sj.getJoin().getRightItem().accept(this);
		}
	}

	static class ExpressionVisitor extends ExpressionVisitorAdapter{
		List<String> tableNames;

		public ExpressionVisitor(List<String> tableNames) {
			this.tableNames = tableNames;
		}

		@Override
		public void visit(SubSelect subSelect) {
			if (getSelectVisitor() != null) {
				subSelect.getSelectBody().accept(getSelectVisitor());
			}
		}
	}

	static class SelectItemVisitor extends SelectItemVisitorAdapter{
		List<String> tableNames;
		ExpressionVisitor expressionVisitor;

		public SelectItemVisitor(List<String> tableNames) {
			this.tableNames = tableNames;
		}

		public void setExpressionVisitor(ExpressionVisitor expressionVisitor){
			this.expressionVisitor = expressionVisitor;
		}

		@Override
		public void visit(SelectExpressionItem item) {
			item.getExpression().accept(expressionVisitor);
		}
	}

}
