package com.xunmall.base.util.security;

import org.junit.Test;

public class DigestUtilsTest {

	@Test
	public void digestString() {
		String input = "123";

		System.out.println("md5 in hex result              :" + DigestUtils.md5ToHex(input));
		System.out.println("md5 in base64 result           :" + DigestUtils.md5ToBase64(input));
		System.out.println("md5 in base64 url result       :" + DigestUtils.md5ToBase64UrlSafe(input));
		System.out.println("sha1 in hex result              :" + DigestUtils.sha1ToHex(input));
		System.out.println("sha1 in base64 result           :" + DigestUtils.sha1ToBase64(input));
		System.out.println("sha1 in base64 url result       :" + DigestUtils.sha1ToBase64UrlSafe(input));
	}

}
