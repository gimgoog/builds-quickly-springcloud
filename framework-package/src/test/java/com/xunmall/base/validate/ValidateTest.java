package com.xunmall.base.validate;


import com.xunmall.base.validation.annotation.IsChineseChar;
import lombok.Data;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class ValidateTest {
    private static Validator validator;

    @BeforeClass
    public static void setUpValidator() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void testValidate() {

        Car car = new Car("Morris", "dd-ab-123", 4);
        Set<ConstraintViolation<Car>> constraintViolations = validator.validate(car);
        assertEquals(1, constraintViolations.size());
        assertEquals("无效的汉字", constraintViolations.iterator().next().getMessage());

//        car = new Car( "大众", "DD-AB-123", 1 );
//        constraintViolations = validator.validate( car );
//        assertEquals( 1, constraintViolations.size() );
//        assertEquals( "最小不能小于2", constraintViolations.iterator().next().getMessage());
//        System.out.println(constraintViolations.iterator().next().getMessage());

        car = new Car("大众", null, 4);
        constraintViolations = validator.validate(car);
        assertEquals(1, constraintViolations.size());
        assertEquals("不能为空", constraintViolations.iterator().next().getMessage());
    }
}

@Data
class Car {

    @NotNull
    @IsChineseChar
    private String manufacturer;

    @NotNull
    @Size(min = 2, max = 14)
    private String licensePlate;

    @Min(2)
    private int seatCount;

    Car(String manufacturer, String licencePlate, int seatCount) {
        this.manufacturer = manufacturer;
        this.licensePlate = licencePlate;
        this.seatCount = seatCount;
    }

}